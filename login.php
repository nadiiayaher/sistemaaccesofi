<?php
    include('routes.php');
    include('captcha.php');
    include(SERVER_ROUTE.'/database.php');
    session_start();
    if(isset($_SESSION['id_usuario']))
    {
        switch($_SESSION['tipo_persona'])
        {
            case 1: // Tipo de usuario admin
                header('Location: '.WEB_ROUTE.'/admin.php');
                break;
            case 2: // Tipo de usuario encargado cp
                header('Location: '.WEB_ROUTE.'/empleado.php');
                break;
            case 3: // Tipo de usuario profesor
                header('Location: '.WEB_ROUTE.'/profesor.php');
                break;
            default:
            header('Location: '.WEB_ROUTE.'/index.php');
                break;
        }
    }
    
    $message = '';
    $email = '';
    $pass = '';
    $af_email = '';
    $af_pass = '';
    $af_captcha = '';
    $pass_btn = 'd-none';
    $roundedSquares = 'rounded_squares';
    $captchaImage = '';

    if (!empty($_POST['user_email']) && !empty($_POST['user_password']) && !empty($_POST['user_captcha']) && isset($_SESSION['captcha'])) {
        $records = $connection->prepare('SELECT * FROM persona WHERE email = :user_email');
        $records->bindParam('user_email',$_POST['user_email']);
        $records->execute();
        $result_user = $records->fetch(PDO::FETCH_ASSOC);
        if (!empty($result_user)) {
                if ($result_user['password'] == $_POST['user_password']) {
                    if( $_POST['user_captcha'] == $_SESSION['captcha'] )
                    {
                        $_SESSION['id_usuario'] = $result_user['id_persona'];
                        $_SESSION['username'] = $result_user['nombre'];
                        $_SESSION['user_email'] = $result_user['email'];
                        $_SESSION['tipo_persona'] = $result_user['tipo_persona'];
                        switch($_SESSION['tipo_persona'])
                        {
                            case 1:
                                $_SESSION['tipo_persona_str'] = "ADMINISTRADOR";
                                header('Location: admin.php');
                                break;
                            case 2:
                                $_SESSION['tipo_persona_str'] = "ENCARGADO DE PUNTO DE CONTROL";
                                header('Location: empleado.php');
                                break;
                            case 3: // Tipo de usuario profesor
                                $_SESSION['tipo_persona_str'] = "PROFESOR";
                                header('Location: profesor.php');
                            default:
                                break;
                        }
                    }
                    else
                    {
                        $message = 'Captcha incorrecto. Intentalo nuevamente';
                        $captchaImage = createCaptchaImage();
                        $email = $_POST['user_email'];
                        $pass = $_POST['user_password'];
                        $af_captcha = 'autofocus';
                        $pass_btn = '';
                        $roundedSquares = '';
                    }
                    
                } 
                else {
                    $message = 'La contraseña ingresada es incorrecta';
                    $email = $_POST['user_email'];
                    $pass = $_POST['user_password'];
                    $af_pass = 'autofocus';
                    $captchaImage = createCaptchaImage();
                    $pass_btn = '';
                    $roundedSquares = '';
                }
        }
        else {
            $message = 'No existe ningun usuario con ese email';
            $email = $_POST['user_email'];
            $af_email = 'autofocus';
            $captchaImage = createCaptchaImage();
            $pass_btn = '';
            $roundedSquares = '';
        }
    }
    else
    {
        $captchaImage = createCaptchaImage();
    }
?>

<!doctype html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
    <link href="<?=WEB_ROUTE?>/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link rel="icon" type="/image/png" href="<?=WEB_ROUTE?>/img/png/fi_invert.png" />
    <link rel="stylesheet" href="<?=WEB_ROUTE?>/css/standard-style.css">
    <link rel="stylesheet" href="<?=WEB_ROUTE?>/css/login-style.css">
    <title>Sistema de control de acceso FI UAEMex</title>
</head>

<body>
    <div class="container bg-white" id="topbar">
        <header class="head">
            <nav class="navbar navbar-expand-lg navbar-light head__custom-nav">
                <a class="navbar-brand d-none d-lg-block d-xl-block" href="" id="main-title">
                    <img class="" height="50" src="<?=WEB_ROUTE?>/img/png/img_logo_uaem18-21.png" alt="Universidad Autonoma del Estado de Mexico">
                </a>
                <a class="navbar-brand d-flex flex-row d-none d-sm-inline-block d-md-inline-block d-lg-none" href="" id="main-title">
                    <img class="" height="50" src="<?=WEB_ROUTE?>/img/png/logo_fi_uaem_invert.png" alt="Universidad Autonoma del Estado de Mexico">
                </a>
                <button class="btn navbar-toggler" id="menuButton" type="button" data-toggle="collapse" data-target="#navbarNav"
                    style="border: none;">
                    <ion-icon name="chevron-down-outline"
                        style="color: black; font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item pt-2 mr-3">
                            <a class="nav-link" href="<?=WEB_ROUTE?>/index.php">Inicio</a>
                        </li>
                        <li class="nav-item pt-2 mr-3">
                            <a class="nav-link" href="">Acerca de</a>
                        </li>
                        <li class="nav-item pt-2 mr-3">
                            <a class="nav-link" href="">Ayuda</a>
                        </li>
                        <li class="nav-item d-none d-md-block d-xl-block d-xxl-block pt-2">
                            <a href="">
                                <img class="" src="<?=WEB_ROUTE?>/img/svg/logo_fi_uaem.svg" alt="Facultad de Ingeniería UAEM" height="48">
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xl-10 col-lg-12 col-md-9 col-sm-12 mx-auto">
                <div class="card o-hidden login-card border-0 shadow-lg animate__animated animate__fadeIn">
                    <!-- animate__pulse -->
                    <div class="card-body p-0" id="custom-body" style="overflow: hidden; border-top-left-radius: 4px; border-bottom-left-radius: 4px;">
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block bg-image">
                            </div>
                            <div class="col-lg-6">
                                <div class="" style="padding:10%;">
                                    <div class="text-center">
                                        <h5 class="h5 text-gray-900 mb-4" style="font-weight:600;">Inicio de sesión</h5>
                                    </div>
                                    <hr>
                                    <div class="text-center">
                                        <p>Ingresa con tu cuenta institucional:</p>
                                    </div>
                                    <?php if ($message != ''): ?>
                                    <p class="error-message"><?=$message?></p>
                                    <?php 
                                $message = '';
                                endif; ?>

                                    <form action="<?=WEB_ROUTE?>/login.php" method="POST" id="login_form">
                                        <div class="form-group pt-1">
                                            <input type="email" class="form-control form-control-user user_email"
                                                id="user_email" name="user_email" aria-describedby="emailHelp"
                                                placeholder="example@dominio.uaemex.mx" value="<?=$email?>"
                                                <?=$af_email?> style="color: black;">
                                            <div id="msg_email" class="danger-msg mt-1">
                                                <!-- Error message here -->
                                            </div>
                                        </div>
                                        <div class="input-group">
                                            <input type="password" class="form-control form-control-user form-input <?=$roundedSquares?>"
                                                id="user_password" name="user_password" placeholder="Contraseña"
                                                value="<?=$pass?>" <?=$af_pass?>>
                                            <div class="input-group-append">
                                                <button class="btn btn-pass <?=$pass_btn?>" id="see_pass_btn" type="button" style="max-height:38px;">
                                                    <ion-icon name="eye-outline"
                                                        style="vertical-align:middle; font-size:22px;"></ion-icon>
                                                </button>
                                            </div>
                                        </div>
                                        <div id="msg_pass" class="danger-msg mt-1 mb-2">
                                            <!-- Error message here -->
                                        </div>

                                        <label class style="font-size:12px;">Completa el captcha:</label>
                                        <div class="input-group mt-0">
                                            <div class="col-3 p-0 m-0">
                                                <img src="data:image/png;base64,<?=$captchaImage?>" height="38px" id="captcha_img"></img>
                                            </div>
                                            <div class="col-9 p-0 m-0">
                                                <input type="text" name="user_captcha" class="form-control form-input w-100" id="user_captcha" autocomplete="off" <?=$af_captcha?>>
                                            </div>
                                        </div>
                                        <div id="msg_captcha" class="danger-msg mt-1 mb-2">
                                            <!-- Error message here -->
                                        </div>

                                        <button type="submit" class="btn btn-primary btn-block btn-login mt-4">
                                            Ingresar
                                        </button>
                                    </form>
                                    <hr>
                                    <div class="text-center">
                                        <a class="small" href="<?=WEB_ROUTE?>/forgot-password.php"
                                            style="color: #958419;">Olvidaste tu contraseña?</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pt-2 mb-3 ml-1 d-flex justify-content-between">
                    <a href="" class="text-white" style="font-size:14px;">Aviso de privacidad &nbsp;<i
                            class="fas fa-external-link-alt" style="font-size:12px;"></i></a>
                    <span class="text-white d-none d-lg-inline-block d-xl-inline-block" style="font-size:14px;">Facultad de Ingeniería UAEM</span>
                </div>
            </div>
        </div>
    </div>

    

    <script src="https://code.jquery.com/jquery-3.5.1.js"
        integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous">
    </script>
    <script src="https://unpkg.com/ionicons@5.2.3/dist/ionicons.js"></script>
    <script src="<?=WEB_ROUTE?>/js/login-func.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
</body>

</html>