<?php
  include('routes.php');
  include(SERVER_ROUTE.'/database.php');
  session_start();
  if(isset($_SESSION['id_usuario']))
  {
      switch($_SESSION['tipo_persona'])
      {
          case 1: // Tipo de usuario admin
              header('Location: '.WEB_ROUTE.'/admin.php');
              break;
          case 2: // Tipo de usuario encargado de cp
              break;
          case 3: // Tipo de usuario profesor
              header('Location: '.WEB_ROUTE.'/profesor.php');
              break;
          default:
            header('Location: '.WEB_ROUTE.'/index.php');
            break;
      }
  }
  else
  {
      header('Location: '.WEB_ROUTE.'/index.php');
  }
  $records = $connection->prepare('SELECT * FROM  punto_control;');
  $records->execute();
  $control_points = $records->fetchAll();
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Sistema de control de acceso FI UAEM</title>
  <!-- Custom fonts for this template-->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
  <link href="<?=WEB_ROUTE?>/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link rel="icon" type="image/png" href="<?=WEB_ROUTE?>/img/png/fi_invert.png"/>
  <link rel="stylesheet" href="<?=WEB_ROUTE?>/toastr/build/toastr.css">
  <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
  <!-- Custom styles for this template-->
  <link href="<?=WEB_ROUTE?>/css/standard-style.css" rel="stylesheet">
  <link rel="stylesheet" href="<?=WEB_ROUTE?>/css/empleado-style.css">
</head>
<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    <!-- Sidebar -->
    <ul class="navbar-nav sidebar accordion" id="accordionSidebar">
      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
        <div class="sidebar-brand-icon">
          <img src="<?=WEB_ROUTE?>/img/png/uaem-logo.png" alt="" width="57px" height="50px" style="border-radius:3px;">
        </div>
        <div class="sidebar-brand-text mx-2">UAEM</div>
      </a>
      
      <hr class="sidebar-divider my-1">
      <!--
      <li class="nav-item">
        <a class="nav-link" href="<?=WEB_ROUTE?>/empleado.php">
          <ion-icon name="contract-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Puntos de control</span>
        </a>
      </li>
      
      <hr class="sidebar-divider">
      -->
      <div class="sidebar-heading">
        Operaciones
      </div>
      <!-- Nav Item - Tables -->
      <li class="nav-item active">
        <a class="nav-link" href="">
        <ion-icon name="person-add-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Registrar acceso</span>
        </a>
      </li>

      <!-- Nav Item - Tables -->
      <li class="nav-item">
        <a class="nav-link" href="<?=WEB_ROUTE?>/empleado-files/empleado-registrar-caso.php">
        <ion-icon name="warning-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Registrar caso sospechoso</span>
        </a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">
      <!-- Heading -->
      <div class="sidebar-heading">
        Ayuda
      </div>
      <!-- Nav Item - Tables -->
      <li class="nav-item">
        <a class="nav-link" href="">
          <ion-icon name="help-circle-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Ayuda</span>
        </a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">
      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>
    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
      <?php include(SERVER_ROUTE.'/templates/navbar.php'); ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">
           <!-- Page Heading -->
            <div class="row">
              <div class="col-sm-12 col-md-11 mx-auto">
                <h6 class="mb-4" style="font-weight:550; font-size:20px;">Registrar acceso de persona</h6>
                  <div class="card card-content shadow mb-3">
                    <div class="card-body">
                      <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                          <a class="nav-link tab active" id="nav-interno" data-toggle="tab" href="#nav-interno-tab" role="tab" aria-controls="nav-interno-tab" aria-selected="true">Interno</a>
                          <a class="nav-link tab" id="nav-externo" data-toggle="tab" href="#nav-externo-tab" role="tab" aria-controls="nav-externo-tab" aria-selected="false">Externo</a>
                        </div>
                      </nav>
                      <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-interno-tab" role="tabpanel" aria-labelledby="nav-interno">
                                <form class="mt-4">
                                    <label for="">Punto de control:</label>
                                    <select name="" class="custom-select form-input" id="cp-interno">
                                      <option value="0" hidden selected>Selecciona un punto de control</option>
                                      <?php for ($i = 0; $i < sizeof($control_points); $i++){?>
                                        <option value="<?= $control_points[$i]['id_cp']?>"><?= $control_points[$i]['nombre']?></option>
                                      <?php };?>
                                    </select>
                                    <div id="select-error" class="danger-msg mt-1">
                                      <!-- Error message here -->
                                    </div>
                                    <label class="mt-3" for="exampleFormControlInput1">Buscar persona:</label>
                                    <div class="input-group">
                                      <input type="text" class="form-control form-input" id="to_search" placeholder="Escribe el nombre o la matrícula de la persona a buscar" autofocus>
                                      <div class="input-group-append">
                                        <button class="btn btn-hov btn-primary" id="search-btn" type="submit">
                                          <span class="d-none d-sm-inline">Buscar &nbsp;</span>
                                          <ion-icon name="search-outline" style="font-size: 20px; vertical-align: middle;"></ion-icon>
                                        </button>
                                      </div>
                                    </div>
                                    <div id="search-error" class="error-text">
                                      <!-- Error message here -->
                                    </div>
                                    <div class="d-flex justify-content-center" id="error_container">
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="nav-externo-tab" role="tabpanel" aria-labelledby="nav-externo">
                                <form class="mt-4">
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label for="">Nombre:</label>
                                            <input class="form-control form-input" type="text" name="externo-nombre" id="externo-nombre" placeholder="Ingrese nombre completo de la persona a registrar">
                                            <div id="extern-name-error" class="danger-msg mt-1">
                                              <!-- Error message here -->
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="">Punto de control:</label>
                                            <select name="cp-externo" id="cp-externo" class="custom-select form-input">
                                              <option value="0" hidden selected>Selecciona un punto de control</option>
                                              <?php for ($i = 0; $i < sizeof($control_points); $i++){?>
                                                <option value="<?= $control_points[$i]['id_cp']?>"><?= $control_points[$i]['nombre']?></option>
                                              <?php };?>
                                            </select>
                                            <div id="extern-select-error" class="danger-msg mt-1">
                                              <!-- Error message here -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label for="">Email (opcional):</label>
                                            <input class="form-control form-input" type="email" name="externo-email" id="externo-email" placeholder="Ingrese un email de contacto">
                                            <div id="extern-email-error" class="danger-msg mt-1">
                                              <!-- Error message here -->
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="">Número de celular (opcional):</label>
                                            <input class="form-control form-input" type="tel" id="externo-tel" placeholder="Ingrese numero de celular">
                                            <div id="extern-tel-error" class="danger-msg mt-1">
                                              <!-- Error message here -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row mb-4">
                                        <label for="">Motivo de visita:</label>
                                        <textarea class="form-control form-input" id="externo-motivo" rows="3" placeholder="Ingrese una descripcion del motivo de la visita"></textarea>
                                        <div id="extern-motivo-error" class="danger-msg mt-1">
                                          <!-- Error message here -->
                                        </div>
                                    </div>
                                    <div class="form-row mb-2">
                                        <button class="btn btn-hov mx-auto" type="submit" id="register-btn" style="width: 100%;">REGISTRAR</button>
                                    </div>
                                </form>
                            </div>
                          </div>
                       </div>
                   </div>
               </div>
            </div>
            
            <div class="result_header"></div>
            <div class="row mb-4" id="result_container">
              <!-- RESULT HERE -->
            </div>
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
    </div>
    <!-- End of Content Wrapper -->
  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <div class="modal fade border-0" tabindex="-1" id="register_modal">
    <div class="modal-dialog modal-dialog-centered border-0">
      <div class="modal-content border-0">
        <div class="modal-header border-0">
          <h6 class="modal-title">Seguro que deseas registrar el acceso de este usuario?</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body border-0">
          <div class="row mt-2 mb-2 ml-2">
            <div class="col-4 mr-0 p-1 info-left">Matrícula:</div>
            <div class="col-7 mr-2 info-right" id="info_userid" data-userstatus=""></div>
          </div>

          <div class="row mb-2 ml-2">
            <div class="col-4 mr-0 p-1 info-left">Nombre:</div>
            <div class="col-7 mr-2 info-right" id="info_username"></div>
          </div>

          <div class="row mb-2 ml-2">
            <div class="col-4 mr-0 p-1 info-left">Tipo de usuario:</div>
            <div class="col-7 mr-2 info-right" id="info_usetype"></div>
          </div>

          <div class="form-row ml-2 mr-3 mt-4">
            <label for="" style="font-size:14px;">Detalles (opcional):</label>
            <textarea class="form-control form-input placeholder-size" id="info_detalles" rows="3" placeholder="Escribe aquí los detalles de la visita que consideres nesesarios"></textarea>
          </div>

          <div class="" id="info_sospechoso">
          </div>

        </div>
        <div class="modal-footer border-0">
          <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Cancelar</button>
          <button type="button" class="btn btn-green-strong" id="modal_register">Registrar usuario</button>
        </div>
      </div>
    </div>
  </div>


  <?php include(SERVER_ROUTE.'/templates/logout-modal.php');?>

  <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
  <script src="https://unpkg.com/ionicons@5.2.3/dist/ionicons.js"></script>
  <script src="<?=WEB_ROUTE?>/toastr/build/toastr.min.js"></script>
  <!-- Custom scripts for all pages-->
  <script src="<?=WEB_ROUTE?>/js/standard-func.js"></script>
  <script src="<?=WEB_ROUTE?>/js/empleado-registrar.js"></script>
</body>
</html>
