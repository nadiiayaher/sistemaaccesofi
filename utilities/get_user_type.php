<?php
    function getUserType($user_int)
    {
        if ($user_int == 1)
        {
            return "ADMINISTRATIVO / EMPLEADO";
        }
        else if ($user_int == 2)
        {
            return "ENCARGADO DE PUNTO DE CONTROL";
        }
        else if ($user_int == 3)
        {
            return "PROFESOR";
        }
        else if ($user_int == 4)
        {
            return "ALUMNO";
        }
        else if ($user_int == 5)
        {
            return "USUARIO EXTERNO";
        }
        else if ($user_int == 6)
        {
            return "ADMINISTRATIVO / EMPLEADO";
        }
        else
        {
            return "NULL";
        }
    };
?>