<?php
    function getMonths($actual_month) {
        $month_now = intval(date('m'),$base = 10 );
        $months = ["","","","","","","","","","","",""];
        switch ($month_now) {
        case 1:
            $months[0] = "selected";
            break;
        case 2:
            $months[1] = "selected";
            break;
        case 3:
            $months[2] = "selected";
            break;
        case 4:
            $months[3] = "selected";
            break;
        case 5:
            $months[4] = "selected";
            break;
        case 6:
            $months[5] = "selected";
            break;
        case 7:
            $months[6] = "selected";
            break;
        case 8:
            $months[7] = "selected";
            break;
        case 9:
            $months[8] = "selected";
            break;
        case 10:
            $months[9] = "selected";
            break;
        case 11:
            $months[10] = "selected";
            break;
        case 12:
            $months[11] = "selected";
            break;
        default:
            break;
        }
        return $months;
    };
?>