<?php
    function getStats($first_day,$last_day,$connection)
    {
        $first_day.= " 00:00";
        $last_day.= " 23:59";

        $records = $connection->prepare('SELECT COUNT(id_acceso) AS accesos_totales FROM acceso WHERE acceso.registered_at >= :first_day AND acceso.registered_at <= :last_day;');
        $records->bindParam('first_day',$first_day);
        $records->bindParam('last_day',$last_day);
        $records->execute();
        $result = $records->fetch(PDO::FETCH_ASSOC);
        $accesos_totales = $result['accesos_totales'];
        if( $accesos_totales == "0" )
        {
            $accesos_alumnos = "0";
            $accesos_profesores = "0";
            $accesos_externos = "0";
            $accesos_administrativos = "0";
            $accesos = "[]";
        }
        else
        {
            $records = $connection->prepare('SELECT COUNT(acceso.id_acceso) AS accesos_alumnos FROM acceso,persona WHERE acceso.id_persona = persona.id_persona AND acceso.registered_at >= :first_day AND acceso.registered_at <= :last_day and persona.tipo_persona = 4;');
            $records->bindParam('first_day',$first_day);
            $records->bindParam('last_day',$last_day);
            $records->execute();
            $result = $records->fetch(PDO::FETCH_ASSOC);
            $accesos_alumnos = $result['accesos_alumnos'];

            $records = $connection->prepare('SELECT COUNT(acceso.id_acceso) AS accesos_profesores FROM acceso,persona WHERE acceso.id_persona = persona.id_persona AND acceso.registered_at >= :first_day AND acceso.registered_at <= :last_day and persona.tipo_persona = 3;');
            $records->bindParam('first_day',$first_day);
            $records->bindParam('last_day',$last_day);
            $records->execute();
            $result = $records->fetch(PDO::FETCH_ASSOC);
            $accesos_profesores = $result['accesos_profesores'];
        
            $records = $connection->prepare('SELECT COUNT(acceso.id_acceso) AS accesos_externos FROM acceso,persona WHERE acceso.id_persona = persona.id_persona AND acceso.registered_at >= :first_day AND acceso.registered_at <= :last_day and persona.tipo_persona = 5;');
            $records->bindParam('first_day',$first_day);
            $records->bindParam('last_day',$last_day);
            $records->execute();
            $result = $records->fetch(PDO::FETCH_ASSOC);
            $accesos_externos = $result['accesos_externos'];
        
            $records = $connection->prepare('SELECT COUNT(acceso.id_acceso) AS accesos_administrativos FROM acceso,persona WHERE acceso.id_persona = persona.id_persona AND acceso.registered_at >= :first_day AND acceso.registered_at <= :last_day AND (persona.tipo_persona = 6 OR persona.tipo_persona = 1 OR persona.tipo_persona = 2 );');
            $records->bindParam('first_day',$first_day);
            $records->bindParam('last_day',$last_day);
            $records->execute();
            $result = $records->fetch(PDO::FETCH_ASSOC);
            $accesos_administrativos = $result['accesos_administrativos'];

            $records = $connection->prepare('SELECT persona.id_persona, persona.nombre, persona.tipo_persona,
            DATE_FORMAT(acceso.registered_at,\'%H:%i\') AS hora_reg,
            DATE_FORMAT(acceso.registered_at,\'%d/%m/%Y\') AS fecha_reg,
            punto_control.nombre AS punto_control, acceso.detalles_visita AS detalles, A.nombre AS encargado
            FROM acceso,persona,punto_control, persona A
            WHERE acceso.id_persona = persona.id_persona
            AND acceso.id_cp = punto_control.id_cp
            AND acceso.registered_at >= :first_day
            AND acceso.registered_at <= :last_day
            AND acceso.id_encargado = A.id_persona
            ORDER BY acceso.registered_at;
            ');
            $records->bindParam('first_day',$first_day);
            $records->bindParam('last_day',$last_day);
            $records->execute();
            $accesos = json_encode($records->fetchAll());
        }
    
        $records = $connection->prepare('SELECT COUNT(caso_sospechoso.id_caso) AS casos_totales FROM caso_sospechoso WHERE caso_sospechoso.registered_at >= :first_day AND caso_sospechoso.registered_at <= :last_day;');
        $records->bindParam('first_day',$first_day);
        $records->bindParam('last_day',$last_day);
        $records->execute();
        $result = $records->fetch(PDO::FETCH_ASSOC);
        $casos_totales = $result['casos_totales'];
        if ( $casos_totales == "0" ) {
            $casos_alumnos = "0";
            $casos_profesores = "0";
            $casos_externos = "0";
            $casos_administrativos = "0";
            $casos_sospechosos = "[]";
        }
        else
        {
            $records = $connection->prepare('SELECT COUNT(caso_sospechoso.id_caso) AS casos_alumnos FROM caso_sospechoso,persona WHERE caso_sospechoso.id_persona = persona.id_persona AND caso_sospechoso.registered_at >= :first_day AND caso_sospechoso.registered_at <= :last_day and persona.tipo_persona = 4;');
            $records->bindParam('first_day',$first_day);
            $records->bindParam('last_day',$last_day);
            $records->execute();
            $result = $records->fetch(PDO::FETCH_ASSOC);
            $casos_alumnos = $result['casos_alumnos'];

            $records = $connection->prepare('SELECT COUNT(caso_sospechoso.id_caso) AS casos_profesores FROM caso_sospechoso,persona WHERE caso_sospechoso.id_persona = persona.id_persona AND caso_sospechoso.registered_at >= :first_day AND caso_sospechoso.registered_at <= :last_day and persona.tipo_persona = 3;');
            $records->bindParam('first_day',$first_day);
            $records->bindParam('last_day',$last_day);
            $records->execute();
            $result = $records->fetch(PDO::FETCH_ASSOC);
            $casos_profesores = $result['casos_profesores'];

            $records = $connection->prepare('SELECT COUNT(caso_sospechoso.id_caso) AS casos_externos FROM caso_sospechoso,persona WHERE caso_sospechoso.id_persona = persona.id_persona AND caso_sospechoso.registered_at >= :first_day AND caso_sospechoso.registered_at <= :last_day and persona.tipo_persona = 5;');
            $records->bindParam('first_day',$first_day);
            $records->bindParam('last_day',$last_day);
            $records->execute();
            $result = $records->fetch(PDO::FETCH_ASSOC);
            $casos_externos = $result['casos_externos'];

            $records = $connection->prepare('SELECT COUNT(caso_sospechoso.id_caso) AS casos_administrativos FROM caso_sospechoso,persona WHERE caso_sospechoso.id_persona = persona.id_persona AND caso_sospechoso.registered_at >= :first_day AND caso_sospechoso.registered_at <= :last_day AND (persona.tipo_persona = 6 OR persona.tipo_persona = 1 OR persona.tipo_persona = 2 );');
            $records->bindParam('first_day',$first_day);
            $records->bindParam('last_day',$last_day);
            $records->execute();
            $result = $records->fetch(PDO::FETCH_ASSOC);
            $casos_administrativos = $result['casos_administrativos'];
            
            $records = $connection->prepare('SELECT persona.id_persona,persona.nombre,persona.tipo_persona,
            DATE_FORMAT(caso_sospechoso.registered_at,\'%H:%i\') AS hora_reg,
            DATE_FORMAT(caso_sospechoso.registered_at,\'%d/%m/%Y\') AS fecha_reg,
            punto_control.nombre AS punto_control,caso_sospechoso.detalles, A.nombre AS encargado
            FROM caso_sospechoso,persona,punto_control,persona A
            WHERE caso_sospechoso.id_persona = persona.id_persona
            AND caso_sospechoso.id_cp = punto_control.id_cp
            AND caso_sospechoso.registered_at >= :first_day
            AND caso_sospechoso.registered_at <= :last_day
            AND caso_sospechoso.id_encargado = A.id_persona
            ORDER BY caso_sospechoso.registered_at;
            ');
            $records->bindParam('first_day',$first_day);
            $records->bindParam('last_day',$last_day);
            $records->execute();
            $casos_sospechosos = json_encode($records->fetchAll());
        }
        
        $records = $connection->prepare('SELECT COUNT(persona.id_persona) AS casos_activos_totales FROM persona WHERE persona.status = 1;');
        $records->execute();
        $result = $records->fetch(PDO::FETCH_ASSOC);
        $casos_activos_totales = $result['casos_activos_totales'];
        if ($casos_activos_totales == "0") {
            $casos_activos_alumnos = "0";
            $casos_activos_profesores = "0";
            $casos_activos_externos = "0";
            $casos_activos_administrativos = "0";
            $casos_activos = "[]";
        }
        else
        {
            $records = $connection->prepare('SELECT COUNT(persona.id_persona) AS casos_activos_alumnos FROM persona WHERE persona.status = 1 AND persona.tipo_persona = 4;');
            $records->execute();
            $result = $records->fetch(PDO::FETCH_ASSOC);
            $casos_activos_alumnos = $result['casos_activos_alumnos'];

            $records = $connection->prepare('SELECT COUNT(persona.id_persona) AS casos_activos_profesores FROM persona WHERE persona.status = 1 AND persona.tipo_persona = 3;');
            $records->execute();
            $result = $records->fetch(PDO::FETCH_ASSOC);
            $casos_activos_profesores = $result['casos_activos_profesores'];

            $records = $connection->prepare('SELECT COUNT(persona.id_persona) AS casos_activos_externos FROM persona WHERE persona.status = 1 AND persona.tipo_persona = 5;');
            $records->execute();
            $result = $records->fetch(PDO::FETCH_ASSOC);
            $casos_activos_externos = $result['casos_activos_externos'];

            $records = $connection->prepare('SELECT COUNT(persona.id_persona) AS casos_activos_administrativos FROM persona WHERE persona.status = 1 AND (persona.tipo_persona = 1 OR persona.tipo_persona = 2 OR persona.tipo_persona = 6);');
            $records->execute();
            $result = $records->fetch(PDO::FETCH_ASSOC);
            $casos_activos_administrativos = $result['casos_activos_administrativos'];
            /*
            $records = $connection->prepare('SELECT persona.id_persona,persona.nombre,persona.tipo_persona,persona.status,
            DATE_FORMAT(caso_sospechoso.registered_at,\'%H:%i\') AS hora_reg,
            DATE_FORMAT(caso_sospechoso.registered_at,\'%d/%m/%Y\') AS fecha_reg,
            punto_control.nombre AS punto_control, A.nombre AS encargado
            FROM caso_sospechoso,persona,punto_control,persona A
            WHERE caso_sospechoso.id_persona = persona.id_persona
            AND caso_sospechoso.id_cp = punto_control.id_cp
            AND persona.status = 1
            AND caso_sospechoso.id_encargado = A.id_persona
            ORDER BY caso_sospechoso.registered_at;
            ');
            $records->execute();
            $casos_activos = json_encode($records->fetchAll());
            */
        }
        
        $res = array(
            "status" => 202,
            "accesos_totales" => $accesos_totales,
            "accesos_profesores" => $accesos_profesores,
            "accesos_alumnos" => $accesos_alumnos,
            "accesos_externos" => $accesos_externos,
            "accesos_administrativos" => $accesos_administrativos,
            "accesos" => $accesos,
            
            "casos_totales" => $casos_totales,
            "casos_alumnos" => $casos_alumnos,
            "casos_profesores" => $casos_profesores,
            "casos_externos" => $casos_externos,
            "casos_administrativos" => $casos_administrativos,
            "casos_sospechosos" => $casos_sospechosos,

            "casos_activos_totales" => $casos_activos_totales,
            "casos_activos_alumnos" => $casos_activos_alumnos,
            "casos_activos_profesores" => $casos_activos_profesores,
            "casos_activos_externos" => $casos_activos_externos,
            "casos_activos_administrativos" => $casos_activos_administrativos,
            "casos_activos" => $casos_activos,
        );
        return $res;
    };
?>