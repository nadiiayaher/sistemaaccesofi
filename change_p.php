<?php
include('routes.php');
include(SERVER_ROUTE.'/database.php');
session_start();
if(isset($_SESSION['id_usuario']))
{
    $identificador = $_POST['identificador'];
    $new_pass = $_POST['new_p'];
    $user_type = $_POST['user_type'];
    switch($_SESSION['tipo_persona'])
    {
        case 1:
            if ($user_type == "1") {
                $records = $connection->prepare('UPDATE persona SET persona.password = :pass WHERE persona.identificador = :identificador;');
                $records->bindParam('pass',$new_pass);
                $records->bindParam('identificador',$identificador);
                if( $records->execute() ){
                    $res = array("status" => 202, "message" => 'Se ha cambiado la contraseña exitosamente!');
                    echo json_encode($res);
                }
                else
                {
                    $res = array("status" => 404, "message" => 'No se pudo realizar la operación. Parece que el servidor esta tenido problemas. Intenta realizar la operación más tarde');
                    echo json_encode($res);
                }
            }
            else
            {
                $res = array("status" => 404, "message" => 'No se pudo realizar la operación. No se ha podido confirmar tu idetidad');
                echo json_encode($res);
            }
            break;
        case 2:
            if ($user_type == "2") {
                $records = $connection->prepare('UPDATE persona SET persona.password = :pass WHERE persona.identificador = :identificador;');
                $records->bindParam('pass',$new_pass);
                $records->bindParam('identificador',$identificador);
                if( $records->execute() ){
                    $res = array("status" => 202, "message" => 'Se ha cambiado la contraseña exitosamente!');
                    echo json_encode($res);
                }
                else
                {
                    $res = array("status" => 404, "message" => 'No se pudo realizar la operación. Parece que el servidor esta tenido problemas. Intenta realizar la operación más tarde');
                    echo json_encode($res);
                }
            }
            else
            {
                $res = array("status" => 404, "message" => 'No se pudo realizar la operación. No se ha podido confirmar tu idetidad');
                echo json_encode($res);
            }
            break;
        case 3: // Tipo de usuario profesor
            if ($user_type == "3") {
                $records = $connection->prepare('UPDATE persona SET persona.password = :pass WHERE persona.identificador = :identificador;');
                $records->bindParam('pass',$new_pass);
                $records->bindParam('identificador',$identificador);
                if( $records->execute() ) {
                    $res = array("status" => 202, "message" => 'Se ha cambiado la contraseña exitosamente!');
                    echo json_encode($res);
                }
                else
                {
                    $res = array("status" => 404, "message" => 'No se pudo realizar el registro. Parece que el servidor esta tenido problemas. Intenta realizar la operacion mas tarde');
                    echo json_encode($res);
                }
            }
            else
            {
                $res = array("status" => 404, "message" => 'No se pudo realizar la operación. No se ha podido confirmar tu idetidad');
                echo json_encode($res);
            }
            break;
        default:
            $res = array("status" => 404, "message" => 'No se pudo realizar la operación. No se ha podido confirmar tu idetidad');
            echo json_encode($res);
            break;
    }
}
else
{
    $res = array("status" => 404, "message" => 'No se pudo realizar la operación. No se ha podido confirmar tu idetidad');
    echo json_encode($res);
}
?>