
<!-- Logout Modal-->
<div class="modal fade border-0" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered border-0" role="document">
        <div class="modal-content border-0">
            <div class="modal-header border-0">
                <h6 class="modal-title"><strong>Seguro que deseas salir?</strong></h6>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body border-0">Selecciona "Cerrar sesión" a continuación si estás listo para terminar tu sesión actual.</div>
            <div class="modal-footer border-0">
                <button type="button" class="btn btn-outline-dark" type="cancel" data-dismiss="modal">Cancelar</button>
                <a class="btn btn-hov" href="<?=WEB_ROUTE?>/logout.php">Cerrar sesión</a>
            </div>
        </div>
    </div>
</div>