<?php
  include(SERVER_ROUTE.'/database.php');
  date_default_timezone_set('America/Tegucigalpa');
  $date = date('Y-m-d');
  $fecha = date('d/m/Y');
  $records = $connection->prepare('SELECT DATE_FORMAT(acceso.registered_at,\'%d/%m/%Y\') AS fecha_reg, DATE_FORMAT(acceso.registered_at,\'%H:%i:%s\') AS hora_reg, punto_control.nombre AS punto_control FROM acceso,punto_control WHERE acceso.id_cp = punto_control.id_cp AND acceso.id_persona = :id_persona AND acceso.registered_at >= :fecha_actual;');
  $records->bindParam('id_persona',$_SESSION['id_usuario']);
  $records->bindParam('fecha_actual',$date);
  $records->execute();
  $result = $records->fetch(PDO::FETCH_ASSOC);

  $records = $connection->prepare('SELECT persona.status FROM persona WHERE persona.id_persona = :id_persona;');
  $records->bindParam('id_persona',$_SESSION['id_usuario']);
  $records->execute();
  $status = $records->fetch(PDO::FETCH_ASSOC);

  $access = false;

  if ( !empty($status) && $status['status'] == "1") {
    $message = "No tienes acceso";
    $access = false;
  }
  else
  {
    if(empty($result))
    {
      $message = "No has pasado por ningun punto de control";
      $access = false;
    }
    else {
      $message = "Tienes acceso";
      $access = true;
    }
  }
  
  $profile_route = "";
  switch($_SESSION['tipo_persona'])
  {
      case 1: // Tipo de usuario admin
          $profile_route = "/admin-files/admin-profile.php";
          break;
      case 2: // Tipo de usuario encargado de cp
          $profile_route = "/empleado-files/employe-profile.php";
          break;
      case 3: // Tipo de usuario profesor
          $profile_route = "/profesor-files/profesor-profile.php";
          break;
      default:
          $profile_route = "";
          break;
  }
?>
<!-- Topbar -->
<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top" style="box-shadow: 0 4px 4px 0 #dfe6e9;">
    <!-- Sidebar Toggle (Topbar) -->
    <button id="sidebarToggleTop" class="btn btn-link p-0 m-0 d-md-none">
      <ion-icon name="menu-outline"  class="mx-auto" style="font-size: 27px; color: #2d3436; padding-top:5px;"></ion-icon>
    </button>

    <div class="ml-4 d-md-none" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;">
      <img src="<?=WEB_ROUTE?>/img/svg/logo_fi_uaem_invert.svg" alt="FI UAEM"  height="40" class="" style="vertical-align:middle;">
    </div>

    <div class="ml-4 d-none d-md-inline d-lg-inline" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;">
      <img src="<?=WEB_ROUTE?>/img/svg/fi_invert.svg" alt="FI UAEM"  height="30" width="30" class="" style="vertical-align:middle;">
      <h3 class="d-none d-lg-inline" style="font-size:18px; font-weight:720; font-style:bold; color:#333333; vertical-align:middle;"> &nbsp; Sistema de Control de Acceso FI UAEM</h3>
      <h1 class="d-none d-md-inline d-lg-none" style="font-size:22px; font-weight:750; font-style:bold; color:#333333; vertical-align:middle;"> &nbsp; UAEM</h1>
    </div>

    <!-- Topbar Search -->
    <form class="d-none form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Buscar aqui..." aria-label="Search" aria-describedby="basic-addon2">
        <div class="input-group-append">
          <button class="btn btn-primary" type="button" style="background: #1C3D14; border: none;">
            <ion-icon name="search-outline" style="font-size: 20px; color:white; vertical-align: middle;"></ion-icon>
          </button>
        </div>
      </div>
    </form>

    <!-- Topbar Navbar -->
    <ul class="navbar-nav ml-auto">
      <!-- Nav Item - Search Dropdown (Visible Only XS) -->
      <li class="nav-item dropdown no-arrow d-none">
        <a class="nav-link dropdown-toggle" href="" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <ion-icon name="search-outline" style="font-size: 20px; color:#2d3436; vertical-align: middle;"></ion-icon>
        </a>
        <!-- Dropdown - Messages -->
        <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
          <form class="form-inline mr-auto w-100 navbar-search">
            <div class="input-group">
              <input type="text" class="form-control small" placeholder="Buscar aquí" aria-label="Search" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-primary" type="button" style="background: #1C3D14; border: none;">
                  <ion-icon name="search-outline" style="font-size: 20px; color:white; vertical-align: middle;"></ion-icon>
                </button>
              </div>
            </div>
          </form>
        </div>
      </li>

      <!-- Nav Item - Alerts -->
      <li class="nav-item dropdown no-arrow mx-1 d-none">
        <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <ion-icon name="notifications-outline" style="font-size: 25px; color: #5a5c69;"></ion-icon>
          <!-- Counter - Alerts -->
          <span class="badge badge-danger badge-counter">3+</span>
        </a>
        <!-- Dropdown - Alerts -->
        <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
          <h6 class="dropdown-header">
            Alerts Center
          </h6>
          <!--
          <a class="dropdown-item d-flex align-items-center" href="#">
            <div class="mr-3">
              <div class="icon-circle bg-primary">
                <i class="fas fa-file-alt text-white"></i>
              </div>
            </div>
            <div>
              <div class="small text-gray-500">December 12, 2019</div>
              <span class="font-weight-bold">A new monthly report is ready to download!</span>
            </div>
          </a>
          -->
          <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
        </div>
      </li>

      <!-- Nav Item - Messages -->
      <li class="nav-item dropdown no-arrow mx-1 d-none">
        <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <ion-icon name="mail-outline" style="font-size: 25px; color: #5a5c69;"></ion-icon>
          <!-- Counter - Messages -->
          <span class="badge badge-danger badge-counter">7</span>
        </a>
        <!-- Dropdown - Messages -->
        <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">
          <h6 class="dropdown-header">
            Message Center
          </h6>
          <!--
          <a class="dropdown-item d-flex align-items-center" href="#">
            <div class="dropdown-list-image mr-3">
              <img class="rounded-circle" src="https://source.unsplash.com/fn_BT9fwg_E/60x60" alt="">
              <div class="status-indicator bg-success"></div>
            </div>
            <div class="font-weight-bold">
              <div class="text-truncate">Hi there! I am wondering if you can help me with a problem I've been having.</div>
              <div class="small text-gray-500">Emily Fowler · 58m</div>
            </div>
          </a>
          -->
          <a class="dropdown-item text-center small text-gray-500" href="#">Read More Messages</a>
        </div>
      </li>

      <div class="topbar-divider d-none d-sm-block"></div>
      <!-- Nav Item - User Information -->
      <li class="nav-item dropdown no-arrow" data-toggle="tooltip" data-placement="bottom" title="<?=$message?>">
        <a class="nav-link dropdown-toggle" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <span class="mr-2 d-none d-lg-inline" style="color:#333333; font-size:15px;" id="user_tag" data-userid="<?= $_SESSION['id_usuario']?>"><?= $_SESSION['username']?></span>
          <ion-icon name="person-circle-outline" style="color:#444444;font-size:26px;"></ion-icon>
          <?php if($access): ?>
            <span class="badge badge-pill bg-green-sidebar text-white badge-counter"><i class="fas fa-check" style="font-size:11px;"></i></span>
          <?php else: ?>
            <span class="badge badge-pill badge-danger badge-counter">-</span>
          <?php endif; ?>
        </a>
        <!-- Dropdown - User Information -->
        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">

          <a class="dropdown-item" href="<?=WEB_ROUTE.$profile_route?>" data-toggle="tooltip" data-placement="bottom" title="Tu perfil">
            <ion-icon name="person-outline" style="font-size: 20px; vertical-align: middle; padding-right:5px;"></ion-icon>
            Perfil
          </a>
          <a class="dropdown-item" data-toggle="modal" data-target="#statusModal">
            <ion-icon name="bookmark-outline"  style="font-size: 20px; vertical-align: middle; padding-right:5px;"></ion-icon>
            Tu estatus &nbsp;
            <?php if($access): ?>
              <span class="badge badge-pill bg-green-sidebar text-white">Con acceso</span>
            <?php else: ?>
              <span class="badge badge-pill badge-danger">Sin acceso</span>
            <?php endif; ?>
          </a>
          <a class="dropdown-item" data-toggle="tooltip" data-placement="bottom" title="Ayuda">
            <ion-icon name="help-circle-outline" style="font-size: 20px; vertical-align: middle; padding-right:5px;"></ion-icon>
            Ayuda
          </a>
          <div class="dropdown-divider"></div>
          <div data-toggle="tooltip" data-placement="bottom" title="Cerrar sesión">
            <a class="dropdown-item" data-toggle="modal" data-target="#logoutModal">
              <ion-icon name="log-out-outline" style="font-size: 20px; vertical-align: middle; padding-right:5px;"></ion-icon>
              Cerrar sesión
            </a>
          </div>
        </div>
      </li>
      <div class="topbar-divider d-none d-md-inline"></div>
      <li class="nav-item d-none d-md-inline" style="padding-top:10px">
        <a href="" class="d-flex flex-row">
          <img class="d-inline" src="<?=WEB_ROUTE?>/img/svg/logo_fi_uaem.svg" alt="Facultad de Ingenieria UAEM" height="45">
        </a>
      </li>
    </ul>
</nav>
<!-- End of Topbar -->

<div class="modal fade border-0" id="statusModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">>
  <div class="modal-dialog modal-dialog-centered border-0">
    <div class="modal-content border-0">
      <div class="modal-header border-0">
        <h6 class="modal-title"><strong>Tu estatus</strong></h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center border-0">
        <?php if( $status['status'] == "1" ): ?>
          <h4 class="text-danger"><strong>Sin acceso</strong></h4>
          <img src="<?=WEB_ROUTE?>/img/png/wrong.png" alt="Sin acceso" width="150" height="150">
          <div class="row bg-warning p-1 mb-3 mt-4 mx-3" id="row_warn" style="border-radius:4px;">
            <div class="col-2 mr-0 pr-0 text-center d-flex align-items-center">
              <ion-icon name="warning-outline" style="font-size: 27px; vertical-align: middle;"></ion-icon>
            </div>
            <div class="col-10 ml-0 pl-0 d-flex align-items-center" style="font-size:15px; text-align:start;">
              Fuiste registrado como caso sospechoso de COVID-19
            </div>
          </div>
          <p class="mx-3 text-secondary mb-0 pb-0" style="font-size:13px;">Si diste positivo en COVID te pedimos quedarte en casa. Sigue las indicaciones de tus médicos. El personal de nuestra Facultad te dará indicaciones para incorporarte nuevamente cuando recibas el alta médica.</p>
        <?php else: ?>
            <?php if( !empty($result) ): ?>
              <h4 class="mb-3 text-green-sidebar"><strong>Con acceso</strong></h4>
              <img src="<?=WEB_ROUTE?>/img/png/tick.png" alt="Tienes acceso" width="150" height="150">
              <div class="row mt-4 mb-2 mx-2">
                <div class="col-4 mr-0 p-1 info-left">Hora:</div>
                <div class="col-8 info-right"><?=$result['hora_reg']?></div>
              </div>
              <div class="row mb-2 mx-2">
                <div class="col-4 mr-0 p-1 info-left">Fecha:</div>
                <div class="col-8 info-right"><?=$result['fecha_reg']?></div>
              </div>
              <div class="row mx-2">
               <div class="col-4 mr-0 p-1 info-left">Punto de control:</div>
                <div class="col-8 info-right"><?=$result['punto_control']?></div>
              </div>
            <?php else: ?>
              <h4 class="text-danger mb-3"><strong>Sin acceso</strong></h4>
              <img src="<?=WEB_ROUTE?>/img/png/wrong.png" alt="Sin acceso" width="150" height="150">
              <p class="m-0 p-0 mt-4"><strong>No has pasado por ningún punto de control</strong></p>
            <?php endif; ?>
        <?php endif; ?>
      </div>
      <div class="modal-footer border-0">
      </div>
    </div>
  </div>
</div>