<?php
    include('routes.php');
    include(SERVER_ROUTE.'/database.php');
    $tipo_registro = $_POST['tipo_registro'];
    if ($tipo_registro == "3") { // Registro manual de persona ya registrada
        $id_persona = $_POST['id_persona'];
        $id_cp = $_POST['id_cp'];
        $id_encargado = $_POST['id_encargado'];
        $detalles = $_POST['detalles'];
        $records = $connection->prepare('INSERT INTO caso_sospechoso(id_persona,id_cp,id_encargado,detalles) VALUES(:id_persona,:id_cp,:id_encargado,:detalles);');
        $records->bindParam('id_persona',$id_persona);
        $records->bindParam('id_cp',$id_cp);
        $records->bindParam('id_encargado',$id_encargado);
        $records->bindParam('detalles',$detalles);
        if ($records->execute()) {
            $records = $connection->prepare('UPDATE persona SET status = 1 WHERE persona.id_persona = :id_persona');
            $records->bindParam('id_persona',$id_persona);
            if ($records->execute()) {
                $res = array("status" => 202, "message" => 'Caso sospechoso registrado');
                echo json_encode($res);
            }
            else {
                $res = array("status" => 404, "message" => 'No se ha podido registrar el acceso del usuario. Parece que el servidor esta teniendo problemas. Intentalo de nuevo dentro de unos minutos.');
                echo json_encode($res);
            }
        }
        else {
            $res = array("status" => 404, "message" => 'No se ha podido registrar el acceso del usuario. Parece que el servidor esta teniendo problemas. Intentalo de nuevo dentro de unos minutos.');
            echo json_encode($res);
        }
    }
    else if ($tipo_registro == "4") { // Registro manual de persona externa
        $externo_nombre = $_POST['externo_nombre'];
        $detalles = $_POST['externo_detalles'];
        $id_encargado = $_POST['id_encargado'];
        $id_cp = $_POST['id_cp'];
        $externo_email = $_POST['externo_email'];
        $externo_tel = $_POST['externo_tel'];
        $query1 = "INSERT INTO persona(nombre,detalles_visita,tipo_persona";
        $query2 = ") VALUES ('".$externo_nombre."','".$externo_motivo."',5";
        if ($externo_email != '')
        {
            $query1 .= ",email";
            $query2 .= ",'".$externo_email."'";
        }
        if ($externo_tel != '')
        {
            $query1 .= ",telefono";
            $query2 .= ",'".$externo_tel."'";
        }
        $query = $query1.$query2.");";
        $records = $connection->prepare($query);
        if ( $records->execute() ) {
            $last_id = $connection->lastInsertId();
            $records = $connection->prepare('INSERT INTO caso_sospechoso(id_persona,id_cp,id_encargado,detalles) VALUES(:id_persona,:id_cp,:id_encargado,:detalles);');
            $records->bindParam('id_persona',$last_id);
            $records->bindParam('id_cp',$id_cp);
            $records->bindParam('id_encargado',$id_encargado);
            $records->bindParam('detalles',$detalles);
            if ( $records->execute() ) {
                $records = $connection->prepare('UPDATE persona SET status = 1 WHERE persona.id_persona = :id_persona');
                $records->bindParam('id_persona',$last_id);
                if ($records->execute()) {
                    $res = array("status" => 202, "message" => 'Caso sospechoso registrado');
                    echo json_encode($res);
                }
                else {
                    $res = array("status" => 404, "message" => 'No se ha podido registrar el acceso del usuario. Parece que el servidor esta teniendo problemas. Intentalo de nuevo dentro de unos minutos.');
                    echo json_encode($res);
                }
            }
            else
            {
                $res = array("status" => 404, "message" => 'No se reconoce el tipo de registro que intentas realizar!');
                echo json_encode($res);
            }
        }
        else {
            $res = array("status" => 404, "message" => 'No se reconoce el tipo de registro que intentas realizar!');
            echo json_encode($res);
        }
    }
?>