<?php
    include('../routes.php');
    include(SERVER_ROUTE.'/database.php');
    $new_id=$_POST['new_id'];
    $new_email=$_POST['new_email'];
    $new_tel=$_POST['new_tel'];
    $new_password=$_POST['new_password'];
    $new_nombre=$_POST['new_nombre'];
    $tipo_persona = $_POST['tipo_persona'];
    $records = $connection->prepare('INSERT INTO persona(id_persona,email,telefono,password,nombre,tipo_persona) VALUES(:new_id, :new_email, :new_tel,:new_password,:new_nombre,:tipo_persona);');
    $records->bindParam('new_id', $new_id);
    $records->bindParam('new_email', $new_email);
    $records->bindParam('new_tel', $new_tel);
    $records->bindParam('new_password', $new_password);
    $records->bindParam('new_nombre', $new_nombre);
    $records->bindParam('tipo_persona', $tipo_persona);
    if ($records->execute()) {
        $last_id = $connection->lastInsertId();
        $records = $connection->prepare('SELECT id_persona, email, telefono, password, nombre, tipo_persona FROM persona WHERE id_persona = :id_persona;');
        $records->bindParam('id_persona', $last_id);
        if ($records->execute()) {
            $result = $records->fetch(PDO::FETCH_ASSOC);
            $res = array(
            "status" => 202,
            "message" => "Usuario creado exitosamente",
            "id_persona" => $result['id_persona'],
            "email" => $result['email'],
            "telefono"=> $result['telefono'],
            "password"=>$result['password'],
            "nombre"=>$result['nombre'],
            "tipo_persona"=>$result['tipo_persona']
        );
            echo json_encode($res);
        } else {
            $res = array("status" => 404, "message" => 'No se pudo realizar el registro. Parece que el servidor esta tenido problemas. Intenta realizar la operacion mas tarde');
            echo json_encode($res);
        }
    } else {
        $res = array("status" => 404, "message" => 'No se pudo realizar el registro. Parece que el servidor esta tenido problemas. Intenta realizar la operacion mas tarde');
        echo json_encode($res);
    }
