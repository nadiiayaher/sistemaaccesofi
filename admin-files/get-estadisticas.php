<?php
    include('../routes.php');
    include(SERVER_ROUTE.'/database.php');
    include(SERVER_ROUTE.'/utilities/get_stats.php');
    session_start();
    if(isset($_SESSION['id_usuario']))
    {
        switch($_SESSION['tipo_persona'])
        {
            case 1: // Tipo de usuario admin 
                $first_day = $_POST['first_day'];
                $last_day  = $_POST['last_day'];
                $actual_first_day = $_POST['actual_first_day'];
                $actual_last_day = $_POST['actual_last_day'];

                $first_day_time = strtotime($first_day);
                $last_day_time = strtotime($last_day);
                $actual_first_day_time = strtotime($actual_first_day);
                $actual_last_day_time = strtotime($actual_last_day);
                
                if( $first_day_time <= $last_day_time ) {
                    if ( $first_day_time <= $actual_last_day_time ) {
                        $res = getStats($first_day,$last_day,$connection);
                        echo json_encode($res);
                    }
                    else
                    {
                        $res = array(
                            "status" => 404,
                            "title" => "No se pudieron obtener las estadísticas",
                            "message" => "A menos que viajes al futuro, estos datos no estan disponibles",
                        );
                        echo json_encode($res);
                    }
                }
                else
                {
                    $res = array(
                        "status" => 404,
                        "title" => "No se pudieron obtener las estadísticas",
                        "message" => "La fecha final debe ser mayor o igual a la fecha incial",
                    );
                    
                    echo json_encode($res);
                }
                break;
            case 2: // Tipo de usuario encargado de cp
                $res = array("status" => 404, "message" => 'No se pudo realizar la operación. No se ha podido confirmar tu idetidad');
                echo json_encode($res);
                break;
            case 3: // Tipo de usuario profesor
                $res = array("status" => 404, "message" => 'No se pudo realizar la operación. No se ha podido confirmar tu idetidad');
                echo json_encode($res);
                break;
            default:
                $res = array("status" => 404, "message" => 'No se pudo realizar la operación. No se ha podido confirmar tu idetidad');
                echo json_encode($res);
                break;
        }
    }
    else
    {
        $res = array("status" => 404, "message" => 'No se pudo realizar la operación. No se ha podido confirmar tu idetidad');
        echo json_encode($res);
    }
?>