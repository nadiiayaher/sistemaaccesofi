<?php
    include('../routes.php');
    include(SERVER_ROUTE.'/database.php');
    include(SERVER_ROUTE.'/vendor/autoload.php');
    use Endroid\QrCode\QrCode;
    date_default_timezone_set('America/Tegucigalpa');
    $date_now = date('d-m-Y');

    $nombre = $_POST['nombre'];
    $records = $connection->prepare('INSERT INTO punto_control(nombre) VALUES(:nombre);');
    $records->bindParam('nombre',$nombre);
    if($records->execute()) {
        $last_id = $connection->lastInsertId();
        $res = str_replace(' ','-',$nombre);
        $desc = $last_id.'_'.$date_now.'_'.$res;

        $qrCode = new QrCode($desc);
        $qrCode->setSize(300);
        $image = $qrCode->writeString();
        $imageData = base64_encode($image);

        $records = $connection->prepare('UPDATE punto_control SET qrcode = :qrcode, img_text = :img_text WHERE id_cp = :id_cp;');
        $records->bindParam('qrcode',$desc);
        $records->bindParam('img_text',$imageData);
        $records->bindParam('id_cp',$last_id);
        $records->execute();

        $records = $connection->prepare('SELECT id_cp,nombre FROM punto_control WHERE id_cp = :last_id;');
        $records->bindParam('last_id',$last_id);
        $records->execute();
        $result = $records->fetch(PDO::FETCH_ASSOC);
        $res = array(
            "status" => 202,
            "message" => "Punto de control creado exitosamente",
            "id_cp" => $result['id_cp'],
            "nombre" => $result['nombre']
        );
        echo json_encode($res);
    }
    else{
        $res = array("status" => 404, "message" => 'No se pudo realizar la operacion. Parece que el servidor esta tenido problemas. Intenta realizar la operacion mas tarde');
        echo json_encode($res);
    }
?>