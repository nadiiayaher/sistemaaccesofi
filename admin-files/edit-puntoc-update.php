<?php
    include('../routes.php');
    include(SERVER_ROUTE.'/database.php');
    include(SERVER_ROUTE.'/vendor/autoload.php');
    use Endroid\QrCode\QrCode;
    date_default_timezone_set('America/Tegucigalpa');
    $date_now = date('d-m-Y');

    $id_cp = $_POST['id_cp'];
    $descripcion = $_POST['descripcion'];
    $desc_real = $descripcion;
    $records = $connection->prepare('UPDATE punto_control SET descripcion = :descripcion WHERE id_cp = :id_cp;');
    $records->bindParam('descripcion',$descripcion);
    $records->bindParam('id_cp',$id_cp);
    if( $records->execute() ){

        $res = str_replace(' ','-',$desc_real);
        $desc = $id_cp.'_'.$date_now.'_'.$res;

        $qrCode = new QrCode($desc);
        $qrCode->setSize(300);
        $image = $qrCode->writeString();
        $imageData = base64_encode($image);

        $records = $connection->prepare('UPDATE punto_control SET qrcode = :qrcode, img_text = :img_text WHERE id_cp = :id_cp;');
        $records->bindParam('qrcode',$desc);
        $records->bindParam('img_text',$imageData);
        $records->bindParam('id_cp',$id_cp);
        if($records->execute()) {
            $records = $connection->prepare('SELECT id_cp, descripcion FROM punto_control;');
        $records->execute();
        $cps = json_encode($records->fetchAll());
        $res = array(
            "status" => 202,
            "message" => "Se editaron los datos exitosamente!",
            "cps" => $cps
        );
        echo json_encode($res);
        }
        else{
            $res = array("status" => 404, "message" => 'No se pudo realizar la operacion. Parece que el servidor esta tenido problemas. Intenta realizar la operacion mas tarde');
            echo json_encode($res);
        }
    } else{
        $res = array("status" => 404, "message" => 'No se pudo realizar la operacion. Parece que el servidor esta tenido problemas. Intenta realizar la operacion mas tarde');
        echo json_encode($res);
    }
?>