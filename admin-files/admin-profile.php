<?php
  include('../routes.php');
  session_start();
  if(isset($_SESSION['id_usuario']))
  {
      switch($_SESSION['tipo_persona'])
      {
          case 1: // Tipo de usuario admin
              break;
          case 2: // Tipo de usuario encargado de cp
              header('Location: '.WEB_ROUTE.'/empleado.php');
              break;
          case 3: // Tipo de usuario profesor
              header('Location: '.WEB_ROUTE.'/profesor.php');
              break;
          default:
            header('Location: '.WEB_ROUTE.'/index.php');
            break;
      }
  }
  else
  {
      header('Location: '.WEB_ROUTE.'/index.php');
  }
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Admin - Sistema de control de acceso FI UAEM</title>
  <!-- Templates and librarys-->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
  <link href="<?=WEB_ROUTE?>/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link rel="icon" type="image/png" href="<?=WEB_ROUTE?>/favicon.png"/>
  <link rel="stylesheet" href="<?=WEB_ROUTE?>/toastr/build/toastr.css">
  <link href="<?=WEB_ROUTE?>/css/standard-style.css" rel="stylesheet">
  <link rel="stylesheet" href="<?=WEB_ROUTE?>/css/admin-style.css">
</head>
<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    <!-- Sidebar -->
    <ul class="navbar-nav sidebar accordion" id="accordionSidebar">
      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
        <div class="sidebar-brand-icon">
          <img src="<?=WEB_ROUTE?>/img/png/uaem-logo.png" alt="Universidad Autonoma del Estado de Mexico" width="57px" height="50px" style="border-radius:3px;">
        </div>
        <div class="sidebar-brand-text mx-2">UAEM</div>
      </a>
      <!-- Divider -->
      <hr class="sidebar-divider my-0">
      <!-- Nav Item - Dashboard -->
      <li class="nav-item">
        <a class="nav-link" href="<?=WEB_ROUTE?>/admin.php">
          <ion-icon name="home-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Inicio</span>
        </a>
      </li>
      <!-- Divider -->
      <hr class="sidebar-divider">
      <!-- Heading -->
      <div class="sidebar-heading">
        Operaciones
      </div>
      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <ion-icon name="people-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Usuarios</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-light py-2 collapse-inner rounded">
            <h6 class="collapse-header">Tipo de usuario:</h6>
            <a class="collapse-item" href="<?=WEB_ROUTE?>/admin-files/alumno_data.php">Alumnos</a>
            <a class="collapse-item" href="<?=WEB_ROUTE?>/admin-files/profesor_data.php">Profesores</a>
            <a class="collapse-item" href="<?=WEB_ROUTE?>/admin-files/trabajador_data.php" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;">Encargados de puntos de control</a>
            <a class="collapse-item" href="<?=WEB_ROUTE?>/admin-files/empleado_data.php">Otros empleados</a>
            <a class="collapse-item" href="<?=WEB_ROUTE?>/admin-files/invitado_data.php">Usuarios externos</a>
            <a class="collapse-item" href="<?=WEB_ROUTE?>/admin-files/admin_data.php">Administradores</a>
          </div>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="" data-toggle="collapse" data-target="#collapseCatalogos" aria-expanded="true" aria-controls="collapseCatalogos">
          <ion-icon name="browsers-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Catálogos</span>
        </a>
        <div id="collapseCatalogos" class="collapse" aria-labelledby="headingCatalogos" data-parent="#accordionSidebar">
          <div class="bg-light py-2 collapse-inner rounded">
            <h6 class="collapse-header">Catálogos disponibles:</h6>
            <a class="collapse-item" href="">Grupos</a>
            <a class="collapse-item" href="<?=WEB_ROUTE?>/admin-files/materia_data.php">Materias</a>
          </div>
        </div>
      </li>
      <!-- Nav Item - Tables -->
      <li class="nav-item">
        <a class="nav-link" href="<?=WEB_ROUTE?>/admin-files/puntoc_data.php">
          <ion-icon name="contract-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Puntos de control</span>
        </a>
      </li>
      <hr class="sidebar-divider">
      <div class="sidebar-heading">
        Búsquedas
      </div>
      <li class="nav-item">
        <a class="nav-link" href="<?=WEB_ROUTE?>/admin-files/admin-busqueda.php">
          <ion-icon name="search-circle-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Busqueda avanzada</span>
        </a>
      </li>
      <hr class="sidebar-divider">
      <div class="sidebar-heading">
        Estadísticas y reportes
      </div>
      <li class="nav-item">
        <a class="nav-link" href="">
          <ion-icon name="bar-chart-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Estadísticas</span>
        </a>
      </li>
      <hr class="sidebar-divider">
      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>
    </ul>
    <!-- End of Sidebar -->
    
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">

      <?php include(SERVER_ROUTE.'/templates/navbar.php');?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

        <nav aria-label="breadcrumb" style="font-size: 14px; margin-bottom:30px;">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?=WEB_ROUTE?>/admin.php" style="color: #958419;">Inicio</a></li>
            <li class="breadcrumb-item active" aria-current="page"><a href="">Tu perfil</a></li>
          </ol>
        </nav>

        <div class="row">
              <div class="col-sm-12 col-md-10 mx-auto">
                <h4 class="mb-3">Tu perfil</h4>
                <div class="card card-content shadow mb-4">
                  <div class="card-body">
                        <div class="row no-gutters align-items-center mt-3 mb-0">
                          <div class="col-md-3">
                            <div class="text-center">
                              <ion-icon name="person-outline" style="font-size:90px; color:#555555;"></ion-icon>
                            </div>
                          </div>
                          <div class="col-md-9 mt-2">

                            <div class="form-group row">
                              <label for="user_name" class="col-md-3 ml-0 col-form-label profile-label">Nombre: </label>
                              <div class="col-md-8">
                                <input type="text" class="form-control border-0" id="user_name" value="<?=$_SESSION['username']?>" disabled>
                              </div>
                            </div>

                            <div class="form-group row">
                              <label for="user_name" class="col-md-3 ml-0 col-form-label profile-label">Matrícula: </label>
                              <div class="col-md-8">
                                <input type="number" class="form-control border-0" id="user_id" value="<?=$_SESSION['identificador']?>" disabled>
                              </div>
                            </div>

                            <div class="form-group row">
                              <label for="user_email" class="col-md-3 ml-0 col-form-label profile-label">Email: </label>
                              <div class="col-md-8">
                                <input type="text" class="form-control border-0" id="user_email" value="<?=$_SESSION['user_email']?>" disabled>
                              </div>
                            </div>

                            <div class="form-group row">
                              <label for="user_type" class="col-md-3 ml-0 col-form-label profile-label">Tipo de usuario: </label>
                              <div class="col-md-8">
                                <input type="text" class="form-control border-0" id="user_type" value="<?=$_SESSION['tipo_persona_str']?>" disabled>
                              </div>
                            </div>

                            <div class="col-md-6 m-0 p-0 mt-4" id="first_btn">
                              <button class="btn btn-green-strong-invert" id="btn-chg" style="width:100%"> Cambiar contraseña</button>
                            </div>

                            <div class="form-group row d-none mt-4" id="pass_row">
                              <label for="user_new_pass" class="col-md-3 ml-0 col-form-label profile-label">Nueva contraseña: </label>
                              <div class="col-md-8 input-group align-items-center">
                                <input type="password" class="form-control form-input" id="user_new_pass">
                                <div class="input-group-append">
                                  <button class="btn btn-pass d-none" id="pass_see_btn" style="max-height:38px;">
                                    <ion-icon name="eye-outline" style="vertical-align:middle; font-size:22px;"></ion-icon>
                                  </button>
                                </div>
                              </div>
                              <div class="col-md-3"></div>
                              <div id="msg_pass" class="danger-msg ml-3 mt-1">
                                <!-- Error message here -->
                              </div>
                            </div>

                            <div class="form-group row d-none mt-0" id="confirm_row">
                              <label for="user_pass_confirm" class="col-md-3 ml-0 col-form-label profile-label">Confirma tu contraseña: </label>
                              <div class="col-md-8 input-group align-items-center">
                                <input type="password" class="form-control form-input" id="user_new_confirm">
                                <div class="input-group-append">
                                  <button class="btn btn-pass d-none" id="confirm_see_btn" style="max-height:38px;">
                                    <ion-icon name="eye-outline" style="vertical-align:middle; font-size:22px;"></ion-icon>
                                  </button>
                                </div>
                              </div>
                              <div class="col-md-3"></div>
                              <div id="msg_confirm" class="danger-msg ml-3">
                                <!-- Error message here -->
                              </div>
                            </div>

                            <div class="form-group row mb-0 d-none"  id="btns_row" style="margin-top:30px;">
                              <div class="col-sm-6 mb-2">
                                <button class="btn btn-green-strong" id="go_btn" style="width:100%">Guardar cambios</button>
                              </div>
                              <div class="col-sm-5">
                                <button class="btn btn btn-dark" id="cancel_chg_btn" style="width:100%">Cancelar</button>
                              </div>
                            </div>

                          </div>
                        </div>

                  </div>
                 </div>
              </div>
            </div>
        
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
    </div>
    <!-- End of Content Wrapper -->
  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php include(SERVER_ROUTE.'/templates/logout-modal.php'); ?>

  <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
  <script src="https://unpkg.com/ionicons@5.2.3/dist/ionicons.js"></script>
  <!-- Custom scripts-->
  <script src="<?=WEB_ROUTE?>/toastr/build/toastr.min.js"></script>
  <script src="<?=WEB_ROUTE?>/js/standard-func.js"></script>
  <script src="<?=WEB_ROUTE?>/js/admin.js"></script>
</body>
</html>