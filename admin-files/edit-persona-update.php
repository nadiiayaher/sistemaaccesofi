<?php
    include('../routes.php');
    include(SERVER_ROUTE.'/database.php');
    $id_persona=$_POST['id_persona'];
    $email=$_POST['email'];
    $telefono=$_POST['telefono'];
    $password=$_POST['password'];
    $nombre=$_POST['nombre'];
    $tipo_persona=$_POST['tipo_persona'];
    $records = $connection->prepare('UPDATE persona SET email = :email, telefono = :telefono, password = :password, nombre = :nombre, id_persona = :id_persona WHERE id_persona = :id_persona;');
    $records->bindParam('id_persona',$id_persona);
    $records->bindParam('email',$email);
    $records->bindParam('telefono',$telefono);
    $records->bindParam('password',$password);
    $records->bindParam('nombre',$nombre);
    if( $records->execute() ){
        $records = $connection->prepare('SELECT id_persona,nombre, email, telefono FROM persona WHERE tipo_persona = :tipo_persona;');
        $records->bindParam('tipo_persona',$tipo_persona);
        $records->execute();
        $personas = json_encode($records->fetchAll());
        $res = array(
            "status" => 202,
            "message" => "Se editaron los datos del usuario exitosamente!",
            "personas" => $personas
        );
        echo json_encode($res);
    } else{
        $res = array("status" => 404, "message" => 'No se pudo realizar el registro. Parece que el servidor esta tenido problemas. Intenta realizar la operacion mas tarde');
        echo json_encode($res);
    }
?>