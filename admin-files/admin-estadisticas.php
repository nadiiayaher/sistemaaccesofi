<?php
include('../routes.php');
include(SERVER_ROUTE.'/database.php');
include(SERVER_ROUTE.'/utilities/date_manipulation.php');
session_start();
if(isset($_SESSION['id_usuario']))
{
    switch($_SESSION['tipo_persona'])
    {
        case 1: // Tipo de usuario admin
            break;
        case 2: // Tipo de usuario encargado de cp
            header('Location: '.WEB_ROUTE.'/empleado.php');
            break;
        case 3: // Tipo de usuario profesor
            header('Location: '.WEB_ROUTE.'/profesor.php');
            break;
        default:
          header('Location: '.WEB_ROUTE.'/index.php');
          break;
    }
}
else
{
    header('Location: '.WEB_ROUTE.'/index.php');
}

date_default_timezone_set('America/Tegucigalpa');
$date_now = date('Y-m-d');
$actual_month = date('m');
$months = getMonths($actual_month);
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Admin - Sistema de control de acceso FI UAEM</title>
  <!-- Templates and librarys-->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
  <link href="<?=WEB_ROUTE?>/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link rel="icon" type="image/png" href="<?=WEB_ROUTE?>/img/png/fi_invert.png"/>
  <link href="<?=WEB_ROUTE?>/js/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
  <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
  <link href="<?=WEB_ROUTE?>/css/standard-style.css" rel="stylesheet">
  <link rel="stylesheet" href="<?=WEB_ROUTE?>/css/admin-style.css">
</head>
<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    <!-- Sidebar -->
    <ul class="navbar-nav sidebar accordion" id="accordionSidebar">
      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
        <div class="sidebar-brand-icon">
          <img src="<?=WEB_ROUTE?>/img/png/uaem-logo.png" alt="Universidad Autonoma del Estado de Mexico" width="57px" height="50px" style="border-radius:3px;">
        </div>
        <div class="sidebar-brand-text mx-2">UAEM</div>
      </a>
      <!-- Divider -->
      <hr class="sidebar-divider my-0">
      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="<?=WEB_ROUTE?>/admin.php">
          <ion-icon name="home-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Inicio</span>
        </a>
      </li>
      <!-- Divider -->
      <hr class="sidebar-divider">
      <!-- Heading -->
      <div class="sidebar-heading">
        Operaciones
      </div>
      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <ion-icon name="people-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Usuarios</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-light py-2 collapse-inner rounded">
            <h6 class="collapse-header">Tipo de usuario:</h6>
            <a class="collapse-item" href="<?=WEB_ROUTE?>/admin-files/alumno_data.php">Alumnos</a>
            <a class="collapse-item" href="<?=WEB_ROUTE?>/admin-files/profesor_data.php">Profesores</a>
            <a class="collapse-item" href="<?=WEB_ROUTE?>/admin-files/trabajador_data.php" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;">Encargados de puntos de control</a>
            <a class="collapse-item" href="<?=WEB_ROUTE?>/admin-files/empleado_data.php">Otros empleados</a>
            <a class="collapse-item" href="<?=WEB_ROUTE?>/admin-files/invitado_data.php">Usuarios externos</a>
            <a class="collapse-item" href="<?=WEB_ROUTE?>/admin-files/admin_data.php">Administradores</a>
          </div>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="" data-toggle="collapse" data-target="#collapseCatalogos" aria-expanded="true" aria-controls="collapseCatalogos">
          <ion-icon name="browsers-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Catálogos</span>
        </a>
        <div id="collapseCatalogos" class="collapse" aria-labelledby="headingCatalogos" data-parent="#accordionSidebar">
          <div class="bg-light py-2 collapse-inner rounded">
            <h6 class="collapse-header">Catálogos disponibles:</h6>
            <a class="collapse-item" href="">Grupos</a>
            <a class="collapse-item" href="<?=WEB_ROUTE?>/admin-files/materia_data.php">Materias</a>
          </div>
        </div>
      </li>
      <!-- Nav Item - Tables -->
      <li class="nav-item">
        <a class="nav-link" href="<?=WEB_ROUTE?>/admin-files/puntoc_data.php">
          <ion-icon name="contract-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Puntos de control</span>
        </a>
      </li>
      <hr class="sidebar-divider">
      <div class="sidebar-heading">
        Búsquedas
      </div>
      <li class="nav-item">
        <a class="nav-link" href="<?=WEB_ROUTE?>/admin-files/admin-busqueda.php">
          <ion-icon name="search-circle-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Busqueda avanzada</span>
        </a>
      </li>
      <hr class="sidebar-divider">
      <div class="sidebar-heading">
        Estadísticas y reportes
      </div>
      <li class="nav-item">
        <a class="nav-link" href="">
          <ion-icon name="bar-chart-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Estadísticas</span>
        </a>
      </li>
      <hr class="sidebar-divider">
      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>
    </ul>
    <!-- End of Sidebar -->
    
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">

      <?php include(SERVER_ROUTE.'/templates/navbar.php');?>

        <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- Content Row -->
          <div class="row">
            <div class="col-md-12 mx-auto mt-3">
              <!-- Page Heading -->
              <h4 class="" style="font-weight:600;">Estadísticas</h4>
            </div>

            <div class="col-md-12 mx-auto mb-4">
                <hr>
                <ul class="nav nav-pills" id="pills-tab" role="tablist">
                    <li class="nav-item tab-item-stat text-center" role="presentation">
                        <a class="nav-link tab-link-stat active" id="pills-day-tab" data-toggle="pill" href="#pills-day" role="tab" aria-controls="pills-day" aria-selected="true">Día</a>
                    </li>
                    <li class="nav-item tab-item-stat text-center" role="presentation">
                        <a class="nav-link tab-link-stat" id="pills-week-tab" data-toggle="pill" href="#pills-week" role="tab" aria-controls="pills-week" aria-selected="false">Semana</a>
                    </li>
                    <li class="nav-item tab-item-stat text-center" role="presentation">
                        <a class="nav-link tab-link-stat" id="pills-month-tab" data-toggle="pill" href="#pills-month" role="tab" aria-controls="pills-month" aria-selected="false">Mes</a>
                    </li>
                    <li class="nav-item tab-item-stat text-center" role="presentation">
                        <a class="nav-link tab-link-stat" id="pills-year-tab" data-toggle="pill" href="#pills-year" role="tab" aria-controls="pills-year" aria-selected="false">Año</a>
                    </li>
                    <li class="nav-item tab-item-stat text-center" role="presentation">
                        <a class="nav-link tab-link-stat" id="pills-custom-tab" data-toggle="pill" href="#pills-custom" role="tab" aria-controls="pills-year" aria-selected="false">Personalizado</a>
                    </li>
                </ul>
                <hr>
                <!-- Pills container -->
                <div class="tab-content mb-4" id="pills-tabContent">

                  <!-- Day tab pane -->
                  <div class="tab-pane fade show active" id="pills-day" role="tabpanel" aria-labelledby="pills-day-tab">
                        
                    <div class="d-sm-flex justify-content-end mb-2">
                        <a href="<?=WEB_ROUTE?>" class="btn btn-green-med-invert d-none d-inline-block" id="day_download_btn" style="font-size:15px;">
                            <ion-icon name="arrow-down-circle-outline" style="font-size: 22px; vertical-align: middle;"></ion-icon>
                            &nbsp; Descargar estadísticas
                        </a>
                    </div>
                    
                    <div class="form-row mt-4">
                      <div class="col-md-9">
                        <label class="" for="exampleFormControlInput1">Fecha de estadistícas:</label>
                        <input type="date" class="form-control form-input" id="input_day" value="<?=$date_now?>" min="2021-01-01" max="">
                      </div>
                      <div class="col-md-3">
                        <button class="btn btn-hov" id="day_stats_btn" type="submit" style="width:100%; margin-top:31px;">
                          <span class="d-inline">GENERAR &nbsp;</span>
                          <ion-icon name="arrow-forward-circle-outline" style="font-size: 25px; vertical-align: middle;"></ion-icon>
                        </button>
                      </div>
                    </div>
                    
                    <!-- Stats day container -->
                    <div class="row" id="stats-day-container">
                      <!-- Cards Stats container -->
                    </div>
                    <div class="row mt-2 d-none" id="tables-day-container">
                      <div class="col-md-12">
                          <ul class="nav nav-tabs border-0" id="myTab" role="tablist">
                              <li class="nav-item border-0" role="presentation">
                                  <a class="nav-link active border-0 accesos" id="accesos-day-tab" data-toggle="tab" href="#accesos-day-pane" role="tab" aria-controls="accesos-day-pane" aria-selected="true">Detalle de usuarios registrados</a>
                              </li>
                              <li class="nav-item border-0" role="presentation">
                                  <a class="nav-link border-0 casos" id="casos-day-tab" data-toggle="tab" href="#casos-day-pane" role="tab" aria-controls="casos-day-pane" aria-selected="false">Detalles de casos sospechosos registrados</a>
                              </li>
                          </ul>
                          <div class="tab-content bg-white" id="myTabContent" style="border-radius:4px;">
                              <div class="tab-pane fade show active bg-white shadow px-3 py-4" id="accesos-day-pane" role="tabpanel" aria-labelledby="accesos-day-tab">
                              </div>
                              <div class="tab-pane fade bg-white shadow px-3 py-4" id="casos-day-pane" role="tabpanel" aria-labelledby="casos-day-tab">
                              </div>
                          </div>
                      </div>
                    </div>
                    <!-- End of stats day container -->
                  </div>
                  <!-- End of day tab pane -->

                  <!-- Week tab pane -->
                  <div class="tab-pane fade" id="pills-week" role="tabpanel" aria-labelledby="pills-week-tab">
                        
                      <div class="d-sm-flex justify-content-end mb-2">
                          <button href="" class="btn btn-green-med-invert d-none d-inline-block" id="week_download_btn" style="font-size:15px;">
                              <ion-icon name="arrow-down-circle-outline" style="font-size: 22px; vertical-align: middle;"></ion-icon>
                              &nbsp; Descargar estadísticas
                          </button>
                      </div>
                      
                      <div class="form-row mt-4">
                        <div class="col-md-6">
                          <label class="" for="exampleFormControlInput1">Selecciona cualquier dia de la semana:</label>
                          <input type="date" class="form-control form-input" id="input_week" value="<?=$date_now?>" min="2021-01-01" max="">
                        </div>
                        <div class="col-md-2">
                          <label class="" for="exampleFormControlInput1">Día inicial:</label>
                          <input type="date" class="form-control form-input" id="disabled_week_first" disabled>
                        </div>
                        <div class="col-md-2">
                          <label class="" for="exampleFormControlInput1">Día final:</label>
                          <input type="date" class="form-control form-input" id="disabled_week_last" disabled>
                        </div>
                        <div class="col-md-2">
                          <button class="btn btn-hov btn-primary" id="week_stats_btn" type="" style="width:100%; margin-top:31px;">
                          <span class="d-none d-md-none d-lg-none d-xl-inline">GENERAR &nbsp;</span>
                          <ion-icon name="arrow-forward-circle-outline" style="font-size: 25px; vertical-align: middle;"></ion-icon>
                          </button>
                        </div>
                      </div>

                    <div class="row" id="stats-week-container">
                      <!-- Cards Stats container -->
                    </div>
                    <div class="row mt-2 d-none" id="tables-week-container">
                      <div class="col-md-12">
                          <ul class="nav nav-tabs border-0" id="myTab" role="tablist">
                              <li class="nav-item border-0" role="presentation">
                                  <a class="nav-link active border-0 accesos" id="accesos-week-tab" data-toggle="tab" href="#accesos-week-pane" role="tab" aria-controls="accesos-week-pane" aria-selected="true">Detalle de usuarios registrados</a>
                              </li>
                              <li class="nav-item border-0" role="presentation">
                                  <a class="nav-link border-0 casos" id="casos-week-tab" data-toggle="tab" href="#casos-week-pane" role="tab" aria-controls="casos-week-pane" aria-selected="false">Detalles de casos sospechosos registrados</a>
                              </li>
                          </ul>
                          <div class="tab-content bg-white" id="myTabContent" style="border-radius:4px;">
                              <div class="tab-pane fade show active bg-white shadow px-3 py-4" id="accesos-week-pane" role="tabpanel" aria-labelledby="accesos-week-tab">
                              </div>
                              <div class="tab-pane fade bg-white shadow px-3 py-4" id="casos-week-pane" role="tabpanel" aria-labelledby="casos-week-tab">
                              </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- End of week tab pane -->

                  <!-- Month tab pane -->
                  <div class="tab-pane fade" id="pills-month" role="tabpanel" aria-labelledby="pills-month-tab">
                    
                    <div class="d-sm-flex justify-content-end mb-2">
                      <button href="" class="btn btn-green-med-invert d-none d-inline-block" id="month_download_btn" style="font-size:15px;">
                        <ion-icon name="arrow-down-circle-outline" style="font-size: 22px; vertical-align: middle;"></ion-icon>
                        &nbsp; Descargar estadísticas
                      </button>
                    </div>
                      
                    <div class="form-row mt-4">
                      <div class="col-md-5">
                        <label class="" for="exampleFormControlInput1">Mes:</label>
                        <select class="custom-select form-input" id="input_month_month">
                          <option value="01" <?=$months[0]?> >Enero</option>
                          <option value="02" <?=$months[1]?> >Febrero</option>
                          <option value="03" <?=$months[2]?> >Marzo</option>
                          <option value="04" <?=$months[3]?> >Abril</option>
                          <option value="05" <?=$months[4]?> >Mayo</option>
                          <option value="06" <?=$months[5]?> >Junio</option>
                          <option value="07" <?=$months[6]?> >Julio</option>
                          <option value="08" <?=$months[7]?> >Agosto</option>
                          <option value="09" <?=$months[8]?> >Septiembre</option>
                          <option value="10" <?=$months[9]?> >Octubre</option>
                          <option value="11" <?=$months[10]?> >Noviembre</option>
                          <option value="12" <?=$months[11]?> >Diciembre</option>
                        </select>
                      </div>
                      <div class="col-md-4">
                        <label class="" for="exampleFormControlInput1">Año:</label>
                        <select class="custom-select form-input" id="input_month_year" value="2021-09-04">
                          <option selected>2021</option>
                          <option>2022</option>
                          <option>2023</option>
                          <option>2024</option>
                          <option>2025</option>
                        </select>
                      </div>
                      
                      <div class="col-md-3">
                        <button class="btn btn-hov btn-primary" id="month_stats_btn" type="submit" style="width:100%; margin-top:31px;">
                          <span class="d-inline">GENERAR &nbsp;</span>
                          <ion-icon name="arrow-forward-circle-outline" style="font-size: 25px; vertical-align: middle;"></ion-icon>
                        </button>
                      </div>
                    </div>

                    <div class="row" id="stats-month-container">
                      <!-- Cards Stats container -->
                    </div>
                    <div class="row mt-2 d-none" id="tables-month-container">
                      <div class="col-md-12">
                          <ul class="nav nav-tabs border-0" id="myTab" role="tablist">
                              <li class="nav-item border-0" role="presentation">
                                  <a class="nav-link active border-0 accesos" id="accesos-month-tab" data-toggle="tab" href="#accesos-month-pane" role="tab" aria-controls="accesos-month-pane" aria-selected="true">Detalle de usuarios registrados</a>
                              </li>
                              <li class="nav-item border-0" role="presentation">
                                  <a class="nav-link border-0 casos" id="casos-month-tab" data-toggle="tab" href="#casos-month-pane" role="tab" aria-controls="casos-month-pane" aria-selected="false">Detalles de casos sospechosos registrados</a>
                              </li>
                          </ul>
                          <div class="tab-content bg-white" id="myTabContent" style="border-radius:4px;">
                              <div class="tab-pane fade show active bg-white shadow px-3 py-4" id="accesos-month-pane" role="tabpanel" aria-labelledby="accesos-month-tab">
                              </div>
                              <div class="tab-pane fade bg-white shadow px-3 py-4" id="casos-month-pane" role="tabpanel" aria-labelledby="casos-month-tab">
                              </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- End of month tab pane -->

                   <!-- Year tab pane -->
                  <div class="tab-pane fade" id="pills-year" role="tabpanel" aria-labelledby="pills-year-tab">

                    <div class="d-sm-flex justify-content-end mb-2">
                      <button href="" class="btn btn-green-med-invert d-none d-inline-block" id="year_download_btn" style="font-size:15px;">
                        <ion-icon name="arrow-down-circle-outline" style="font-size: 22px; vertical-align: middle;"></ion-icon>
                        &nbsp; Descargar estadísticas
                      </button>
                    </div>
                      
                    <div class="form-row mt-4">
                      <div class="col-md-9">
                        <label class="" for="exampleFormControlInput1">Año:</label>
                        <select class="custom-select form-input" id="input_year">
                          <option selected>2021</option>
                          <option>2022</option>
                          <option>2023</option>
                          <option>2024</option>
                          <option>2025</option>
                        </select>
                      </div>
                      <div class="col-md-3">
                        <button class="btn btn-hov btn-primary" id="" type="submit" style="width:100%; margin-top:31px;">
                          <span class="d-inline">GENERAR &nbsp;</span>
                          <ion-icon name="arrow-forward-circle-outline" style="font-size: 25px; vertical-align: middle;"></ion-icon>
                        </button>
                      </div>
                    </div>

                    <div class="row" id="stats-year-container">
                      <!-- Cards Stats container -->
                    </div>
                    <div class="row mt-2 d-none" id="tables-year-container">
                      <div class="col-md-12">
                          <ul class="nav nav-tabs border-0" id="myTab" role="tablist">
                              <li class="nav-item border-0" role="presentation">
                                  <a class="nav-link active border-0 accesos" id="accesos-year-tab" data-toggle="tab" href="#accesos-year-pane" role="tab" aria-controls="accesos-year-pane" aria-selected="true">Detalle de usuarios registrados</a>
                              </li>
                              <li class="nav-item border-0" role="presentation">
                                  <a class="nav-link border-0 casos" id="casos-year-tab" data-toggle="tab" href="#casos-year-pane" role="tab" aria-controls="casos-year-pane" aria-selected="false">Detalles de casos sospechosos registrados</a>
                              </li>
                          </ul>
                          <div class="tab-content bg-white" id="myTabContent" style="border-radius:4px;">
                              <div class="tab-pane fade show active bg-white shadow px-3 py-4" id="accesos-year-pane" role="tabpanel" aria-labelledby="accesos-year-tab">
                              </div>
                              <div class="tab-pane fade bg-white shadow px-3 py-4" id="casos-year-pane" role="tabpanel" aria-labelledby="casos-year-tab">
                              </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- End of year tab pane -->

                  <!-- Custom tab pane -->
                  <div class="tab-pane fade" id="pills-custom" role="tabpanel" aria-labelledby="pills-custom-tab">
                    
                    <div class="d-sm-flex justify-content-end mb-2">
                      <button href="" class="btn btn-green-med-invert d-none d-inline-block" id="custom_download_btn" style="font-size:15px;">
                        <ion-icon name="arrow-down-circle-outline" style="font-size: 22px; vertical-align: middle;"></ion-icon>
                        &nbsp; Descargar estadísticas
                      </button>
                    </div>
                      
                    <div class="form-row mt-4">
                      <div class="col-md-5">
                        <label class="" for="exampleFormControlInput1">Día inicial:</label>
                        <input type="date" class="form-control form-input" id="input_custom_first" min="2021-01-01" max="">
                        <div class="danger-msg mt-1" id="first_day_msg"></div>
                      </div>
                      <div class="col-md-5">
                        <label class="" for="exampleFormControlInput1">Día final:</label>
                        <input type="date" class="form-control form-input" id="input_custom_last" min="2021-01-01" max="">
                        <div class="danger-msg mt-1" id="last_day_msg"></div>
                      </div>
                      <div class="col-md-2">
                        <button class="btn btn-hov btn-primary" id="custom-stats-btn" type="" style="width:100%; margin-top:31px;">
                          <span class="d-none d-md-none d-lg-none d-xl-inline">GENERAR &nbsp;</span>
                          <ion-icon name="arrow-forward-circle-outline" style="font-size: 25px; vertical-align: middle;"></ion-icon>
                        </button>
                      </div>
                    </div>
                  
                    <div class="row" id="stats-custom-container">
                      <!-- Cards Stats container -->
                    </div>
                    <div class="row mt-2 d-none" id="tables-custom-container">
                      <div class="col-md-12">
                          <ul class="nav nav-tabs border-0" id="myTab" role="tablist">
                              <li class="nav-item border-0" role="presentation">
                                  <a class="nav-link active border-0 accesos" id="accesos-custom-tab" data-toggle="tab" href="#accesos-custom-pane" role="tab" aria-controls="accesos-custom-pane" aria-selected="true">Detalle de usuarios registrados</a>
                              </li>
                              <li class="nav-item border-0" role="presentation">
                                  <a class="nav-link border-0 casos" id="casos-custom-tab" data-toggle="tab" href="#casos-custom-pane" role="tab" aria-controls="casos-custom-pane" aria-selected="false">Detalles de casos sospechosos registrados</a>
                              </li>
                          </ul>
                          <div class="tab-content bg-white" id="myTabContent" style="border-radius:4px;">
                              <div class="tab-pane fade show active bg-white shadow px-3 py-4" id="accesos-custom-pane" role="tabpanel" aria-labelledby="accesos-custom-tab">
                              </div>
                              <div class="tab-pane fade bg-white shadow px-3 py-4" id="casos-custom-pane" role="tabpanel" aria-labelledby="casos-custom-tab">
                              </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- End of custom tab pane -->
                </div>
                <!-- End of pills container -->
            </div>
          </div>
        </div>
        <!-- end of container-fluid -->
      </div>
      <!-- End of Main Content -->
    </div>
    <!-- End of Content Wrapper -->
  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php include(SERVER_ROUTE.'/templates/logout-modal.php'); ?>

  <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
  <script src="https://unpkg.com/ionicons@5.2.3/dist/ionicons.js"></script>
  <script src="<?=WEB_ROUTE?>/js/datatables/jquery.dataTables.min.js"></script>
  <script src="<?=WEB_ROUTE?>/js/datatables/dataTables.bootstrap4.min.js"></script>
  <script src="<?=WEB_ROUTE?>/js/moment.js"></script>
  <script src="<?=WEB_ROUTE?>/js/standard-func.js"></script>
  <script src="<?=WEB_ROUTE?>/js/admin/admin-stadistics.js"></script>
</body>
</html>