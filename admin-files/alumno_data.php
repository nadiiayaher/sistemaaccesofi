<?php
include('../routes.php');
include(SERVER_ROUTE.'/database.php');
session_start();
if(isset($_SESSION['id_usuario']))
{
    switch($_SESSION['tipo_persona'])
    {
        case 1: // Tipo de usuario admin
            break;
        case 2: // Tipo de usuario encargado de cp
            header('Location: '.WEB_ROUTE.'/empleado.php');
            break;
        case 3: // Tipo de usuario profesor
            header('Location: '.WEB_ROUTE.'/profesor.php');
            break;
        default:
          header('Location: '.WEB_ROUTE.'/index.php');
          break;
    }
}
else
{
    header('Location: '.WEB_ROUTE.'/index.php');
}
$pass_btn = 'd-none';
    $roundedSquares = 'rounded_squares';

$records = $connection->prepare('SELECT id_persona, email, telefono, nombre FROM persona WHERE tipo_persona=4;');
$records->execute();
$persona = $records->fetchAll();
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Admin - Sistema de control de acceso FI UAEM</title>
    <!-- Custom fonts for this template-->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
    <link href="<?=WEB_ROUTE?>/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link rel="icon" type="image/png" href="<?=WEB_ROUTE?>/img/png/fi_invert.png">
    <!-- Custom styles for this page -->
    <link href="<?=WEB_ROUTE?>/js/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link rel="stylesheet" href="<?=WEB_ROUTE?>/toastr/build/toastr.css">
    <link href="<?=WEB_ROUTE?>/css/standard-style.css" rel="stylesheet">
    <link rel="stylesheet" href="<?=WEB_ROUTE?>/css/admin-style.css">
</head>
<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
        <!-- Sidebar -->
        <ul class="navbar-nav sidebar accordion" id="accordionSidebar">
            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
                <div class="sidebar-brand-icon">
                    <img src="<?=WEB_ROUTE?>/img/png/uaem-logo.png" alt="" width="57px" height="50px" style="border-radius:3px;">
                </div>
                <div class="sidebar-brand-text mx-2">UAEM</div>
            </a>
            <!-- Divider -->
            <hr class="sidebar-divider my-0">
            <!-- Nav Item - Dashboard -->
            <li class="nav-item">
                <a class="nav-link" href="<?=WEB_ROUTE?>/admin.php">
                    <ion-icon name="home-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;">
                    </ion-icon>
                    <span>Inicio</span>
                </a>
            </li>
            <!-- Divider -->
            <hr class="sidebar-divider">
            <!-- Heading -->
            <div class="sidebar-heading">
                Operaciones
            </div>
            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item active">
                <a class="nav-link collapsed" href="" data-toggle="collapse" data-target="#collapseTwo"
                    aria-expanded="true" aria-controls="collapseTwo">
                    <ion-icon name="people-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;">
                    </ion-icon>
                    <span>Usuarios</span>
                </a>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-light py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Tipo de usuario:</h6>
                        <a class="collapse-item active" href="<?=WEB_ROUTE?>/admin-files/alumno_data.php">Alumnos</a>
                        <a class="collapse-item" href="<?=WEB_ROUTE?>/admin-files/profesor_data.php">Profesores</a>
                        <a class="collapse-item" href="<?=WEB_ROUTE?>/admin-files/trabajador_data.php"
                            style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;">Encargados de puntos
                            de control</a>
                        <a class="collapse-item" href="<?=WEB_ROUTE?>/admin-files/empleado_data.php">Otros empleados</a>
                        <a class="collapse-item" href="<?=WEB_ROUTE?>/admin-files/invitado_data.php">Usuarios
                            externos</a>
                        <a class="collapse-item" href="<?=WEB_ROUTE?>/admin-files/admin_data.php">Administradores</a>
                    </div>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link collapsed" href="" data-toggle="collapse" data-target="#collapseCatalogos"
                    aria-expanded="true" aria-controls="collapseCatalogos">
                    <ion-icon name="browsers-outline"
                        style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
                    <span>Catálogos</span>
                </a>
                <div id="collapseCatalogos" class="collapse" aria-labelledby="headingCatalogos"
                    data-parent="#accordionSidebar">
                    <div class="bg-light py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Catálogos disponibles:</h6>
                        <a class="collapse-item" href="<?=WEB_ROUTE?>/admin-files/grupo_data.php">Grupos</a>
                        <a class="collapse-item" href="<?=WEB_ROUTE?>/admin-files/materia_data.php">Materias</a>
                    </div>
                </div>
            </li>
            <!-- Nav Item - Tables -->
            <li class="nav-item">
                <a class="nav-link" href="<?=WEB_ROUTE?>/admin-files/puntoc_data.php">
                    <ion-icon name="contract-outline"
                        style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
                    <span>Puntos de control</span>
                </a>
            </li>
            <!-- Divider -->
            <hr class="sidebar-divider">
            <!-- Heading -->
            <div class="sidebar-heading">
                Búsquedas
            </div>
            <!-- Nav Item - Tables -->
            <li class="nav-item">
                <a class="nav-link" href="<?=WEB_ROUTE?>/admin-files/admin-busqueda.php">
                    <ion-icon name="search-circle-outline"
                        style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
                    <span>Busqueda avanzada</span>
                </a>
            </li>
            <!-- Divider -->
            <hr class="sidebar-divider">
            <!-- Heading -->
            <div class="sidebar-heading">
                Estadísticas y reportes
            </div>

            <!-- Nav Item - Tables -->
            <li class="nav-item">
                <a class="nav-link" href="">
                    <ion-icon name="bar-chart-outline"
                        style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
                    <span>Estadísticas</span>
                </a>
            </li>
            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>
        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
                <?php include(SERVER_ROUTE.'/templates/navbar.php'); ?>
                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <nav aria-label="breadcrumb" style="font-size: 14px">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?=WEB_ROUTE?>/admin.php"
                                    style="color: #958419;">Inicio</a></li>
                            <li class="breadcrumb-item" style="color: #958419;">Usuarios</li>
                            <li class="breadcrumb-item active" aria-current="page"><a href="">Alumnos</a></li>
                        </ol>
                    </nav>

                    <!-- Page Heading -->
                    <h4 class="mt-5" style="font-weight:600;">Datos de alumnos</h4>
                    <!--BOTON PARA UN NUEVO REGISTRO-->
                    <div class="container w-100 pr-0 mr-0">
                        <div class="row my-3 mx-0 w-100 d-flex justify-content-end">
                            <button class="btn btn-hov" type="submit" data-toggle="modal" data-target="#newModal">
                                <ion-icon name="person-add-outline" style="font-size: 22px; vertical-align: middle;">
                                </ion-icon>
                                &nbsp; Nuevo alumno
                            </button>
                            <button class="btn btn-green-light ml-2" type="submit">
                                <ion-icon name="duplicate-outline" style="font-size: 22px; vertical-align: middle;">
                                </ion-icon>
                                &nbsp; Cargar CSV
                            </button>
                        </div>
                    </div>
                    <!--TERMINA BOTON DE NUEVO REGISTRO-->

                    <!-- DataTales Example -->
                    <div class="card shadow mb-4 border-0">
                        <div class="card-header border-0 bg-white">
                            <h6 class="m-0 text-green-med font-weight-bold" style="font-size:14px;">Alumnos disponibles</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table datatable table-hover table-bordered table-condensed" id="dataTable"
                                    style="width:100%">
                                    <thead class="text-center text-white">
                                        <tr>
                                            <th>No. de cuenta</th>
                                            <th>Nombre</th>
                                            <th>Email</th>
                                            <th>Teléfono</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tfoot class="text-center text-white">
                                        <th>No. de cuenta</th>
                                        <th>Nombre</th>
                                        <th>Email</th>
                                        <th>Teléfono</th>
                                        <th>Acciones</th>
                                        </tr>
                                    </tfoot>
                                    <tbody class="text-center" id="table-body">
                                        <?php for ($i = 0; $i < sizeof($persona); $i++) { ?>
                                        <tr>
                                            <td><?= $persona[$i]['id_persona'] ?></td>
                                            <td><?= $persona[$i]['nombre'] ?></td>
                                            <td><?= $persona[$i]['email'] ?></td>
                                            <td><?= $persona[$i]['telefono'] ?></td>
                                            <td class="d-flex justify-content-between">
                                                <button type="button" class="btn btn-sm btn-outline-dark open-edit-modal mr-1">
                                                    <ion-icon name="list-outline" style="font-size: 22px; vertical-align: middle;"></ion-icon>
                                                </button>
                                                <button type="button" class="btn btn-sm btn-outline-danger open-delete-modal">
                                                    <ion-icon name="trash-outline"
                                                        style="font-size: 22px; vertical-align: middle;"></ion-icon>
                                                </button>
                                            </td>
                                        </tr>
                                        <?php }; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- End of Main Content -->
        </div>
        <!-- End of Content Wrapper -->
    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!--MODAL PARA EDITAR FORMULARIO-->
    <div class="modal fade border-0" id="editModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered border-0">
            <div class="modal-content">
                <div class="modal-header border-0 bg-white">
                    <h6 class="modal-title font-weight-bold" id="exampleModalLabel">Editar datos de alumno</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body border-0 py-0 mx-3">
                    <div class="row bg-warning py-2 mx-1 mb-3" id="row_warn" style="border-radius:4px">
                        <div class="col-2 mr-0 pr-0 text-center d-flex align-items-center">
                            <ion-icon name="warning-outline" style="font-size: 27px; vertical-align: middle;"></ion-icon>
                        </div>
                        <div class="col-10 ml-0 pl-0 d-flex align-items-center" style="font-size:15px;">
                            Recuerda que los cambios no se pueden deshacer.
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Número de cuenta:</label>
                        <input class="form-control" type="number" name="edit_id" id="edit_id" placeholder="Ingrese número de cuenta" disabled>
                    </div>
                    <div class="form-group">
                        <label for="">Nombre completo:</label>
                        <input class="form-control" type="text" name="edit_nombre" id="edit_nombre" placeholder="Ingrese nombre completo">
                    </div>
                    <div class="form-group">
                        <label for="">Correo institucional:</label>
                        <input class="form-control" type="email" name="edit_email" id="edit_email" placeholder="Ingrese correo institucional">
                    </div>
                    <div class="form-group">
                        <label for="">Teléfono (opcional):</label>
                        <input class="form-control" type="telefono" name="edit_tel" id="edit_tel" placeholder="Ingrese teléfono">
                    </div>
                    <div class="form-group">
                        <label for="">Contraseña (opcional):</label>
                        <input class="form-control" type="password" name="edit_password" id="edit_password" placeholder="Ingrese contraseña">
                    </div>
                    <div class="form-group">
                        <label for="">Cambiar rol:</label>
                        <select class="custom-select form-input" name="" id="">
                            <option value="1">Administrador</option>
                            <option value="2">Encargado de punto de control</option>
                            <option value="3">Profesor</option>
                            <option value="4" selected>Alumno</option>
                            <option value="5">Usuario externo</option>
                            <option value="6">Administrativo / Empleado</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer border-0">
                    <button type="button" class="btn btn-outline-dark" type="cancel" data-dismiss="modal"
                        id="cancel-btn">Cancelar</button>
                    <button class="btn btn-hov" type="button" id="edit-btn">Guardar</button>
                </div>
            </div>
        </div>
    </div>
    <!--FIN FORMULARIO EDITAR MODAL-->

    <!--MODAL PARA CREAR UN NUEVO REGISTRO-->
    <div class="modal fade border-0" id="newModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered border-0">
            <div class="modal-content">
                <div class="modal-header bg-white border-0">
                    <h6 class="modal-title font-weight-bold">Registrar alumno nuevo</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body border-0 py-2 mx-3">
                    <div class="form-group">
                        <label for="">Número de cuenta:</label>
                        <input class="form-control" type="number" name="new_id" id="new_id" placeholder="Ingrese número de cuenta" min="1">
                    </div>
                    <div class="form-group">
                        <label for="">Nombre completo:</label>
                        <input class="form-control" type="text" name="new_nombre" id="new_nombre" placeholder="Ingrese nombre completo">
                    </div>
                    <div class="form-group">
                        <label for="">Correo institucional:</label>
                        <input class="form-control" type="email" name="new_email" id="new_email" placeholder="Ingrese correo institucional">
                    </div>
                    <div class="form-group">
                        <label for="">Teléfono (opcional):</label>
                        <input class="form-control" type="telefono" name="new_tel" id="new_tel" placeholder="Ingrese teléfono">
                    </div>
                    
                    <label for="">Contraseña (opcional):</label>
                    <div class="input-group">
                        <input type="password" class="form-control form-control-user form-input rounded_squares" id="user_password" name="user_password" placeholder="Contraseña" value="<?=$pass?>">
                        <div class="input-group-append">
                            <button class="btn btn-pass d-none" id="see_pass_btn" type="button" style="max-height:38px;">
                                <ion-icon name="eye-outline" style="vertical-align:middle; font-size:22px;"></ion-icon>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer border-0">
                    <button type="button" class="btn btn-outline-dark" type="cancel" data-dismiss="modal"
                        id="cancel-btn">Cancelar</button>
                    <button class="btn btn-hov" type="submit" id="create-btn">Crear</button>
                </div>
            </div>
        </div>
    </div>
    <!--FIN FORMULARIO MODAL-->

    <!-- delete Modal-->
    <div class="modal fade border-0" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="delete-modal"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered border-0">
            <div class="modal-content">
                <div class="modal-header border-0">
                    <h6 class="modal-title font-weight-bold" id="deleteModal">Seguro que deseas borrar a este usuario?</h6>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body border-0 mx-3">
                    <div class="row bg-warning py-2" id="row_warn" style="border-radius:4px">
                        <div class="col-2 mr-0 pr-0 text-center d-flex align-items-center">
                            <ion-icon name="warning-outline" style="font-size: 27px; vertical-align: middle;"></ion-icon>
                        </div>
                        <div class="col-10 ml-0 pl-0 d-flex align-items-center" style="font-size:15px;">
                            Recuerda que esta acción no se puede deshacer.
                        </div>
                    </div>
                </div>
                <div class="modal-footer border-0">
                    <button type="button" class="btn btn-outline-dark" type="cancel"
                        data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-danger" id="delete-btn">Eliminar</button>
                </div>
            </div>
        </div>
    </div>

    <?php include(SERVER_ROUTE.'/templates/logout-modal.php'); ?>

    <script src="https://code.jquery.com/jquery-3.5.1.js"
        integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous">
    </script>
    <script src="https://unpkg.com/ionicons@5.2.3/dist/ionicons.js"></script>
    <!-- Page level plugins -->
    <script src="<?=WEB_ROUTE?>/js/datatables/jquery.dataTables.min.js"></script>
    <script src="<?=WEB_ROUTE?>/js/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Custom scripts-->
    <script src="<?=WEB_ROUTE?>/toastr/build/toastr.min.js"></script>
    <script src="<?=WEB_ROUTE?>/js/standard-func.js"></script>
    <script src="<?=WEB_ROUTE?>/js/admin/alumno-func.js"></script>
</body>

</html>