<?php
    include('../routes.php');
    include(SERVER_ROUTE.'/database.php');
    include(SERVER_ROUTE.'/utilities/get_stats.php');
    include(SERVER_ROUTE.'/utilities/get_user_type.php');
    session_start();

    $to_res = false;
    $res = NULL;
    $message = "";

    if(isset($_SESSION['id_usuario']))
    {
        switch($_SESSION['tipo_persona'])
        {
            case 1: // Tipo de usuario admin 
                $first_day = $_GET['first_day'];
                $last_day  = $_GET['last_day'];

                $first_day_time = strtotime($first_day);
                $last_day_time = strtotime($last_day);

                if ( $first_day_time <= $last_day_time ) {
                    $res = getStats($first_day,$last_day,$connection);
                    $to_res = true;
                }
                else
                {
                    $to_res = false;
                    $message = "fechas no coincidentes";
                }
                break;
            case 2: // Tipo de usuario encargado de cp
                $to_res = false;
                break;
            case 3: // Tipo de usuario profesor
                $to_res = false;
                break;
            default:
                $to_res = false;
                break;
        }
    }
    else
    {
       $to_res = false;
    }

?>
<?php if($to_res): ?>
<?php
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename= estadisticas_".gmdate("d-m-Y",$first_day_time)."_".gmdate("d-m-Y",$last_day_time).".xlsx");
?>
    <h2>Estad&iacute;sticas</h2>
    <table>
        <thead>
            <tr style="background-color:#a7964d;color:white;">
                <th>Fecha inicial</th>
                <th>Fecha final</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="border:solid 1px black;"><?=gmdate("d-m-Y",$first_day_time);?></td>
                <td style="border:solid 1px black;"><?=gmdate("d-m-Y",$last_day_time);?></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
        </tbody>
    </table>
    <table>
        <thead>
            <tr>
                <th>Accesos</th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th>Casos sospechosos</th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            <tr>
                <th style="background-color:#597552;color:white;">Accesos totales registrados</th>
                <th style="background-color:#597552;color:white;">Alumnos</th>
                <th style="background-color:#597552;color:white;">Profesores</th>
                <th style="background-color:#597552;color:white;">Administrativos / Empleados</th>
                <th style="background-color:#597552;color:white;">Usuarios externos</th>
                <th></th>
                <th style="background-color:#6c757d;color:white;">Casos sospechosos registrados</th>
                <th style="background-color:#6c757d;color:white;">Alumnos</th>
                <th style="background-color:#6c757d;color:white;">Profesores</th>
                <th style="background-color:#6c757d;color:white;">Administrativos / Empleados</th>
                <th style="background-color:#6c757d;color:white;">Usuarios externos</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="border:solid 1px black;"><?=$res["accesos_totales"]?></td>
                <td style="border:solid 1px black;"><?=$res["accesos_alumnos"]?></td>
                <td style="border:solid 1px black;"><?=$res["accesos_profesores"]?></td>
                <td style="border:solid 1px black;"><?=$res["accesos_administrativos"]?></td>
                <td style="border:solid 1px black;"><?=$res["accesos_externos"]?></td>
                <td></td>
                <td style="border:solid 1px black;"><?=$res["casos_totales"]?></td>
                <td style="border:solid 1px black;"><?=$res["casos_alumnos"]?></td>
                <td style="border:solid 1px black;"><?=$res["casos_profesores"]?></td>
                <td style="border:solid 1px black;"><?=$res["casos_administrativos"]?></td>
                <td style="border:solid 1px black;"><?=$res["casos_externos"]?></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
    </table>
    <table>
        <thead>
            <tr>
                <th>Accesos</th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th>Casos sospechosos</th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            <tr>
                <th style="background-color:#597552;color:white;">Matr&iacute;cula</th>
                <th style="background-color:#597552;color:white;">Nombre</th>
                <th style="background-color:#597552;color:white;">Tipo de usuario</th>
                <th style="background-color:#597552;color:white;">Hora de registro</th>
                <th style="background-color:#597552;color:white;">Fecha de registro</th>
                <th style="background-color:#597552;color:white;">Registrado por</th>
                <th style="background-color:#597552;color:white;">Punto de control</th>
                <th style="background-color:#597552;color:white;">Detalles de registro</th>
                <th></th>
                <th style="background-color:#6c757d;color:white;">Matr&iacute;cula</th>
                <th style="background-color:#6c757d;color:white;">Nombre</th>
                <th style="background-color:#6c757d;color:white;">Tipo de usuario</th>
                <th style="background-color:#6c757d;color:white;">Hora de registro</th>
                <th style="background-color:#6c757d;color:white;">Fecha de registro</th>
                <th style="background-color:#6c757d;color:white;">Registrado por</th>
                <th style="background-color:#6c757d;color:white;">Punto de control</th>
                <th style="background-color:#6c757d;color:white;">Detalles de registro</th>
            </tr>
        </thead>
        <tbody>
            <tr>
            <?php $index = 0; ?>
            <?php while(true): ?>
                <?php if ($index < sizeof($res["accesos"])): ?>
                    <td style="border:solid 1px black;"><?=$res["accesos"][$index]["id_persona"]?></td>
                    <td style="border:solid 1px black;"><?=$res["accesos"][$index]["nombre"]?></td>
                    <td style="border:solid 1px black;"><?=getUserType($res["accesos"][$index]["tipo_persona"]);?></td>
                    <td style="border:solid 1px black;"><?=$res["accesos"][$index]["hora_reg"]?></td>
                    <td style="border:solid 1px black;"><?=$res["accesos"][$index]["fecha_reg"]?></td>
                    <td style="border:solid 1px black;"><?=$res["accesos"][$index]["encargado"]?></td>
                    <td style="border:solid 1px black;"><?=$res["accesos"][$index]["punto_control"]?></td>
                    <td style="border:solid 1px black;"><?=$res["accesos"][$index]["detalles"]?></td>
                <?php else: ?>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                <?php endif; ?>
                    <td></td>
                <?php if ($index < sizeof($res["casos_sospechosos"])): ?>
                    <td style="border:solid 1px black;"><?=$res["casos_sospechosos"][$index]["id_persona"]?></td>
                    <td style="border:solid 1px black;"><?=$res["casos_sospechosos"][$index]["nombre"]?></td>
                    <td style="border:solid 1px black;"><?=getUserType($res["accesos"][$index]["tipo_persona"]);?></td>
                    <td style="border:solid 1px black;"><?=$res["casos_sospechosos"][$index]["hora_reg"]?></td>
                    <td style="border:solid 1px black;"><?=$res["casos_sospechosos"][$index]["fecha_reg"]?></td>
                    <td style="border:solid 1px black;"><?=$res["casos_sospechosos"][$index]["encargado"]?></td>
                    <td style="border:solid 1px black;"><?=$res["casos_sospechosos"][$index]["punto_control"]?></td>
                    <td style="border:solid 1px black;"><?=$res["casos_sospechosos"][$index]["detallles"]?></td>
                <?php else: ?>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                <?php endif; ?>
                <?php if ( $index >= sizeof($res["casos_sospechosos"]) && $index >= sizeof($res["accesos"]) ): ?>
                    <?php break; ?>
                <?php else: ?>
                    <?php $index = $index + 1; ?>
                <?php endif; ?> 
            <?php endwhile; ?>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
    </table>
<?php else: ?>
<?php
    header('Location: '.WEB_ROUTE.'/index.php');
?>
<?php endif; ?>