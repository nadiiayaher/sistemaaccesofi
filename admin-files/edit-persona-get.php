<?php
    include('../routes.php');
    include(SERVER_ROUTE.'/database.php');
    $id_persona = $_POST['id_persona'];
    $records = $connection->prepare('SELECT persona.id_persona,persona.email,persona.telefono,persona.password,persona.nombre FROM persona WHERE id_persona = :id_persona');
    $records->bindParam('id_persona',$id_persona);
    $records->execute();
    $result = $records->fetch(PDO::FETCH_ASSOC);
    echo json_encode($result);
?>
