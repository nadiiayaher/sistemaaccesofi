<?php
    function generar_caracteres($chars, $length)
    {
        $captcha = null;
        for($i = 0; $i<$length; $i++)
        {
            $rand = rand(0, count($chars)-1);
            $captcha .= $chars[$rand];
        }
        return $captcha;
    }

    function createCaptchaImage()
    {
        $chars_cap = array('0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f','g','h','i',
        'j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I',
        'J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');

        $imagen = imagecreatetruecolor(120,45);
        $color_fondo = imagecolorallocate($imagen, 167, 150, 77);
        $color_texto = imagecolorallocate($imagen, 254, 254, 254);
        imagefill($imagen,0,0,$color_fondo);
        $captcha = generar_caracteres($chars_cap,5);
        $_SESSION['captcha'] = $captcha;
        imagettftext($imagen,17,10,5,35, $color_texto, SERVER_ROUTE."/sewer.ttf",$captcha);
        ob_start();
        imagepng($imagen);
        $bin = ob_get_clean();
        return base64_encode($bin);
    }
?>
