<?php
  include('routes.php');
?>

<!doctype html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
    <link href="<?=WEB_ROUTE?>/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link rel="icon" type="image/png" href="<?=WEB_ROUTE?>/favicon.png"/>
    <link rel="stylesheet" href="<?=WEB_ROUTE?>/css/login-style.css">
    <title>Sistema de control de acceso FI UAEMex</title>
</head>
<body style="background:#f5f6fa;">
    <div class="container bg-white" id="topbar">
        <header class="head">
            <nav class="navbar navbar-expand-lg navbar-light head__custom-nav">
                <a class="navbar-brand d-flex flex-row" href="" id="main-title">
                    <img class="" src="img/png/fi_uaem_logo.png" alt="website logo" height="48">
                    <span class="d-none d-sm-block pt-2">&nbsp; Facultad de Ingeniería</span>
                    <span class="d-block d-sm-none pt-2">&nbsp; FI UAEM</span>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" style="border: none;">
                    <ion-icon name="chevron-down-outline" style="color: black; font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>                        
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item pt-2 mr-3">
                            <a class="nav-link" href="index.php">Inicio</a>
                        </li>
                        <li class="nav-item pt-2 mr-3">
                            <a class="nav-link" href="">Acerca de</a>
                        </li>
                        <li class="nav-item pt-2 mr-3">
                            <a class="nav-link" href="">Ayuda</a>
                        </li>
                        <li class="nav-item d-none d-lg-block d-xl-block pt-2">
                            <a class="btn btn-login-nav" href="login.php">Ingresar</a>
                        </li>
                        <li class="nav-item d-none d-md-block d-xl-block d-xxl-block pt-2">
                            <a href="">
                                <img class="" height="50" width="165" src="img/png/img_logo_uaem18-21.png" alt="website logo">
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
    </div>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-10 col-lg-12 col-md-9 col-sm-12">
                <div class="card o-hidden border-0 shadow-lg mt-5">
                  <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                      <div class="col-sm-12 col-lg-6 d-none d-lg-block bg-image">
                      </div>
                      <div class="col-sm-12 col-lg-6">
                        <div class="" style="padding:10%;">
                          <div class="text-center">
                            <h4 class="mb-4">Olvidaste tu contraseña?</h4>
                            <hr>
                            <p class="mb-4">Ingresa tu correo institucional para poder reestablecer tu contraseña.</p>
                          </div>
                          <p class="error-message d-none">message</p>
                          <form class="user">
                            <div class="form-group mb-4">
                              <input type="email" class="form-control form-control-user user_email" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="example@dominio.uaemex.mx">
                            </div>
                            <button class="btn btn-primary btn-user btn-block btn-login">
                              Reestablecer contraseña
                            </button>
                          </form>
                          <hr>
                          <div class="text-center">
                            <a class="small" href="login.php" style="color: #958419;">Regresar a inicio de sesión</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="pt-3 d-flex justify-content-between">
                    <a href="" class="text-secondary" style="font-size:14px;">Aviso de privacidad &nbsp;<i class="fas fa-external-link-alt" style="font-size:12px;"></i></a>
                    <span class="text-secondary mr-3" style="font-size:14px;">Facultad de Ingeniería UAEM</span>
                </div>
            </div>

        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/ionicons@5.2.3/dist/ionicons.js"></script>
    <script src="js/login-func.js"></script>
</body>
</html>