<?php
    include('routes.php');
    include(SERVER_ROUTE.'/database.php');
    $tipo_registro = $_POST['tipo_registro'];
    if ($tipo_registro == "1" ) { // Registro con QR
        $qrcode = $_POST['qrcode'];
        $id_persona = $_POST['id_persona'];
        $id_cp = $_POST['id_cp'];
        $records = $connection->prepare('SELECT qrcode FROM punto_control WHERE id_cp =:id_cp;');
        $records->bindParam('id_cp',$id_cp);
        $records->execute();
        $result = $records->fetch(PDO::FETCH_ASSOC);
        if(!empty(result)){
            $correct_qrcode = $result['qrcode'];
            if ($qrcode == $correct_qrcode) {
                $records = $connection->prepare('INSERT INTO acceso(id_persona,id_cp) VALUES(:id_persona,:id_cp);');
                $records->bindParam('id_persona',$id_persona);
                $records->bindParam('id_cp',$id_cp);
                if($records->execute()) {
                    $last_id = $connection->lastInsertId();
                    $records = $connection->prepare('SELECT DATE_FORMAT(acceso.registered_at,\'%d-%m-%Y\') AS fecha_reg, DATE_FORMAT(acceso.registered_at,\'%H:%i:%s\') AS hora_reg, punto_control.descripcion FROM acceso,punto_control WHERE acceso.id_cp = punto_control.id_cp AND acceso.id_persona = :id_persona AND acceso.id_acceso = :last_id;');
                    $records->bindParam('id_persona',$id_persona);
                    $records->bindParam('last_id',$last_id);
                    $records->execute();
                    $result = $records->fetch(PDO::FETCH_ASSOC);
                    $res = array(
                    "status" => 202,
                    "message" => "!Registro exitoso!",
                    "hora_reg" => $result['hora_reg'],
                    "fecha_reg" => $result['fecha_reg'],
                    "punto_control" => $result['descripcion'],
                    );
                    echo json_encode($res);
                }
                else {
                    $res = array("status" => 404, "message" => 'No se pudo realizar el registro. Parece que el servidor esta tenido problemas. Intenta realizar la operacion mas tarde');
                    echo json_encode($res);
                }
            }
            else
            {
                $res = array("status" => 404, "message" => 'El QR que escaneaste no es valido!');
                echo json_encode($res);
            }
        }
        else {
            $res = array("status" => 404, "message" => 'El QR que escaneaste no es valido!');
            echo json_encode($res);
        }
    }
    else if ($tipo_registro == "2" ) { // Registro con credencial
        $matricula = $_POST['matricula'];
        $id_cp = $_POST['id_cp'];
        $records = $connection->prepare('SELECT id_persona FROM persona WHERE identificador = :matricula;');
        $records->bindParam('matricula',$matricula);
        $records->execute();
        $result = $records->fetch(PDO::FETCH_ASSOC);
        if(!empty($result)) {
            $records = $connection->prepare('INSERT INTO acceso(id_persona,id_cp) VALUES(:id_persona,:id_cp);');
            $records->bindParam('id_persona',$result['id_persona']);
            $records->bindParam('id_cp',$id_cp);
            if ($records->execute()) {
                $res = array("status" => 202, "message" => 'Se ha registrado exitosamente el acceso del usuario');
                echo json_encode($res);
            }
            else{
                $res = array("status" => 404, "message" => 'No se ha podido registrar el acceso del usuario. Parece que el servidor esta teniendo problemas. Intentalo de nuevo dentro de unos minutos.');
                echo json_encode($res);
            }
        }
        else {
            $res = array("status" => 404, "message" => 'Parece que este usuario no esta registrado en el sistema!');
            echo json_encode($res);
        }
    }
    else if ($tipo_registro == "3") { // Registro manual de persona ya registrada
        
        $id_persona = $_POST['id_persona'];
        $id_cp = $_POST['id_cp'];
        $id_encargado = $_POST['id_encargado'];
        $status = $_POST['status'];
        $detalles = $_POST['detalles'];

        $query1 = "INSERT INTO acceso(id_persona,id_cp,id_encargado";
        $query2 = ") VALUES (".$id_persona.",".$id_cp.",".$id_encargado;
        if ($detalles != "") {
            $query1 .= ",detalles_visita";
            $query2 .= ",'".$detalles."'";
        }
        $query = $query1.$query2.");";
        $records = $connection->prepare($query);
        if ($records->execute()) {
            if ($status == "1") {
                $records = $connection->prepare('UPDATE persona SET status = 0 WHERE persona.id_persona = :id_persona');
                $records->bindParam('id_persona',$id_persona);
                if ($records->execute()) {
                    $res = array("status" => 202, "message" => 'Usuario registrado');
                    echo json_encode($res);
                }
                else {
                    $res = array("status" => 404, "message" => 'No se ha podido registrar el acceso del usuario. Parece que el servidor esta teniendo problemas. Intentalo de nuevo dentro de unos minutos.');
                    echo json_encode($res);
                }
            }
            else
            {
                $res = array("status" => 202, "message" => 'Usuario registrado');
                echo json_encode($res);
            }
        }
        else {
            $res = array("status" => 404, "message" => 'No se ha podido registrar el acceso del usuario. Parece que el servidor esta teniendo problemas. Intentalo de nuevo dentro de unos minutos.');
            echo json_encode($res);
        }
    }
    else if ($tipo_registro == "4") { // Registro manual de persona externa
        $externo_nombre = $_POST['externo_nombre'];
        $externo_motivo = $_POST['externo_motivo'];
        $id_encargado = $_POST['id_encargado'];
        $id_cp = $_POST['id_cp'];
        $externo_email = $_POST['externo_email'];
        $externo_tel = $_POST['externo_tel'];

        $query1 = "INSERT INTO persona(nombre,tipo_persona";
        $query2 = ") VALUES ('".$externo_nombre."',5";

        if ($externo_email != '')
        {
            $query1 .= ",email";
            $query2 .= ",'".$externo_email."'";
        }
        if ($externo_tel != '')
        {
            $query1 .= ",telefono";
            $query2 .= ",'".$externo_tel."'";
        }
        $query = $query1.$query2.");";
        $records = $connection->prepare($query);
        if ( $records->execute() ) {
            $last_id = $connection->lastInsertId();
            $records = $connection->prepare('INSERT INTO acceso(id_persona,id_cp,id_encargado,detalles_visita) VALUES(:id_persona,:id_cp,:id_encargado,:detalles_visita);');
            $records->bindParam('id_persona',$last_id);
            $records->bindParam('id_cp',$id_cp);
            $records->bindParam('id_encargado',$id_encargado);
            $records->bindParam('detalles_visita',$externo_motivo);
            if ( $records->execute() ) {
                $res = array("status" => 202, "message" => 'Usuario registrado');
                echo json_encode($res);
            }
            else
            {
                $res = array("status" => 404, "message" => 'No se reconoce el tipo de registro que intentas realizar!');
                echo json_encode($res);
            }
        }
        else {
            $res = array("status" => 404, "message" => 'No se reconoce el tipo de registro que intentas realizar!');
            echo json_encode($res);
        }
    }
?>