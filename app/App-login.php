<?php
    include('database.php');
    session_start();
    date_default_timezone_set('America/Tegucigalpa');
    $user_email = $_POST['user_email'];
    $user_password = $_POST['user_password'];
    
    $records = $connection->prepare('SELECT * FROM persona WHERE email = :user_email');
    $records->bindParam('user_email',$_POST['user_email']);
    $records->execute();
    $result_user = $records->fetch(PDO::FETCH_ASSOC);
    if (!empty($result_user)) {
        if ($result_user['password'] == $_POST['user_password']) {
            $fecha_actual = date('Y-m-d');
            $records = $connection->prepare('SELECT DATE_FORMAT(acceso.registered_at,\'%d-%m-%Y\') AS fecha_reg, DATE_FORMAT(acceso.registered_at,\'%H:%i:%s\') AS hora_reg, punto_control.descripcion FROM acceso,punto_control WHERE acceso.id_cp = punto_control.id_cp AND acceso.id_persona = :id_persona AND acceso.registered_at >= :fecha_actual;');
            $records->bindParam('id_persona',$result_user['id_persona']);
            $records->bindParam('fecha_actual',$fecha_actual);
            $records->execute();
            $reg = $records->fetch(PDO::FETCH_ASSOC);
            if(!empty($reg)) {
                $res = array(
                    "status" => 202,
                    "id_persona" => $result_user['id_persona'],
                    "identificador" => $result_user['identificador'],
                    "nombre" => $result_user['nombre'],
                    "email" => $result_user['email'],
                    "tipo_persona" => $result_user['tipo_persona'],
                    "estado_registro" => 202,
                    "hora_reg" => $reg['hora_reg'],
                    "fecha_reg" => $reg['fecha_reg'],
                    "punto_control" => $reg['descripcion'],
                );
                echo json_encode($res);
            }
            else {
                $res = array(
                    "status" => 202,
                    "id_persona" => $result_user['id_persona'],
                    "identificador" => $result_user['identificador'],
                    "nombre" => $result_user['nombre'],
                    "email" => $result_user['email'],
                    "tipo_persona" => $result_user['tipo_persona'],
                    "estado_registro" => 404
                );
                echo json_encode($res);
            }
        }
        else {
            $res = array("status" => 404, "message" => 'Contraseña incorrecta');
            echo json_encode($res);
        }
    }
    else {
        $res = array("status" => 404, "message" => 'No hay ningun usuario registrado con ese email');
        echo json_encode($res);
    }
?>