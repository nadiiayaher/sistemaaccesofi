-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 29-07-2021 a las 19:39:45
-- Versión del servidor: 10.4.20-MariaDB
-- Versión de PHP: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sistema_acceso`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acceso`
--

CREATE TABLE `acceso` (
  `id_acceso` int(11) NOT NULL,
  `id_persona` int(11) NOT NULL,
  `id_cp` int(11) NOT NULL,
  `id_encargado` int(11) NOT NULL,
  `detalles_visita` varchar(500) DEFAULT NULL,
  `registered_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `acceso`
--

INSERT INTO `acceso` (`id_acceso`, `id_persona`, `id_cp`, `id_encargado`, `detalles_visita`, `registered_at`) VALUES
(21, 1621033, 2, 3, NULL, '2021-07-22 00:07:58'),
(22, 1621045, 2, 3, NULL, '2021-07-22 00:11:30'),
(23, 1621051, 2, 3, NULL, '2021-07-23 02:13:57'),
(26, 1621012, 3, 3, NULL, '2021-07-26 20:26:52'),
(27, 1621033, 3, 3, NULL, '2021-07-26 20:27:23'),
(28, 1621001, 1, 3, 'Visita a CAE', '2021-07-28 20:39:20'),
(29, 1735001, 1, 3, 'Hola', '2021-07-28 20:39:50'),
(30, 1621033, 1, 3, 'Visita a control escolar', '2021-07-28 20:40:24'),
(31, 3, 2, 3, NULL, '2021-07-29 00:06:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno_grupo`
--

CREATE TABLE `alumno_grupo` (
  `id_inscripcion` int(11) NOT NULL,
  `id_grupo` int(11) NOT NULL,
  `id_alumno` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `alumno_grupo`
--

INSERT INTO `alumno_grupo` (`id_inscripcion`, `id_grupo`, `id_alumno`) VALUES
(1, 1, 1621033),
(2, 1, 1621012);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caso_sospechoso`
--

CREATE TABLE `caso_sospechoso` (
  `id_caso` int(11) NOT NULL,
  `id_persona` int(11) NOT NULL,
  `id_cp` int(11) NOT NULL,
  `id_encargado` int(11) NOT NULL,
  `detalles` varchar(500) DEFAULT NULL,
  `registered_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `caso_sospechoso`
--

INSERT INTO `caso_sospechoso` (`id_caso`, `id_persona`, `id_cp`, `id_encargado`, `detalles`, `registered_at`) VALUES
(10, 1621012, 1, 3, 'Tiene fiebre', '2021-07-29 01:21:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo`
--

CREATE TABLE `grupo` (
  `id_grupo` int(11) NOT NULL,
  `id_profesor` int(11) NOT NULL,
  `id_materia` varchar(11) NOT NULL,
  `periodo` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `grupo`
--

INSERT INTO `grupo` (`id_grupo`, `id_profesor`, `id_materia`, `periodo`) VALUES
(1, 2, 'L41012', '2021A'),
(2, 2, 'L41032', '2021A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materia`
--

CREATE TABLE `materia` (
  `id_materia` varchar(11) NOT NULL,
  `nombre` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `materia`
--

INSERT INTO `materia` (`id_materia`, `nombre`) VALUES
('L41012', 'Programacion estructurada'),
('L41032', 'Automatas y lenguajes formales'),
('L41053', 'Programacion avanzada'),
('L41054', 'Estructuras de datos'),
('LINC06', 'Comunicacion y relaciones humanas'),
('LINC11', 'Fisica'),
('LINC13', 'Matematicas discretas'),
('LINC15', 'Programacion I'),
('LINC20', 'Bases de datos I'),
('LINC35', 'Programacion II');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `id_persona` int(11) NOT NULL,
  `email` varchar(200) DEFAULT NULL,
  `telefono` varchar(12) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `nombre` varchar(200) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `tipo_persona` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`id_persona`, `email`, `telefono`, `password`, `nombre`, `status`, `tipo_persona`) VALUES
(1, 'danielgomez@uaemex.mx', '', 'hesoyamlas', 'Daniel Gomez Alvarez', 0, 1),
(2, 'johnperez@profesor.uaemex.mx', '', 'hesoyamlas', 'John Perez Rojas', 0, 3),
(3, 'joeramirez@uaemex.mx', '', 'hesoyamlas', 'Joe Ramirez Castro', 0, 2),
(1621001, NULL, NULL, NULL, 'Jaime Rodriguez Lutero', 0, 4),
(1621012, 'jeremyolmos@alumno.uaemex.mx', NULL, 'hesoyamlas', 'Jeremy Olmos Matt', 1, 4),
(1621033, 'fgomeza033@alumno.uaemex.mx', '', 'xngwk.16B', 'Francisco Daniel Gomez Alvarez', 0, 4),
(1621045, 'ramonmendez@profesor.uaemex.mx', NULL, 'hesoyamlas', 'Ramón Méndez Ayala', 0, 3),
(1621051, 'danielgomez@gmail.com', '7223928209', NULL, 'Daniel Gomez', 0, 5),
(1621306, NULL, NULL, NULL, 'Matias Montoya Juarez', 0, 4),
(1734902, NULL, NULL, NULL, 'Maria Karla Alvarez Tellez', 0, 4),
(1734998, NULL, NULL, NULL, 'Miguel Palacios Osorio', 0, 4),
(1735001, 'prueba@gmail.co', NULL, NULL, 'Prueba 3', 0, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `punto_control`
--

CREATE TABLE `punto_control` (
  `id_cp` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `punto_control`
--

INSERT INTO `punto_control` (`id_cp`, `nombre`) VALUES
(1, 'Sala Multimedia'),
(2, 'Cafeteria'),
(3, 'Puerta D');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `acceso`
--
ALTER TABLE `acceso`
  ADD PRIMARY KEY (`id_acceso`),
  ADD KEY `id_persona` (`id_persona`),
  ADD KEY `id_cp` (`id_cp`),
  ADD KEY `acceso_ibfk_3` (`id_encargado`);

--
-- Indices de la tabla `alumno_grupo`
--
ALTER TABLE `alumno_grupo`
  ADD PRIMARY KEY (`id_inscripcion`),
  ADD KEY `id_grupo` (`id_grupo`),
  ADD KEY `id_alumno` (`id_alumno`);

--
-- Indices de la tabla `caso_sospechoso`
--
ALTER TABLE `caso_sospechoso`
  ADD PRIMARY KEY (`id_caso`),
  ADD KEY `caso_sospechoso_ibfk_3` (`id_cp`),
  ADD KEY `caso_sospechoso_ibfk_1` (`id_persona`),
  ADD KEY `caso_sospechoso_ibfk_2` (`id_encargado`);

--
-- Indices de la tabla `grupo`
--
ALTER TABLE `grupo`
  ADD PRIMARY KEY (`id_grupo`),
  ADD KEY `id_materia` (`id_materia`),
  ADD KEY `grupo_ibfk_1` (`id_profesor`);

--
-- Indices de la tabla `materia`
--
ALTER TABLE `materia`
  ADD PRIMARY KEY (`id_materia`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`id_persona`);

--
-- Indices de la tabla `punto_control`
--
ALTER TABLE `punto_control`
  ADD PRIMARY KEY (`id_cp`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `acceso`
--
ALTER TABLE `acceso`
  MODIFY `id_acceso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT de la tabla `alumno_grupo`
--
ALTER TABLE `alumno_grupo`
  MODIFY `id_inscripcion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `caso_sospechoso`
--
ALTER TABLE `caso_sospechoso`
  MODIFY `id_caso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `grupo`
--
ALTER TABLE `grupo`
  MODIFY `id_grupo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `id_persona` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1735002;

--
-- AUTO_INCREMENT de la tabla `punto_control`
--
ALTER TABLE `punto_control`
  MODIFY `id_cp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `acceso`
--
ALTER TABLE `acceso`
  ADD CONSTRAINT `acceso_ibfk_1` FOREIGN KEY (`id_persona`) REFERENCES `persona` (`id_persona`),
  ADD CONSTRAINT `acceso_ibfk_2` FOREIGN KEY (`id_cp`) REFERENCES `punto_control` (`id_cp`),
  ADD CONSTRAINT `acceso_ibfk_3` FOREIGN KEY (`id_encargado`) REFERENCES `persona` (`id_persona`);

--
-- Filtros para la tabla `alumno_grupo`
--
ALTER TABLE `alumno_grupo`
  ADD CONSTRAINT `alumno_grupo_ibfk_1` FOREIGN KEY (`id_grupo`) REFERENCES `grupo` (`id_grupo`),
  ADD CONSTRAINT `alumno_grupo_ibfk_2` FOREIGN KEY (`id_alumno`) REFERENCES `persona` (`id_persona`);

--
-- Filtros para la tabla `caso_sospechoso`
--
ALTER TABLE `caso_sospechoso`
  ADD CONSTRAINT `caso_sospechoso_ibfk_1` FOREIGN KEY (`id_persona`) REFERENCES `persona` (`id_persona`),
  ADD CONSTRAINT `caso_sospechoso_ibfk_2` FOREIGN KEY (`id_encargado`) REFERENCES `persona` (`id_persona`),
  ADD CONSTRAINT `caso_sospechoso_ibfk_3` FOREIGN KEY (`id_cp`) REFERENCES `punto_control` (`id_cp`);

--
-- Filtros para la tabla `grupo`
--
ALTER TABLE `grupo`
  ADD CONSTRAINT `grupo_ibfk_1` FOREIGN KEY (`id_profesor`) REFERENCES `persona` (`id_persona`),
  ADD CONSTRAINT `grupo_ibfk_2` FOREIGN KEY (`id_materia`) REFERENCES `materia` (`id_materia`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
