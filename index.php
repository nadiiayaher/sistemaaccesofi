<?php
include('routes.php');
session_start();
if(isset($_SESSION['id_usuario']))
{
    switch($_SESSION['tipo_persona'])
    {
        case 1:
            header('Location: '.WEB_ROUTE.'/admin.php');
            break;
        case 2:
            header('Location: '.WEB_ROUTE.'/empleado.php');
            break;
        case 3: // Tipo de usuario profesor
            header('Location: '.WEB_ROUTE.'/profesor.php');
            break;
        default:
        header('Location: '.WEB_ROUTE.'/index.php');
            break;
    }
}
?>
<!doctype html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
    <link href="<?=WEB_ROUTE?>/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link rel="icon" type="image/png" href="<?=WEB_ROUTE?>/img/png/fi_invert.png"/>
    <link rel="stylesheet" href="<?=WEB_ROUTE?>/css/index-style.css">
    <title>Sistema de control de acceso FI UAEMex</title>
</head>
<body id="page-top">
    <div class="container" id="topbar">
        <header class="head">
            <nav class="navbar navbar-expand-lg navbar-light head__custom-nav">
                <a class="navbar-brand d-none d-lg-block d-xl-block" href="" id="main-title">
                    <img class="" height="50" src="<?=WEB_ROUTE?>/img/png/img_logo_uaem18-21.png" alt="Universidad Autonoma del Estado de Mexico">
                </a>
                <a class="navbar-brand d-flex flex-row d-none d-sm-inline-block d-md-inline-block d-lg-none" href="" id="main-title">
                    <img class="" height="50" src="<?=WEB_ROUTE?>/img/svg/logo_fi_uaem_invert.svg" alt="Universidad Autonoma del Estado de Mexico">
                </a>
                <button class="btn navbar-toggler text-center" type="button" id="menuButton" data-toggle="collapse" data-target="#navbarNav">
                    <ion-icon class="mx-auto" name="chevron-down-outline" style="color: black; font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>                        
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item active pt-2 mr-3">
                            <a class="nav-link" href=""><strong>Inicio</strong></a>
                        </li>
                        <li class="nav-item pt-2 mr-3">
                            <a class="nav-link" href="#about">Acerca de</a>
                        </li>
                        <li class="nav-item pt-2 mr-3">
                            <a class="nav-link" href="#about">Ayuda</a>
                        </li>
                        <li class="nav-item d-none d-lg-block d-xl-block pt-2">
                            <a class="btn btn-login-nav" href="<?=WEB_ROUTE?>/login.php">Ingresar</a>
                        </li>
                        <li class="nav-item d-none d-lg-block d-xl-block pt-2">
                            <a href="">
                                <img class="" height="50" src="<?=WEB_ROUTE?>/img/svg/logo_fi_uaem.svg" alt="Universidad Autonoma del Estado de Mexico">
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
    </div>
    
    <div class="container" id="mainContainer">
        <div class="row align-items-center custom-section">
            <div class="col-md-7 mb-4 animate__animated animate__pulse text-white mx-auto" id="main-text-header">
                    <h2 class="mb-3 d-none d-md-inline-block d-lg-inline-block d-xl-inline-block" style="font-size:35px">Bienvenido al <strong style="color:#FFFFFFF0;">Sistema de Control de Acceso de la Facultad de Ingeniería</strong></h2>
                    <h2 class="mb-3 d-sm-inline-block d-md-none d-xl-none" style="font-size:35px"><strong style="color:#FFFFFFF0;">Bienvenido!</strong></h2>
                    
                    <p>Ante la actual contigencia sanitaria, la seguridad de todos y todas dentro de las instalaciones es nuestra prioridad.</p>
                    <div class="my-4">
                        <a class="btn btn-login" href="<?=WEB_ROUTE?>/login.php"><span style="vertical-align:middle;">Ingresar</span></a>
                        <a class="btn btn-more d-none d-lg-inline-block d-xl-inline-block" href="#about"><span style="vertical-align:middle;">Saber más</span></a>
                    </div>
            </div>
            <div class="col-md-5 img-content mx-auto mb-2">
                <img class="main-img" src="<?=WEB_ROUTE?>/img/png/backmain.png" heigth="800px" width="800px">
            </div>
        </div>
    </div>
    
    <div class="container mb-5">

        <h4 class="mb-5" style="color:#333333; text-align:center;"><strong>Recuerda seguir las siguientes medidas de seguridad dentro de las instalaciones para evitar posibles contagios</strong></h4>
        <div class="row my-5">

            <div class="col-md-4 mt-3 mb-4 mx-auto">
                <div class="card shadow border-0 mx-auto h-100" style="width: 18rem; overflow: hidden">
                    <img src="<?=WEB_ROUTE?>/img/png/mask.jpeg" class="" alt="" style="width: 18rem; border-top-left-radius:4px; border-top-right-radius:4px;">
                    <div class="card-body" style="overflow: hidden;">
                        <h5 class="card-title"><strong>Uso de mascarilla</strong></h5>
                        <p class="card-text">Recuerda usar mascarilla o careta en durante tu estadía dentro de las instalaciones de la Facultad.</p>

                    </div>
                </div>
            </div>
            <div class="col-md-4 mt-3 mb-4 mx-auto">
                <div class="card shadow border-0 mx-auto h-100" style="width: 18rem; overflow: hidden">
                    <img src="<?=WEB_ROUTE?>/img/png/wash.jpg" class="" alt="" style="width: 18rem; border-top-left-radius:4px; border-top-right-radius:4px;">
                    <div class="card-body" style="overflow: hidden;">
                        <h5 class="card-title"><strong>Lavado de manos</strong></h5>
                        <p class="card-text">Recuerda lavarte frecuentemente las manos y usar gel antibacterial. Recuerda desinfectar tu calzado en los tapetes sanitizantes.</p>

                    </div>
                </div>
            </div>
            <div class="col-md-4 mt-3 mb-4 mx-auto">
                <div class="card shadow border-0 mx-auto h-100" style="width: 18rem; overflow: hidden">
                    <img src="<?=WEB_ROUTE?>/img/png/dist.jpg" class="" alt="" style="width: 18rem; border-top-left-radius:4px; border-top-right-radius:4px;">
                    <div class="card-body" style="overflow: hidden;">
                        <h5 class="card-title"><strong>Distanciamiento social</strong></h5>
                        <p class="card-text">Recuerda mantener el distanciamiento social. Evita saludar de mano o beso a otras personas.</p>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Start Footer -->
    <footer class="mt-5" style="background:#2A2A2A">
        <div class="container">
            <div class="row">
                <div class="col-md-4 pt-4">
                    <div class="row ml-1 mb-4">
                        <img class="mr-3" src="<?=WEB_ROUTE?>/img/png/fi_invert.png" width="60" height="60" alt="FI UAEM" style="border-radius:4px;">
                        <p class="my-auto text-white" style="font-size:28px; font-weight:600">FI UAEM</p>
                    </div>
                    <h4 class="border-bottom pb-3 border-light text-green-light">Contacto</h4>
                    <ul class="list-unstyled text-light footer-link-list">
                        <li class="mb-3 footer-item">
                            <a href="https://www.google.com.mx/maps/place/Facultad+de+Ingenieria,Universidad+Aut%C3%B3noma+del+Estado+de+M%C3%A9xico/@19.2838166,-99.6788371,17z/data=!3m1!4b1!4m5!3m4!1s0x85cd89b4fab5eacb:0xdae2eb9ae00d8918!8m2!3d19.2838115!4d-99.6766484">
                                <i class="fas fa-map-marker-alt fa-fw"></i>    
                                &nbsp; Facultad de Ingeniería UAEM. Cerro de Coatepec S/N Ciudad Universitaria C.P: 50100. Toluca, Estado de México.
                            </a>
                        </li>
                        <li class="mb-2 footer-item">
                            <a class="" href="tel:7222140855"><i class="fa fa-phone fa-fw"></i>&nbsp; Tels.:(722) 214 08 55 y 214 07 95</a>
                        </li>
                        <li class="mb-1 footer-item">
                            <a class="" href="mailto:info@company.com"><i class="fa fa-envelope fa-fw"></i>&nbsp; sca.fi.uaem@uaemex.mx</a>
                        </li>
                    </ul>
                </div>

                <div class="col-md-4 pt-5">
                    <h4 class="text-green-light border-bottom pb-3 border-light">Sitios de interés</h4>
                    <ul class="list-unstyled text-light footer-link-list">
                        <li class="footer-item"><a class="" href="http://web.uaemex.mx/avisos/Aviso_Privacidad.pdf"><i class="fas fa-external-link-square-alt"></i> &nbsp; Aviso de privacidad</a></li>
                </div>

                <div class="col-md-4 pt-5">
                    <h4 class="text-green-light border-bottom pb-3 border-light">Navegación del sitio</h4>
                    <ul class="list-unstyled text-white">
                        <li class="mb-1 footer-item"><a class="" href="#page-top"><i class="fas fa-home"></i>&nbsp; Inicio</a></li>
                        <li class="mb-1 footer-item"><a class="" href="<?=WEB_ROUTE?>/login.php"><i class="fas fa-user-circle"></i>&nbsp; Iniciar sesión</a></li>
                        <li class="mb-1 footer-item"><a class="" href=""><i class="fas fa-info-circle"></i>&nbsp; Acerca de</a></li>
                        <li class="mb-1 footer-item"><a class="" href=""><i class="fas fa-question-circle"></i>&nbsp; Ayuda</a></li>
                    </ul>
                </div>

            </div>

            <div class="row text-white mb-3">
                <div class="col-12 mb-2">
                    <div class="w-100 my-2 border-top border-light"></div>
                </div>
                <div class="col-auto me-auto">
                    <ul class="list-inline text-left footer-icons">
                        <li class="list-inline-item text-center footer-item">
                            <a class="" href="https://www.facebook.com/FIUAEM/"><ion-icon name="logo-facebook" style="font-size: 37px; vertical-align: middle;"></ion-icon></a>
                        </li>
                        <li class="list-inline-item text-center footer-item">
                            <a class="" href=""></ion-icon><ion-icon name="logo-twitter" style="font-size: 37px; vertical-align: middle;"></ion-icon></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="w-100 bg-black pt-2">
            <div class="container">
                <div class="row pt-2">
                    <div class="col-12">
                        <p class="text-left text-white">
                            "2021, Celebración de los 65 años de la Universidad Autónoma del Estado de México"
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- End Footer -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#">
        <i class="fas fa-angle-up"></i>
    </a>

    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/ionicons@5.2.3/dist/ionicons.js"></script>
    <script src="<?=WEB_ROUTE?>/js/index-func.js"></script>
</body>
</html>