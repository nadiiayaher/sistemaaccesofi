$(document).ready(function () {
    
    id_persona_edit = '';
    id_persona_delete = '';

    tabla_alumnos = $('#dataTable').DataTable({
        "language":{
          "url": "//cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json"
        }
    });

    $('#create-btn').click(function (event) {
        event.preventDefault();
        let new_id = $('#new_id').val();
        let new_email= $('#new_email').val();
        let new_tel= $('#new_tel').val();
        let new_password= $('#new_password').val();
        let new_nombre= $('#new_nombre').val();
        $.ajax({
            type: "POST",
            url: "crear-persona.php",
            data: {
                new_id,
                new_email,
                new_tel,
                new_password,
                new_nombre,
                tipo_persona: 4
            },
           success: function (data) {
               console.log(data);
                let json_data = JSON.parse(data);
                if (json_data.status == 202 ) {
                    $('#new_id').val('');
                    $('#new_email').val('');
                    $('#new_tel').val('');
                    $('#new_password').val('');
                    $('#new_nombre').val('');
                    tabla_alumnos.row.add( $(`
                    <tr>
                        <td>${json_data.id_persona}</td>
                        <td>${json_data.nombre}</td>
                        <td>${json_data.email}</td>
                        <td>${json_data.telefono}</td>
                        <td class="d-flex justify-content-between">
                            <button type="button" class="btn btn-outline-dark open-edit-modal">
                                <ion-icon name="list-outline" style="font-size: 22px; vertical-align: middle;"></ion-icon>
                            </button>
                            <button type="button" class="btn btn-outline-danger open-delete-modal">
                                <ion-icon name="trash-outline" style="font-size: 22px; vertical-align: middle;"></ion-icon>
                            </button>
                        </td>
                    </tr>
                    `)[0]).draw();
                    toastr["success"](json_data.message);
                }
                else if (json_data.status == 404 ) {
                    toastr["error"](json_data.message);
                }
            }
        })
        $('#newModal').modal('hide');
    });

    $('#table-body').on('click','.open-edit-modal', function(event) {
        id_persona_edit = $(this).parent().parent().children()[0].innerHTML;
        console.log(id_persona_edit);
        $.ajax({
            type: "POST",
            url: "edit-persona-get.php",
            data: {
                id_persona: id_persona_edit
            },
            success: function (data) {
                console.log(data);
                let json_data = JSON.parse(data);
                $('#edit_id').val(json_data.id_persona);
                $('#edit_email').val(json_data.email);
                $('#edit_tel').val(json_data.telefono);
                $('#edit_password').val(json_data.password);
                $('#edit_nombre').val(json_data.nombre);
                $('#editModal').modal('show');
            }
        });
    });

    $('#edit-btn').click(function (event) {
        let id_persona = $('#edit_id').val();
        let email = $('#edit_email').val();
        let telefono = $('#edit_tel').val();
        let password = $('#edit_password').val();
        let nombre = $('#edit_nombre').val();
        $('#editModal').modal('hide');
        $('#edit_id').val('');
        $('#edit_email').val('');
        $('#edit_tel').val('');
        $('#edit_password').val('');
        $('#edit_nombre').val('');
        $.ajax({
            type: "POST",
            url: "edit-persona-update.php",
            data: {
                id_persona: id_persona_edit,
                id_persona,
                email,
                telefono,
                password,
                nombre,
                tipo_persona: 4
            },
            success: function (data) {
                console.log(data);
                let json_data = JSON.parse(data);
                if(json_data.status == 202)
                {
                    tabla_alumnos.destroy();
                    $('#table-body').html('');
                    json_personas = JSON.parse(json_data.personas)
                    json_personas.forEach(element => {
                        $('#table-body').append(`
                        <tr>
                            <td>${element.id_persona}</td>
                            <td>${element.nombre}</td>
                            <td>${element.email}</td>
                            <td>${element.telefono}</td>
                            <td class="d-flex justify-content-between">
                                <button type="button" class="btn btn-outline-dark open-edit-modal">
                                    <ion-icon name="list-outline" style="font-size: 22px; vertical-align: middle;"></ion-icon>
                                </button>
                                <button type="button" class="btn btn-outline-danger open-delete-modal">
                                    <ion-icon name="trash-outline" style="font-size: 22px; vertical-align: middle;"></ion-icon>
                                </button>
                            </td>
                        </tr>
                        `);
                    });
                    tabla_alumnos = $('#dataTable').DataTable({
                        "language":{"url":"//cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json"}
                    });
                    toastr["success"](json_data.message);
                }
                else if (json_data.status == 404 ) {
                    toastr["error"](json_data.message);
                }
            }
        });
    });

    $('#table-body').on('click','.open-delete-modal', function(event) {
        $('#deleteModal').modal('show');
        id_persona_delete = $(this).parent().parent().children()[0].innerHTML;
    });

    $('#delete-btn').click( function(event) {
        $('#deleteModal').modal('hide');
        $.ajax({
            type: "POST",
            url: "delete-persona.php",
            data: {
                id_persona: id_persona_delete,
                tipo_persona: 4
            },
            success: function (data) {
                let json_data = JSON.parse(data);
                if(json_data.status == 202){
                    tabla_alumnos.destroy();
                    $('#table-body').html('');
                    json_alumnos = JSON.parse(json_data.personas)
                    json_alumnos.forEach(element => {
                        $('#table-body').append(`
                        <tr>
                            <td>${element.id_persona}</td>
                            <td>${element.nombre}</td>
                            <td>${element.email}</td>
                            <td>${element.telefono}</td>
                            <td class="d-flex justify-content-between">
                                <button type="button" class="btn btn-outline-dark open-edit-modal">
                                    <ion-icon name="list-outline" style="font-size: 22px; vertical-align: middle;"></ion-icon>
                                </button>
                                <button type="button" class="btn btn-outline-danger open-delete-modal">
                                    <ion-icon name="trash-outline" style="font-size: 22px; vertical-align: middle;"></ion-icon>
                                </button>
                            </td>
                        </tr>
                        `);
                    });
                    tabla_alumnos = $('#dataTable').DataTable({
                        "language":{"url":"//cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json"}
                    });
                    toastr["success"](json_data.message);
                }
                else if (json_data.status == 404 ) {
                    toastr["error"](json_data.message);
                }
            }
        });
    });
});



toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": true,
    "positionClass": "toast-bottom-left",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "8000",
    "hideDuration": "1000",
    "timeOut": "8000",
    "extendedTimeOut": "6000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}