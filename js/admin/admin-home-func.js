$(document).ready( function () {
    getDayStats();
});

function getDayStats() {
    addLoader($('#loader_container'));
    let day = $('#date_main').val();
    getStats(day,day, function (data) {
        console.log(data);
        let json_data = JSON.parse(data);
        if (json_data.status == 202) {
            let accessCol = buildAccessCol(json_data);
            let casosCol = buildCasosCol(json_data);
            let activosCol = buildActivosCol(json_data);
            $('#stats_container').html('');
            $('#stats_container').append(accessCol);
            $('#stats_container').append(casosCol);
            $('#stats_container').append(activosCol);
        }
        else{
            addErrorMessage( $('#stats-container'), json_data.message);
        }
    });
}

function getStats(first_day,last_day,callback) {
    $.ajax({
      type: "POST",
      url: "admin-files/get-estadisticas.php",
      data: {
        first_day,
        last_day,
        actual_first_day: first_day,
        actual_last_day: last_day,
      },
      success: callback,
    });
}

function addLoader(element) {
    element.html(`
        <div class="text-center mt-5 loader">
            <div class="spinner-grow" role="status" style="color:#597552;">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
    `);
}

function addErrorMessage(element,message) {
    element.html(`
        <h6 class="text-center text-danger mt-5">${message}</h6>
    `);   
}

function buildAccessCol(json_data) {
    if (parseInt(json_data.accesos_totales) == 0) {
        alumnos_percent = 0;
        profesores_percent = 0;
        administrativos_percent = 0;
        externos_percent = 0;
        encargados_percent = 0;
        admins_percent = 0;
    }
    else{
        alumnos_percent = ( parseInt(json_data.accesos_alumnos) / parseInt(json_data.accesos_totales) ) * 100;
        profesores_percent = ( parseInt(json_data.accesos_profesores) / parseInt(json_data.accesos_totales) ) * 100;
        administrativos_percent = ( parseInt(json_data.accesos_administrativos) / parseInt(json_data.accesos_totales) ) * 100;
        externos_percent = ( parseInt(json_data.accesos_externos) / parseInt(json_data.accesos_totales) ) * 100;
    }
    let bar_char = `
    <div class="col-md-4">
        <div class="card card-top p-0 border-green-med mb-4 shadow" style="text-decoration:none">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col-10">
                        <div class="card-top-title">Accesos totales registrados</div>
                        <div class="card-top-content">${json_data.accesos_totales} personas</div>
                    </div>
                    <div class="col-2">
                        <ion-icon name="walk-outline" style="color: gray; font-size: 40px;"></ion-icon>
                    </div>
                </div>
            </div>
        </div>
        <div class="card shadow border-0 mb-4">
            <div class="card-header bg-white border-0">
                <div class="d-flex justify-content-between">
                    <p class="m-0 text-green-med" style="font-weight:bold; font-size:13px;">Accesos por tipo de usuario</p>
                </div>
            </div>
            <div class="card-body">
                <h4 class="small font-weight-bold">${json_data.accesos_alumnos} Alumnos<span class="float-right">${alumnos_percent}%</span></h4>
                <div class="progress mb-4">
                    <div class="progress-bar bg-danger" role="progressbar" style="width: ${alumnos_percent}%" aria-valuenow="${alumnos_percent}" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <h4 class="small font-weight-bold">${json_data.accesos_profesores} Profesores<span class="float-right">${profesores_percent}%</span></h4>
                <div class="progress mb-4">
                    <div class="progress-bar bg-warning" role="progressbar" style="width: ${profesores_percent}%" aria-valuenow="${profesores_percent}" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <h4 class="small font-weight-bold">${json_data.accesos_administrativos} Administrativos / Empleados<span class="float-right">${administrativos_percent}%</span></h4>
                <div class="progress mb-4">
                    <div class="progress-bar" role="progressbar" style="width: ${administrativos_percent}%" aria-valuenow="${administrativos_percent}" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <h4 class="small font-weight-bold">${json_data.accesos_externos} Usuarios externos<span class="float-right">${externos_percent}%</span></h4>
                <div class="progress mb-1">
                    <div class="progress-bar bg-info" role="progressbar" style="width: ${externos_percent}%" aria-valuenow="${externos_percent}" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
        </div>
    </div>`;
    return bar_char;
}

function buildCasosCol(json_data) {
    if (parseInt(json_data.casos_totales) == 0) {
        alumnos_percent = 0;
        profesores_percent = 0;
        administrativos_percent = 0;
        externos_percent = 0;
        encargados_percent = 0;
        admins_percent = 0;
    }
    else{
        alumnos_percent = ( parseInt(json_data.casos_alumnos) / parseInt(json_data.casos_totales) ) * 100;
        profesores_percent = ( parseInt(json_data.casos_profesores) / parseInt(json_data.casos_totales) ) * 100;
        administrativos_percent = ( parseInt(json_data.casos_administrativos) / parseInt(json_data.casos_totales) ) * 100;
        externos_percent = ( parseInt(json_data.casos_externos) / parseInt(json_data.casos_totales) ) * 100;
    }
    let bar_char = `
    <div class="col-md-4">
        <div class="card card-top border-secondary mb-4 shadow" style="text-decoration:none">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col-10">
                        <div class="card-top-title">Casos sospechosos registrados</div>
                        <div class="card-top-content">${json_data.casos_totales} personas</div>
                    </div>
                    <div class="col-2">
                        <ion-icon name="warning-outline" style="color: gray; font-size: 40px;"></ion-icon>
                    </div>
                </div>
            </div>
        </div>
        <div class="card shadow border-0 mb-4">
            <div class="card-header bg-white border-0">
                <div class="d-flex justify-content-between">
                    <p class="m-0 text-secondary" style="font-weight:bold; font-size:13px;">Casos sospechosos por tipo de usuario</p>
                </div>
            </div>
            <div class="card-body">
                <h4 class="small font-weight-bold">${json_data.casos_alumnos} Alumnos<span class="float-right">${alumnos_percent}%</span></h4>
                <div class="progress mb-4">
                    <div class="progress-bar bg-danger" role="progressbar" style="width: ${alumnos_percent}%" aria-valuenow="${alumnos_percent}" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <h4 class="small font-weight-bold">${json_data.casos_profesores} Profesores<span class="float-right">${profesores_percent}%</span></h4>
                <div class="progress mb-4">
                    <div class="progress-bar bg-warning" role="progressbar" style="width: ${profesores_percent}%" aria-valuenow="${profesores_percent}" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <h4 class="small font-weight-bold">${json_data.casos_administrativos} Administrativos / Empleados<span class="float-right">${administrativos_percent}%</span></h4>
                <div class="progress mb-4">
                    <div class="progress-bar" role="progressbar" style="width: ${administrativos_percent}%" aria-valuenow="${administrativos_percent}" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <h4 class="small font-weight-bold">${json_data.casos_externos} Usuarios externos<span class="float-right">${externos_percent}%</span></h4>
                <div class="progress mb-1">
                    <div class="progress-bar bg-info" role="progressbar" style="width: ${externos_percent}%" aria-valuenow="${externos_percent}" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
        </div>
    </div>`;
    return bar_char;
}

function buildActivosCol(json_data) {
    if (parseInt(json_data.casos_activos_totales) == 0) {
        alumnos_percent = 0;
        profesores_percent = 0;
        administrativos_percent = 0;
        externos_percent = 0;
        encargados_percent = 0;
        admins_percent = 0;
    }
    else{
        alumnos_percent = ( parseInt(json_data.casos_activos_alumnos) / parseInt(json_data.casos_activos_totales) ) * 100;
        profesores_percent = ( parseInt(json_data.casos_activos_profesores) / parseInt(json_data.casos_activos_totales) ) * 100;
        administrativos_percent = ( parseInt(json_data.casos_activos_administrativos) / parseInt(json_data.casos_activos_totales) ) * 100;
        externos_percent = ( parseInt(json_data.casos_activos_externos) / parseInt(json_data.casos_activos_totales) ) * 100;
    }
    let bar_char = `
    <div class="col-md-4">
        <div class="card card-top border-danger mb-4 shadow" style="text-decoration:none">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col-10">
                        <div class="card-top-title">Casos sospechosos activos</div>
                        <div class="card-top-content">${json_data.casos_activos_totales} personas</div>
                    </div>
                    <div class="col-2">
                        <ion-icon name="bandage-outline" style="color: gray; font-size: 40px;"></ion-icon>
                    </div>
                </div>
            </div>
        </div>
        <div class="card shadow border-0 mb-4">
            <div class="card-header bg-white border-0">
                <div class="d-flex justify-content-between">
                    <p class="m-0 text-danger" style="font-weight:bold; font-size:13px;">Casos activos por tipo de usuario</p>
                </div>
            </div>
            <div class="card-body">
                <h4 class="small font-weight-bold">${json_data.casos_activos_alumnos} Alumnos<span class="float-right">${alumnos_percent}%</span></h4>
                <div class="progress mb-4">
                    <div class="progress-bar bg-danger" role="progressbar" style="width: ${alumnos_percent}%" aria-valuenow="${alumnos_percent}" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <h4 class="small font-weight-bold">${json_data.casos_activos_profesores} Profesores<span class="float-right">${profesores_percent}%</span></h4>
                <div class="progress mb-4">
                    <div class="progress-bar bg-warning" role="progressbar" style="width: ${profesores_percent}%" aria-valuenow="${profesores_percent}" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <h4 class="small font-weight-bold">${json_data.casos_activos_administrativos} Administrativos / Empleados<span class="float-right">${administrativos_percent}%</span></h4>
                <div class="progress mb-4">
                    <div class="progress-bar" role="progressbar" style="width: ${administrativos_percent}%" aria-valuenow="${administrativos_percent}" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <h4 class="small font-weight-bold">${json_data.casos_activos_externos} Usuarios externos<span class="float-right">${externos_percent}%</span></h4>
                <div class="progress mb-1">
                    <div class="progress-bar bg-info" role="progressbar" style="width: ${externos_percent}%" aria-valuenow="${externos_percent}" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
        </div>
    </div>`;
    return bar_char;
}