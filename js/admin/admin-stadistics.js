$(document).ready( function () {
    moment.locale();
    tabla_accesos_day = null;
    tabla_casos_day = null;

    tabla_accesos_week = null;
    tabla_casos_week = null;

    tabla_accesos_month = null;
    tabla_casos_month = null;

    tabla_accesos_year = null;
    tabla_casos_year = null;

    tabla_accesos_custom = null;
    tabla_casos_custom = null;

    web_route = $('#day_download_btn').attr("href");

    actual_day = $('#input_day').val();
    $('#input_day').attr("max",actual_day);

    let actualLastDate = moment(actual_day, "YYYY-MM-DD").day(6).format("YYYY-MM-DD");
    $('#input_week').attr("max",actualLastDate);

    $('#input_custom_first').attr("max",actual_day);
    $('#input_custom_last').attr("max",actual_day);
    
    getDayStats();

    $('#pills-week-tab').click( function (event) {
        getWeekStats();
    });

    $('#pills-month-tab').click( function (event) {
        getMonthStats();
    });

    $('#pills-year-tab').click( function (event) {
        getYearStats();
    });

    $('#day_stats_btn').click( function (event) {
        getDayStats();
    });

    $('#week_stats_btn').click( function (event) {
        getWeekStats();
    });

    $('#month_stats_btn').click( function (event) {
        getMonthStats();
    });

    $('#custom-stats-btn').click( function (event) {
        let first_day = $('#input_custom_first').val();
        let last_day = $('#input_custom_last').val();
        if (first_day != "" && last_day != "") {
            getCustomStats();   
        }
        else {
            if (first_day == "") {
                $('#input_custom_first').removeClass("form-input");
                $('#input_custom_first').addClass("is-invalid");
                $('#first_day_msg').html("Selecciona una fecha inicial");
            }
            if (last_day == "") {
                $('#input_custom_last').removeClass("form-input");
                $('#input_custom_last').addClass("is-invalid");
                $('#last_day_msg').html("Selecciona una fecha final");
            }
        }
    });

    $('#input_week').change( function (event) {
        let base_day = $('#input_week').val();
        var firstDate = moment(base_day, "YYYY-MM-DD").day(0).format("YYYY-MM-DD");
        var lastDate =  moment(base_day, "YYYY-MM-DD").day(6).format("YYYY-MM-DD");
        $('#disabled_week_first').val(firstDate);
        $('#disabled_week_last').val(lastDate);
    });

    $('#input_custom_first').change( function (event) {
        if ($('#input_custom_first').val() != "") {
            $('#input_custom_first').addClass("form-input");
            $('#input_custom_first').removeClass("is-invalid");
            $('#first_day_msg').html("");
        }
    });

    $('#input_custom_last').change( function (event) {
        if ($('#input_custom_last').val() != "") {
            $('#input_custom_last').addClass("form-input");
            $('#input_custom_last').removeClass("is-invalid");
            $('#last_day_msg').html("");
        }
    });

});

function getDayStats() {
    if (tabla_accesos_day != null) {
        tabla_accesos_day.destroy();
    }
    if (tabla_casos_day != null) {
        tabla_casos_day.destroy();
    }
    $('#tables-day-container').addClass('d-none');
    addLoader( $('#stats-day-container') );
    let day = $('#input_day').val();
    getStats(day,day,actual_day,actual_day, function (data) {
        console.log(data);
        let json_data = JSON.parse(data);
        if (json_data.status == 202) {
            let accesosCol = buildAccessCol(json_data);
            let casosCol = buildCasosCol(json_data);
            let accesosTable = buildTable(json_data.accesos,'tabla_accesos_day');
            let casosTable = buildTable(json_data.casos_sospechosos,'tabla_casos_day');
            $('#stats-day-container').html('');
            $('#stats-day-container').append(accesosCol);
            $('#stats-day-container').append(casosCol);
            $('#accesos-day-pane').html(accesosTable);
            $('#casos-day-pane').html(casosTable);
            $('#tables-day-container').removeClass('d-none');
            tabla_accesos_day = $('#tabla_accesos_day').DataTable({
                "language":{
                  "url": "//cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json"
                }
            });
            tabla_casos_day = $('#tabla_casos_day').DataTable({
                "language":{
                  "url": "//cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json"
                }
            });
            $('#day_download_btn').attr("href",web_route + "/admin-files/get-stats-file.php?first_day="+day+"&last_day="+day);
        }
        else{
            addErrorMessage( $('#stats-day-container'),json_data);
            $('#tables-day-container').addClass('d-none');
        }
    });
}

function getWeekStats() {
    if (tabla_accesos_week != null) {
        tabla_accesos_week.destroy();
    }
    if (tabla_casos_week != null) {
        tabla_casos_week.destroy();
    }
    $('#tables-week-container').addClass('d-none');
    addLoader($('#stats-week-container'));
    let base_day = $('#input_week').val();
    let firstDate = moment(base_day, "YYYY-MM-DD").day(0).format("YYYY-MM-DD");
    let lastDate =  moment(base_day, "YYYY-MM-DD").day(6).format("YYYY-MM-DD");
    let actualFirstDate = moment(actual_day, "YYYY-MM-DD").day(0).format("YYYY-MM-DD");
    let actualLastDate = moment(actual_day, "YYYY-MM-DD").day(6).format("YYYY-MM-DD");
    $('#disabled_week_first').val(firstDate);
    $('#disabled_week_last').val(lastDate);
    getStats(firstDate,lastDate,actualFirstDate,actualLastDate, function (data) {
        let json_data = JSON.parse(data);
        if (json_data.status == 202) {
            let accesosCol = buildAccessCol(json_data);
            let casosCol = buildCasosCol(json_data);
            let accesosTable = buildTable(json_data.accesos,'tabla_accesos_week');
            let casosTable = buildTable(json_data.casos_sospechosos,'tabla_casos_week');
            $('#stats-week-container').html('');
            $('#stats-week-container').append(accesosCol);
            $('#stats-week-container').append(casosCol);
            $('#accesos-week-pane').html(accesosTable);
            $('#casos-week-pane').html(casosTable);
            $('#tables-week-container').removeClass('d-none');
            tabla_accesos_week = $('#tabla_accesos_week').DataTable({
                "language":{
                  "url": "//cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json"
                }
            });
            tabla_casos_week = $('#tabla_casos_week').DataTable({
                "language":{
                  "url": "//cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json"
                }
            });
            $('#week_download_btn').attr("href",web_route + "/admin-files/get-stats-file.php?first_day="+firstDate+"&last_day="+lastDate);
        }
        else{
            addErrorMessage( $('#stats-week-container'),json_data);
            $('#tables-week-container').addClass('d-none');
        }
    });
}

function getMonthStats() {
    if (tabla_accesos_month != null) {
        tabla_accesos_month.destroy();
    }
    if (tabla_casos_month != null) {
        tabla_casos_month.destroy();
    }
    $('#tables-month-container').addClass('d-none');
    addLoader( $('#stats-month-container'));
    let month = $('#input_month_month').val();
    let year =  $('#input_month_year').val();
    let base_day =  year + "-" + month + "-01"; 
    let startOfMonth = moment(base_day, "YYYY-MM-DD").startOf('month').format('YYYY-MM-DD');
    let endOfMonth   = moment(base_day, "YYYY-MM-DD").endOf('month').format('YYYY-MM-DD');
    let actualStartOfMonth = moment(actual_day, "YYYY-MM-DD").startOf('month').format('YYYY-MM-DD');
    let actualEndOfMonth = moment(actual_day, "YYYY-MM-DD").endOf('month').format('YYYY-MM-DD');
    getStats(startOfMonth,endOfMonth,actualStartOfMonth,actualEndOfMonth,function (data) {
        let json_data = JSON.parse(data);
        if (json_data.status == 202) {
            let accesosCol = buildAccessCol(json_data);
            let casosCol = buildCasosCol(json_data);
            let accesosTable = buildTable(json_data.accesos,'tabla_accesos_month');
            let casosTable = buildTable(json_data.casos_sospechosos,'tabla_casos_month');
            $('#stats-month-container').html('');
            $('#stats-month-container').append(accesosCol);
            $('#stats-month-container').append(casosCol);
            $('#accesos-month-pane').html(accesosTable);
            $('#casos-month-pane').html(casosTable);
            $('#tables-month-container').removeClass('d-none');
            tabla_accesos_month = $('#tabla_accesos_month').DataTable({
                "language":{
                  "url": "//cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json"
                }
            });
            tabla_casos_month = $('#tabla_casos_month').DataTable({
                "language":{
                  "url": "//cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json"
                }
            });
            $('#month_download_btn').attr("href",web_route + "/admin-files/get-stats-file.php?first_day="+startOfMonth+"&last_day="+endOfMonth);
        }
        else{
            addErrorMessage( $('#stats-month-container'),json_data);
            $('#tables-month-container').addClass('d-none');
        }
    });
}

function getYearStats() {
    if (tabla_accesos_year != null) {
        tabla_accesos_year.destroy();
    }
    if (tabla_casos_year != null) {
        tabla_casos_year.destroy();
    }
    $('#tables-year-container').addClass('d-none');
    addLoader($('#stats-year-container'));
    let year = $('#input_year').val();
    let startOfYear = year +  "-01-01";
    let endOfYear = year + "-12-31";
    actual_year = actual_day.split("-")[0];
    let actualStartOfYear = actual_year +  "-01-01";
    let actualEndOfYear = actual_year + "-12-31";
    getStats(startOfYear,endOfYear,actualStartOfYear,actualEndOfYear,function (data) {
        let json_data = JSON.parse(data);
        if (json_data.status == 202) {
            let accesosCol = buildAccessCol(json_data);
            let casosCol = buildCasosCol(json_data);
            let accesosTable = buildTable(json_data.accesos,'tabla_accesos_year');
            let casosTable = buildTable(json_data.casos_sospechosos,'tabla_casos_year');
            $('#stats-year-container').html('');
            $('#stats-year-container').append(accesosCol);
            $('#stats-year-container').append(casosCol);
            $('#accesos-year-pane').html(accesosTable);
            $('#casos-year-pane').html(casosTable);
            $('#tables-year-container').removeClass('d-none');
            tabla_accesos_year = $('#tabla_accesos_year').DataTable({
                "language":{
                  "url": "//cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json"
                }
            });
            tabla_casos_year = $('#tabla_casos_year').DataTable({
                "language":{
                  "url": "//cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json"
                }
            });
            $('#year_download_btn').attr("href",web_route + "/admin-files/get-stats-file.php?first_day="+startOfYear+"&last_day="+endOfYear);
        }
        else {
            addErrorMessage( $('#stats-year-container'),json_data);
            $('#tables-year-container').addClass('d-none');
        }
    });
}

function getCustomStats() {
    if (tabla_accesos_custom != null) {
        tabla_accesos_custom.destroy();
    }
    if (tabla_casos_custom != null) {
        tabla_casos_custom.destroy();
    }
    $('#tables-custom-container').addClass('d-none');
    addLoader( $('#stats-custom-container') );
    let first_day = $('#input_custom_first').val();
    let last_day = $('#input_custom_last').val();
    getStats(first_day,last_day,actual_day,actual_day,function (data) {
        let json_data = JSON.parse(data);
        if (json_data.status == 202) {
            let accesosCol = buildAccessCol(json_data);
            let casosCol = buildCasosCol(json_data);
            let accesosTable = buildTable(json_data.accesos,'tabla_accesos_custom');
            let casosTable = buildTable(json_data.casos_sospechosos,'tabla_casos_custom');
            $('#stats-custom-container').html('');
            $('#stats-custom-container').append(accesosCol);
            $('#stats-custom-container').append(casosCol);
            $('#accesos-custom-pane').html(accesosTable);
            $('#casos-custom-pane').html(casosTable);
            $('#tables-custom-container').removeClass('d-none');
            tabla_accesos_custom = $('#tabla_accesos_custom').DataTable({
                "language":{
                  "url": "//cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json"
                }
            });
            tabla_casos_custom = $('#tabla_casos_custom').DataTable({
                "language":{
                  "url": "//cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json"
                }
            });
            $('#custom_download_btn').attr("href",web_route + "/admin-files/get-stats-file.php?first_day="+first_day+"&last_day="+last_day);
        }
        else
        {
            addErrorMessage( $('#stats-custom-container'),json_data);
            $('#tables-custom-container').addClass('d-none');
        }
    })
}

function getStats(first_day,last_day,actual_first_day,actual_last_day,callback) {
    $.ajax({
      type: "POST",
      url: "get-estadisticas.php",
      data: {
        first_day,
        last_day,
        actual_first_day,
        actual_last_day,
      },
      success: callback,
    });
}

function addLoader(element) {
    element.html(`
    <div class="col-md-12">
        <div class="text-center mt-5 loader">
            <div class="spinner-grow" role="status" style="color:#597552;">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
    </div>
    `);
}

function addErrorMessage(element,data) {
    element.html(`
    <div class="col-md-12 mt-5 mx-auto text-center text-secondary">
        <i class='bx bx-ghost' style="font-size:80px;"></i>
        <h5 class="mb-0 mb-0 mt-2" style="font-size:18px; font-weight:bold;">${data.title}.</h5>
        <p class="mt-2 pt-0" style="font-size:14px;">${data.message}.</p>
    </div>
    `);   
}

function buildAccessCol(json_data) {
    if (parseInt(json_data.accesos_totales) == 0) {
        alumnos_percent = 0;
        profesores_percent = 0;
        administrativos_percent = 0;
        externos_percent = 0;
        encargados_percent = 0;
        admins_percent = 0;
    }
    else{
        alumnos_percent = ( parseInt(json_data.accesos_alumnos) / parseInt(json_data.accesos_totales) ) * 100;
        profesores_percent = ( parseInt(json_data.accesos_profesores) / parseInt(json_data.accesos_totales) ) * 100;
        administrativos_percent = ( parseInt(json_data.accesos_administrativos) / parseInt(json_data.accesos_totales) ) * 100;
        externos_percent = ( parseInt(json_data.accesos_externos) / parseInt(json_data.accesos_totales) ) * 100;
    }
    let bar_char = `
    <div class="col-md-6 mt-5">
        <div class="card card-top p-0 border-green-med mb-4 shadow" style="text-decoration:none">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col-10">
                        <div class="card-top-title">Accesos totales registrados</div>
                        <div class="card-top-content">${json_data.accesos_totales} personas</div>
                    </div>
                    <div class="col-2 text-green-med">
                        <ion-icon name="walk-outline" style="font-size: 40px;"></ion-icon>
                    </div>
                </div>
            </div>
        </div>
        <div class="card shadow border-0 mb-4">
            <div class="card-header bg-white border-0">
                <div class="d-flex justify-content-between">
                    <p class="m-0 text-green-med" style="font-weight:bold; font-size:13px;">Accesos por tipo de usuario</p>
                </div>
            </div>
            <div class="card-body">
                <h4 class="small font-weight-bold">${json_data.accesos_alumnos} Alumnos<span class="float-right">${alumnos_percent}%</span></h4>
                <div class="progress mb-4">
                    <div class="progress-bar bg-danger" role="progressbar" style="width: ${alumnos_percent}%" aria-valuenow="${alumnos_percent}" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <h4 class="small font-weight-bold">${json_data.accesos_profesores} Profesores<span class="float-right">${profesores_percent}%</span></h4>
                <div class="progress mb-4">
                    <div class="progress-bar bg-warning" role="progressbar" style="width: ${profesores_percent}%" aria-valuenow="${profesores_percent}" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <h4 class="small font-weight-bold">${json_data.accesos_administrativos} Administrativos / Empleados<span class="float-right">${administrativos_percent}%</span></h4>
                <div class="progress mb-4">
                    <div class="progress-bar" role="progressbar" style="width: ${administrativos_percent}%" aria-valuenow="${administrativos_percent}" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <h4 class="small font-weight-bold">${json_data.accesos_externos} Usuarios externos<span class="float-right">${externos_percent}%</span></h4>
                <div class="progress mb-1">
                    <div class="progress-bar bg-info" role="progressbar" style="width: ${externos_percent}%" aria-valuenow="${externos_percent}" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
        </div>
    </div>`;
    return bar_char;
}

function buildCasosCol(json_data) {
    if (parseInt(json_data.casos_totales) == 0) {
        alumnos_percent = 0;
        profesores_percent = 0;
        administrativos_percent = 0;
        externos_percent = 0;
        encargados_percent = 0;
        admins_percent = 0;
    }
    else{
        alumnos_percent = ( parseInt(json_data.casos_alumnos) / parseInt(json_data.casos_totales) ) * 100;
        profesores_percent = ( parseInt(json_data.casos_profesores) / parseInt(json_data.casos_totales) ) * 100;
        administrativos_percent = ( parseInt(json_data.casos_administrativos) / parseInt(json_data.casos_totales) ) * 100;
        externos_percent = ( parseInt(json_data.casos_externos) / parseInt(json_data.casos_totales) ) * 100;
    }
    let bar_char = `
    <div class="col-md-6 mt-5">
        <div class="card card-top border-secondary mb-4 shadow" style="text-decoration:none">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col-10">
                        <div class="card-top-title">Casos sospechosos registrados</div>
                        <div class="card-top-content">${json_data.casos_totales} personas</div>
                    </div>
                    <div class="col-2 text-secondary">
                        <ion-icon name="warning-outline" style="font-size: 40px;"></ion-icon>
                    </div>
                </div>
            </div>
        </div>
        <div class="card shadow border-0 mb-4">
            <div class="card-header bg-white border-0">
                <div class="d-flex justify-content-between">
                    <p class="m-0 text-secondary" style="font-weight:bold; font-size:13px;">Casos sospechosos por tipo de usuario</p>
                </div>
            </div>
            <div class="card-body">
                <h4 class="small font-weight-bold">${json_data.casos_alumnos} Alumnos<span class="float-right">${alumnos_percent}%</span></h4>
                <div class="progress mb-4">
                    <div class="progress-bar bg-danger" role="progressbar" style="width: ${alumnos_percent}%" aria-valuenow="${alumnos_percent}" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <h4 class="small font-weight-bold">${json_data.casos_profesores} Profesores<span class="float-right">${profesores_percent}%</span></h4>
                <div class="progress mb-4">
                    <div class="progress-bar bg-warning" role="progressbar" style="width: ${profesores_percent}%" aria-valuenow="${profesores_percent}" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <h4 class="small font-weight-bold">${json_data.casos_administrativos} Administrativos / Empleados<span class="float-right">${administrativos_percent}%</span></h4>
                <div class="progress mb-4">
                    <div class="progress-bar" role="progressbar" style="width: ${administrativos_percent}%" aria-valuenow="${administrativos_percent}" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <h4 class="small font-weight-bold">${json_data.casos_externos} Usuarios externos<span class="float-right">${externos_percent}%</span></h4>
                <div class="progress mb-1">
                    <div class="progress-bar bg-info" role="progressbar" style="width: ${externos_percent}%" aria-valuenow="${externos_percent}" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
        </div>
    </div>`;
    return bar_char;
}

function buildTable(data,id_tabla) {
    json_data = JSON.parse(data);
    table = `
    <div class="table-responsive">
        <table class="table datatable table-hover table-bordered table-condensed" id="${id_tabla}" width="100%" cellspacing="0">
            <thead class="text-center">
                <tr class="text-white">
                    <th>Matrícula</th>
                    <th>Nombre</th>
                    <th>Tipo de usuario</th>
                    <th>Hora de registro</th>
                    <th>Fecha de registro</th>
                    <th>Registrado por</th>
                    <th>Punto de control</th>
                    <th>Detalles</th>
                </tr>
            </thead>
            <tfoot class="text-center">
                <tr class="text-white">
                    <th>Matrícula</th>
                    <th>Nombre</th>
                    <th>Tipo de usuario</th>
                    <th>Hora de registro</th>
                    <th>Fecha de registro</th>
                    <th>Registrado por</th>
                    <th>Punto de control</th>
                    <th>Detalles</th>
                </tr>
            </tfoot>
            <tbody class="text-center" id="">`;
            json_data.forEach(index => {
                table += `
                <tr>
                    <td>${index.id_persona}</td>
                    <td>${index.nombre}</td>
                    <td>${getUserTypeString(index.tipo_persona)}</td>
                    <td>${index.hora_reg}</td>
                    <td>${index.fecha_reg}</td>
                    <td>${index.encargado}</td>
                    <td>${index.punto_control}</td>
                    <td>${index.detalles}</td>
                </tr>`;
            });
            table += `
            </tbody>
        </table>
    </div>
    `;
    return table;
}

function getUserTypeString(usertype) {
    if (usertype == 1) {
        return "ADMINISTRATIVO / EMPLEADO";
    }
    else if( usertype == 2 )
    {
        return "ENCARGADO DE PUNTO DE CONTROL";
    }
    else if( usertype == 3 )
    {
        return "PROFESOR";
    } 
    else if ( usertype == 4 ) {
        return "ALUMNO";
    }      
    else if ( usertype == 5 ) {
        return "USUARIO EXTERNO";
    }
    else if ( usertype == 6 ) {
        return "ADMINISTRATIVO / EMPLEADO";
    }
    else 
    {
        return "NULL";
    }
}
