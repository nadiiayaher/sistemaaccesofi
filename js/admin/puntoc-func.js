$(document).ready(function () {
    
    id_cp_edit = '';
    id_cp_delete = '';

    tabla_cp = $('#dataTable').DataTable({
        "language":{
          "url": "//cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json"
        }
    });

    $('#create-btn').click(function (event) {
        event.preventDefault();
        let nombre = $('#new_cp_nombre').val();
        $('#newModal').modal('hide');
        $.ajax({
            type: "POST",
            url: "crear-puntoc.php",
            data: {
                nombre
            },
            success: function (data) {
                let json_data = JSON.parse(data);
                if (json_data.status == 202 ) {
                    $('#new_cp_nombre').val('');
                    tabla_cp.row.add( $(`
                    <tr>
                        <td>${json_data.id_cp}</td>
                        <td>${json_data.nombre}</td>
                        <td class="d-flex justify-content-between">
                            <button type="button" class="btn btn-outline-dark open-edit-modal ml-auto mr-1">
                                <ion-icon name="list-outline" style="font-size: 22px; vertical-align: middle;"></ion-icon>
                            </button>
                            <button type="button" class="btn btn-outline-danger open-delete-modal mr-auto ml-1">
                                <ion-icon name="trash-outline" style="font-size: 22px; vertical-align: middle;"></ion-icon>
                            </button>
                        </td>
                    </tr>
                    `)[0]).draw();
                    toastr["success"](json_data.message);
                }
                else if (json_data.status == 404 ) {
                    toastr["error"](json_data.message);
                }
            }
        })
    });

    $('#table-body').on('click','.open-edit-modal', function(event) {
        id_cp_edit = $(this).parent().parent().children()[0].innerHTML;
        cp_descripcion = $(this).parent().parent().children()[1].innerHTML;
        $('#edit_id_cp').val(id_cp_edit);
        $('#edit_cp_nombre').val(cp_descripcion);
        $('#editModal').modal('show');
    });

    $('#edit-btn').click(function (event) {
        let descripcion = $('#edit_cp_nombre').val();
        $('#editModal').modal('hide');
        $('#edit_id_cp').val('');
        $('#edit_cp_nombre').val('');
        $.ajax({
            type: "POST",
            url: "edit-puntoc-update.php",
            data: {
                id_cp: id_cp_edit,
                descripcion
            },
            success: function (data) {
                let json_data = JSON.parse(data);
                if(json_data.status == 202)
                {
                    tabla_cp.destroy();
                    $('#table-body').html('');
                    json_cps = JSON.parse(json_data.cps)
                    json_cps.forEach(element => {
                        $('#table-body').append(`
                        <tr>
                            <td>${element.id_cp}</td>
                            <td>${element.descripcion}</td>
                            <td class="d-flex justify-content-between">
                                <button type="button" class="btn btn-outline-dark open-edit-modal ml-auto mr-1">
                                    <ion-icon name="list-outline" style="font-size: 22px; vertical-align: middle;"></ion-icon>
                                </button>
                                <button type="button" class="btn btn-outline-danger open-delete-modal mr-auto ml-1">
                                    <ion-icon name="trash-outline" style="font-size: 22px; vertical-align: middle;"></ion-icon>
                                </button>
                            </td>
                        </tr>
                        `);
                    });
                    tabla_cp = $('#dataTable').DataTable({
                        "language":{"url":"//cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json"}
                    });
                    toastr["success"](json_data.message);
                }
                else if (json_data.status == 404 ) {
                    toastr["error"](json_data.message);
                }
            }
        });
    });

    $('#table-body').on('click','.open-delete-modal', function(event) {
        $('#deleteModal').modal('show');
        id_cp_delete = $(this).parent().parent().children()[0].innerHTML;
    });

    $('#delete-btn').click( function(event) {
        $('#deleteModal').modal('hide');
        $.ajax({
            type: "POST",
            url: "delete-puntoc.php",
            data: {
                id_cp: id_cp_delete
            },
            success: function (data) {
                let json_data = JSON.parse(data);
                if(json_data.status == 202)
                {
                    tabla_cp.destroy();
                    $('#table-body').html('');
                    json_cps = JSON.parse(json_data.cps)
                    json_cps.forEach(element => {
                        $('#table-body').append(`
                        <tr>
                            <td>${element.id_cp}</td>
                            <td>${element.descripcion}</td>
                            <td class="d-flex justify-content-between">
                                <button type="button" class="btn btn-outline-dark open-edit-modal ml-auto mr-1">
                                    <ion-icon name="list-outline" style="font-size: 22px; vertical-align: middle;"></ion-icon>
                                </button>
                                <button type="button" class="btn btn-outline-danger open-delete-modal mr-auto ml-1">
                                    <ion-icon name="trash-outline" style="font-size: 22px; vertical-align: middle;"></ion-icon>
                                </button>
                            </td>
                        </tr>
                        `);
                    });
                    tabla_cp = $('#dataTable').DataTable({
                        "language":{"url":"//cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json"}
                    });
                    toastr["success"](json_data.message);
                }
                else if (json_data.status == 404 ) {
                    toastr["error"](json_data.message);
                }
            }
        });
    });
});

toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": true,
    "positionClass": "toast-bottom-left",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "8000",
    "hideDuration": "1000",
    "timeOut": "8000",
    "extendedTimeOut": "6000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}