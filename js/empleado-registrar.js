$(document).ready(function () {
    let button_event = false;

    $('#search-btn').click( function(event) {
        event.preventDefault();
        let to_search = $('#to_search').val();
        if (to_search != "") {
            if($('#to_search').hasClass('is-invalid'))
            {
                $('#to_search').removeClass('is-invalid');
                $('#to_search').addClass('form-input');
                $('#search-error').html("");
            }
            $('#error_container').html(`
                <div class="spinner-grow mt-4 mb-2" role="status" style="color:#958419;">
                    <span class="sr-only">Loading...</span>
                </div>
            `);
            $.ajax({
                type: "POST",
                url: "busqueda-usuario.php",
                data: {
                    to_search
                },
                success: function (data) {
                    let json_data = JSON.parse(data);
                    $('#result_container').html('');
                    $('#error_container').html('');
                    if (json_data.length > 0) {
                        let user_type_string = "";
                        let status = "";
                        let bottom = "";
                        json_data.forEach( user => {
                            if ( user.status == 1 ) {
                                status = "border-danger";
                                bottom = `<ion-icon class="text-danger mr-4" name="warning-outline" style="font-size:30px; vertical-align:middle;"></ion-icon>`;
                            }
                            else{
                                status = "";
                                bottom = `<button class="btn btn-green-strong-invert mr-3 do_register"><span class="d-none d-md-none d-lg-none d-xl-inline" style="vertical-align:middle;">REGISTRAR </span><ion-icon name="arrow-forward-circle-outline" style="font-size:22px; vertical-align:middle;"></ion-icon></button>`;
                            }
                            $('#result_container').append(`
                            <div class="col-sm-12 col-md-11 mx-auto mb-3">
                                <div class="card card-search card-hov ${status} shadow h-100" style="text-decoration:none" data-userid="${user.id_persona}" data-username="${user.nombre}" data-usertype="${getUserTypeString(user.tipo_persona)}" data-userstatus="${user.status}">
                                    <div class="card-body p-0 ml-3" style="margin-top:10px; margin-bottom:10px;">
                                    <div class="row no-gutters align-items-center m-0 p-0">
                                        <div class="col-sm-6">
                                        <div class="card-search-top-text">${user.id_persona}</div>
                                        <div class="card-search-main">${user.nombre}</div>
                                        </div>
                                        <div class="col-sm-4 pt-1">
                                        <div class="card-search-type">${getUserTypeString(user.tipo_persona)}</div>
                                        </div>
                                        <div class="col-sm-2 d-sm-flex d-none justify-content-end">
                                            ${bottom}
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            `);
                        });
                        $('html, body').animate({scrollTop: 220}, 700);
                    }
                    else {
                        $('#error_container').html(`
                        <div class="row mt-4">
                            <div class="col-sm-12 mx-auto text-center text-secondary">
                            <i class='bx bx-ghost' style="font-size:80px;"></i>
                            <h5 class="mb-0 mb-0 mt-2" style="font-size:18px; font-weight:bold;">No se encontraron usuarios con esa matrícula o nombre.</h5>
                            <p class="mt-2 pt-0" style="font-size:14px;">Intenta buscar con una entrada diferente.</p>
                            </div>
                        </div>
                        `);
                        $('html, body').animate({scrollTop: $("#error_container").offset().top},1000);
                    }
                }
            });
        }
        else {
            $('#to_search').removeClass('form-input');
            $('#to_search').addClass('is-invalid');
            $('#search-error').html("Debes ingresar un nombre o una matrícula");
            $('#to_search').focus();
        }
    });

    $('#result_container').on('click','.card-search', function (event) {
        let id_persona = $(this).data('userid');
        let username = $(this).data('username');
        let usertype = $(this).data('usertype');
        let userstatus = $(this).data('userstatus');
        $('#info_userid').html(id_persona);
        $('#info_username').html(username);
        $('#info_usetype').html(usertype);
        $('#info_userid').data('userstatus',userstatus);
        $('#info_sospechoso').html('');
        if(userstatus == "1" )
        {
            $('#info_sospechoso').append(`
            <div class="row mt-4 bg-warning ml-2 mr-3 px-1 py-2" style="border-radius:4px">
                <div class="col-2 mr-0 pr-0 text-center d-flex align-items-center">
                    <ion-icon name="warning-outline" style="font-size: 27px; vertical-align: middle;"></ion-icon>
                </div>
                <div class="col-10 ml-0 pl-0 d-flex align-items-center" style="font-size:15px;">
                    Este usuario fue registrado previemente como caso sospechoso de COVID-19
                </div>
            </div>
            <div class="row ml-2 mr-3 mt-4 mb-0 text-secondary text-center" style="font-size:14px;">
                <p class="mb-0 pb-0">Antes de permitir el acceso a este usuario asegurate de que cumpla las <a href="" class="text-secondary" style="font-weight:bold;">indicaciones para permitir el acceso a una persona previamente declarada como caso sospechoso.</a></p>
            </div>
            `);
            $('#modal_register').removeClass('btn-green-strong');
            $('#modal_register').addClass('btn-danger');
        }
        else
        {
            $('#modal_register').addClass('btn-green-strong');
            $('#modal_register').removeClass('btn-danger');
        }
        $('#register_modal').modal('show');        
    });

    $('#modal_register').click( function (event) {
        $('#register_modal').modal('hide');
        let id_persona = $('#info_userid').html();
        let id_cp = $('#cp-interno').val();
        let id_encargado = $('#user_tag').data('userid');
        let status = $('#info_userid').data('userstatus');
        let detalles = $('#info_detalles').val();
        if(id_cp != 0) {
            $.ajax({
                type: "POST",
                url: "registro-usuario.php",
                data: {
                    tipo_registro: "3", // Registro manual de persona ya registrada
                    id_persona,
                    id_cp,
                    id_encargado,
                    detalles,
                    status,
                },
                success: function (data) {
                    let json_data = JSON.parse(data);
                    if (json_data.status == 202 )
                    {
                        toastr["success"](json_data.message);
                        $('#result_content').html('');
                        $('#to_search').val('');
                        $('#to_search').focus();
                        $('#info_detalles').val('');
                    }
                    else if (json_data.status == 404 )
                    {
                        toastr["error"](json_data.message);
                    }
                }
            });
        }
        else
        {
            $('#cp-interno').removeClass('form-input');
            $('#cp-interno').addClass('is-invalid');
            $('#select-error').html('Selecciona un punto de control');
            $('html, body').animate({scrollTop: $("#page-top").offset().top},800);
            $('#cp-interno').focus();
        }
    });

    $('#register-btn').click( function(event) {
        event.preventDefault();
        let go = true;
        let id_cp = $('#cp-externo').val();
        if(id_cp  == 0 )
        {
            go = false;
            $('#cp-externo').removeClass('form-input');
            $('#cp-externo').addClass('is-invalid');
            $('#extern-select-error').html('Selecciona un punto de control');
        }
        let externo_nombre = $('#externo-nombre').val();
        if(externo_nombre == '')
        {
            go = false;
            $('#externo-nombre').removeClass('form-input');
            $('#externo-nombre').addClass('is-invalid');
            $('#extern-name-error').html('Ingresa el nombre de la persona a registrar');
        }

        let externo_email = $('#externo-email').val();
        let externo_tel = $('#externo-tel').val();
        
        let externo_motivo = $('#externo-motivo').val();
        if(externo_motivo == '')
        {
            go = false;
            $('#externo-motivo').removeClass('form-input');
            $('#externo-motivo').addClass('is-invalid');
            $('#extern-motivo-error').html('Ingresa el motivo de visita de la persona');
        }
        let id_encargado = $('#user_tag').data('userid');
        if(go) {
            $.ajax({
                type: "POST",
                url: "registro-usuario.php",
                data: {
                    tipo_registro: "4",
                    externo_nombre,
                    id_cp,
                    externo_email,
                    externo_tel,
                    externo_motivo,
                    id_encargado
                },
                success: function (data) {
                    let json_data = JSON.parse(data);
                    if (json_data.status == 202 ) {
                        toastr["success"](json_data.message);
                        $('#externo-nombre').val('');
                        $('#externo-email').val('');
                        $('#externo-tel').val('');
                        $('#externo-motivo').val('');
                        $('#externo-nombre').focus();
                    }
                    else if (json_data.status == 404 ) {
                        toastr["error"](json_data.message);
                    }
                }
            });
        }
    });

    $('#cp-interno').change( function(event) {
        if($('#cp-interno').hasClass('is-invalid')) {
            $('#cp-interno').removeClass('is-invalid');
            $('#cp-interno').addClass('form-input');
            $('#select-error').html('');
        }
    });

    $('#nav-externo').click(function (event) {
        $('#result_container').addClass('d-none');
    });

    $('#nav-interno').click(function (event) {
        $('#result_container').removeClass('d-none');
    });

    $('#to_search').keyup( function (event) {
        if($('#to_search').val() != "" )
        {
            if ($('#to_search').hasClass('is-invalid'))
            {
                $('#to_search').addClass('form-input');
                $('#to_search').removeClass('is-invalid');
                $('#search-error').html("");
            }
        }
    });
});

function getUserTypeString(usertype) {
    if (usertype == 1) {
        return "ADMINISTRATIVO / EMPLEADO";
    }
    else if( usertype == 2 )
    {
        return "ENCARGADO DE PUNTO DE CONTROL";
    }
    else if( usertype == 3 )
    {
        return "PROFESOR";
    } 
    else if ( usertype == 4 ) {
        return "ALUMNO";
    }      
    else if ( usertype == 5 ) {
        return "USUARIO EXTERNO";
    }
    else if ( usertype == 6 ) {
        return "ADMINISTRATIVO / EMPLEADO";
    }
    else 
    {
        return "NULL";
    }
}

toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-bottom-left",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "7000",
    "hideDuration": "1000",
    "timeOut": "7000",
    "extendedTimeOut": "5000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}

toastr.options.onclick = function() { console.log('clicked'); }
toastr.options.onCloseClick = function() { console.log('close button clicked'); }