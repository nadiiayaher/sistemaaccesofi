$(document).ready( function () {

    $('#btn-chg').click( function (event) {
        $('#first_btn').toggleClass('d-none');
        $('#pass_row').toggleClass('d-none');
        $('#confirm_row').toggleClass('d-none');
        $('#btns_row').toggleClass('d-none');
        $('#user_new_pass').focus();
        $('html, body').animate({scrollTop: $("#btns_row").offset().top},1000);
    });

    $('#cancel_chg_btn').click( function (event) {
        $('#first_btn').toggleClass('d-none');
        $('#pass_row').toggleClass('d-none');
        $('#confirm_row').toggleClass('d-none');
        $('#btns_row').toggleClass('d-none');
        $('#user_new_pass').val("");
        $('#user_new_confirm').val("");
        $('#pass_see_btn').addClass('d-none');
        $('#confirm_see_btn').addClass('d-none');
        $('#user_new_pass').addClass('form-input');
        $('#user_new_pass').removeClass('is-invalid');
        $('#user_new_confirm').addClass('form-input');
        $('#user_new_confirm').removeClass('is-invalid');
        $('#msg_pass').html('');
        $('#msg_confirm').html('');
    });

    $('#go_btn').click( function (event) {
        let new_p = $('#user_new_pass').val();
        let new_confirm = $('#user_new_confirm').val();
        if( new_p != "" & new_confirm != "")
        {
            if( new_p.length >= 8 & new_confirm.length >= 8 )
            {
                if ( new_p == new_confirm ) {
                    let identificador = $('#user_id').val();
                    $.ajax({
                        type: "POST",
                        url: "../change_p.php",
                        data: {
                            user_type: "3",
                            identificador,
                            new_p
                        },
                        success: function (data) {
                            let json_data = JSON.parse(data);
                            if (json_data.status == 202 ) {
                                toastr["success"](json_data.message);
                                $('#first_btn').toggleClass('d-none');
                                $('#pass_row').toggleClass('d-none');
                                $('#confirm_row').toggleClass('d-none');
                                $('#btns_row').toggleClass('d-none');
                                $('#user_new_pass').val("");
                                $('#user_new_confirm').val("");
                                $('#pass_see_btn').addClass('d-none');
                                $('#confirm_see_btn').addClass('d-none');
                                $('#user_new_pass').addClass('form-input');
                                $('#user_new_pass').removeClass('is-invalid');
                                $('#user_new_confirm').addClass('form-input');
                                $('#user_new_confirm').removeClass('is-invalid');
                                $('#msg_pass').html('');
                                $('#msg_confirm').html('');
                            }
                            else if (json_data.status == 404 ) {
                                toastr["error"](json_data.message);
                            }
                        }
                    });
                }
                else {
                    $('#user_new_pass').removeClass('form-input');
                    $('#user_new_pass').addClass('is-invalid');
                    $('#user_new_confirm').removeClass('form-input');
                    $('#user_new_confirm').addClass('is-invalid');
                    $('#msg_pass').html('Las contraseñas no coinciden');
                    $('#msg_confirm').html('Las contraseñas no coinciden');
                }
            }
            else {
                if ( new_p.length < 8 ) {
                    $('#user_new_pass').removeClass('form-input');
                    $('#user_new_pass').addClass('is-invalid');
                    $('#msg_pass').html('La contraseña debe tener al menos 8 caracteres');
                }
                if ( new_confirm.length < 8 ) {
                    $('#user_new_confirm').removeClass('form-input');
                    $('#user_new_confirm').addClass('is-invalid');
                    $('#msg_confirm').html('La contraseña debe tener al menos 8 caracteres');
                }
            }
        }
        else 
        {
            if ( new_p == "" ) {
                $('#user_new_pass').removeClass('form-input');
                $('#user_new_pass').addClass('is-invalid');
                $('#msg_pass').html('Debes ingrear una contraseña');
            }
            if ( new_confirm == "" ) {
                $('#user_new_confirm').removeClass('form-input');
                $('#user_new_confirm').addClass('is-invalid');
                $('#msg_confirm').html('Debes confirmar tu contraseña');
            }
        }
    });

    $('#user_new_pass').keyup( function (event) {
        if ($('#user_new_pass').val() != "") {
            $('#pass_see_btn').removeClass('d-none');
            if ($('#user_new_pass').hasClass('is-invalid')) {
                if ($('#user_new_pass').val().length < 8 ) {
                    $('#user_new_pass').removeClass('form-input');
                    $('#user_new_pass').addClass('is-invalid');
                    $('#msg_pass').html('La contraseña debe tener al menos 8 caracteres');
                }
                else{
                    $('#user_new_pass').addClass('form-input');
                    $('#user_new_pass').removeClass('is-invalid');
                    $('#msg_pass').html('');
                }
            }
        }
        else{
            $('#pass_see_btn').addClass('d-none');
            if ($('#user_new_pass').hasClass('is-invalid')) {
                $('#user_new_pass').removeClass('form-input');
                $('#user_new_pass').addClass('is-invalid');
                $('#msg_pass').html('Debes ingrear una contraseña');
            }
        }
    });

    $('#user_new_confirm').keyup( function (event) {
        if ($('#user_new_confirm').val() != "") {
            $('#confirm_see_btn').removeClass('d-none');
            if ($('#user_new_confirm').hasClass('is-invalid')) {
                if ($('#user_new_confirm').val().length < 8 ) {
                    $('#user_new_confirm').removeClass('form-input');
                    $('#user_new_confirm').addClass('is-invalid');
                    $('#msg_confirm').html('La contraseña debe tener al menos 8 caracteres');
                }
                else{
                    $('#user_new_confirm').addClass('form-input');
                    $('#user_new_confirm').removeClass('is-invalid');
                    $('#msg_confirm').html('');
                }
            }
        }
        else {
            $('#confirm_see_btn').addClass('d-none');
            if ($('#user_new_confirm').hasClass('is-invalid')) {
                if ($('#user_new_confirm').hasClass('is-invalid')) {
                    $('#user_new_confirm').removeClass('form-input');
                    $('#user_new_confirm').addClass('is-invalid');
                    $('#msg_confirm').html('Debes ingrear una contraseña');
                }
            }
        }
    });

    $('#pass_see_btn').click( function(event) {
        if( $('#user_new_pass').attr("type") == "password" )
        {
            $('#user_new_pass').attr("type","text");
            $('#pass_see_btn').html('<ion-icon name="eye-off-outline" style="vertical-align:middle; font-size:22px;"></ion-icon>');
        }
        else
        {
            $('#user_new_pass').attr("type","password");
            $('#pass_see_btn').html('<ion-icon name="eye-outline" style="vertical-align:middle; font-size:22px;"></ion-icon>');
        }
    });

    $('#confirm_see_btn').click( function(event) {
        if( $('#user_new_confirm').attr("type") == "password" )
        {
            $('#user_new_confirm').attr("type","text");
            $('#confirm_see_btn').html('<ion-icon name="eye-off-outline" style="vertical-align:middle; font-size:22px;"></ion-icon>');
        }
        else
        {
            $('#user_new_confirm').attr("type","password");
            $('#confirm_see_btn').html('<ion-icon name="eye-outline" style="vertical-align:middle; font-size:22px;"></ion-icon>');
        }
    });

    $('#pass_see_btn').focus( function(event) {
        $('#pass_see_btn').blur();
    });

    $('#confirm_see_btn').focus( function(event) {
        $('#confirm_see_btn').blur();
    });
});

toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-bottom-left",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "7000",
    "hideDuration": "1000",
    "timeOut": "7000",
    "extendedTimeOut": "5000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}