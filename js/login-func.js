$(document).ready( function () {

    $('.btn-login').click( function (event) {
        if ( $('#user_email').val() == '') {
            event.preventDefault();
            $('#user_email').removeClass('fomr-input');
            $('#user_email').addClass('is-invalid');
            $('#msg_email').html("Debes ingresar un correo institucional");
        }
        if ( $('#user_password').val() == '') {
            event.preventDefault();
            $('#user_password').removeClass('fomr-input');
            $('#user_password').addClass('is-invalid');
            $('#msg_pass').html("Debes ingresar una contraseña");
        }
        if ( $('#user_captcha').val() == '') {
            event.preventDefault();
            $('#user_captcha').removeClass('fomr-input');
            $('#user_captcha').addClass('is-invalid');
            $('#msg_captcha').html("Debes completar el captcha");
        }
    });

    $('#user_password').keydown( function (event) {
        if (event.keyCode == 13 ) {
            console.log('enter');
            event.preventDefault();
        }
    });

    $('#user_password').keyup( function (event) {
        if ($('#user_password').val() != "") {
            $('#see_pass_btn').removeClass('d-none');
            $('#user_password').removeClass('rounded_squares');
            if ($('#user_password').hasClass('is-invalid')) {
                $('#user_password').addClass('form-input');
                $('#user_password').removeClass('is-invalid');
                $('#msg_pass').html('');
            }
        }
        else{
            $('#see_pass_btn').addClass('d-none');
            $('#user_password').addClass('rounded_squares');
            if ($('#user_password').hasClass('is-invalid')) {
                $('#user_password').removeClass('form-input');
                $('#user_password').addClass('is-invalid');
                $('#msg_pass').html('Debes ingrear una contraseña');
            }
        }
    });

    $('#user_email').keydown( function (event) {
        if (event.keyCode == 13 ) {
            event.preventDefault();
        }
    });

    $('#user_email').keyup( function (event) {
        if ($('#user_email').val() != "") {
            if ($('#user_email').hasClass('is-invalid')) {
                $('#user_email').addClass('form-input');
                $('#user_email').removeClass('is-invalid');
                $('#msg_email').html('');
            }
        }
        else
        {
            if ($('#user_email').hasClass('is-invalid'))
            {
                $('#user_email').removeClass('form-input');
                $('#user_email').addClass('is-invalid');
                $('#msg_email').html('Debes ingrear una correo institucional');
            }
        }
    });

    $('#user_captcha').keyup( function (event) {
        if ($('#user_captcha').val() != "") {
            if ($('#user_captcha').hasClass('is-invalid')) {
                $('#user_captcha').addClass('form-input');
                $('#user_captcha').removeClass('is-invalid');
                $('#msg_captcha').html('');
            }
        }
        else
        {
            if ($('#user_captcha').hasClass('is-invalid'))
            {
                $('#user_captcha').removeClass('form-input');
                $('#user_captcha').addClass('is-invalid');
                $('#msg_captcha').html('Debes completar el captcha');
            }
        }
    });

    $('#see_pass_btn').click( function(event) {
        event.preventDefault();
        if( $('#user_password').attr("type") == "password" )
        {
            $('#user_password').attr("type","text");
            $('#see_pass_btn').html('<ion-icon name="eye-off-outline" style="vertical-align:middle; font-size:22px;"></ion-icon>');
        }
        else
        {
            $('#user_password').attr("type","password");
            $('#see_pass_btn').html('<ion-icon name="eye-outline" style="vertical-align:middle; font-size:22px;"></ion-icon>');
        }
    });
});