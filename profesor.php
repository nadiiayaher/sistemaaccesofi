<?php
  include('routes.php');
  include(SERVER_ROUTE.'/database.php');
  session_start();
  if(isset($_SESSION['id_usuario']))
  {
      switch($_SESSION['tipo_persona'])
      {
          case 1: // Tipo de usuario admin
              header('Location: '.WEB_ROUTE.'/admin.php');
              break;
          case 2: // Tipo de usuario encargado de cp
              header('Location: '.WEB_ROUTE.'/empleado.php');
              break;
          case 3: // Tipo de usuario profesor
              break;
          default:
            header('Location: '.WEB_ROUTE.'/index.php');
            break;
      }
  }
  else
  {
      header('Location: '.WEB_ROUTE.'/index.php');
  }

  $records = $connection->prepare('SELECT * FROM  materia,grupo WHERE grupo.id_materia = materia.id_materia AND grupo.id_profesor = :id_profesor;');
  $records->bindParam('id_profesor',$_SESSION['id_usuario']);
  $records->execute();
  $grupos = $records->fetchAll();
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Profesor - Sistema de control de acceso FI UAEM</title>
  <!-- Custom fonts for this template-->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
  <link href="<?=WEB_ROUTE?>/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link rel="icon" type="image/png" href="<?=WEB_ROUTE?>/img/png/fi_invert.png"/>
  <link href='https://unpkg.com/boxicons@2.0.8/css/boxicons.min.css' rel='stylesheet'>
  <!-- Custom styles for this template-->
  <link href="<?=WEB_ROUTE?>/css/standard-style.css" rel="stylesheet">
  <link rel="stylesheet" href="<?=WEB_ROUTE?>/css/profesor-style.css">
</head>
<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    <!-- Sidebar -->
    <ul class="navbar-nav sidebar accordion" id="accordionSidebar">
      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
        <div class="sidebar-brand-icon">
          <img src="<?=WEB_ROUTE?>/img/png/uaem-logo.png" alt="Universidad Autonoma del Estado de Mexico" width="57px" height="50px" style="border-radius:3px;">
        </div>
        <div class="sidebar-brand-text text-white mx-2">UAEM</div>
      </a>
      <!-- Divider -->
      <hr class="sidebar-divider my-0">
      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="<?=WEB_ROUTE?>/profesor.php">
          <ion-icon name="people-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Grupos</span>
        </a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">
      <!-- Heading -->
      <div class="sidebar-heading">
        Ayuda
      </div>
      <!-- Nav Item - Tables -->
      <li class="nav-item">
        <a class="nav-link" href="">
          <ion-icon name="help-circle-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Ayuda</span>
        </a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">
      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>
    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <?php include(SERVER_ROUTE.'/templates/navbar.php'); ?>
        <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- Page Heading -->
          <h4 class="mb-4" style="font-weight:600;">Tus grupos</h4>
          <?php if(!empty($grupos)): ?>
              <div class="row">
              <?php for ($i = 0; $i < sizeof($grupos); $i++){?>
                <div class="col-xl-4 col-md-6 mb-4">
                  <a href="<?=WEB_ROUTE?>/profesor-files/profesor-grupo.php?id_grupo=<?=$grupos[$i]['id_grupo']?>&nombre_materia=<?=$grupos[$i]['nombre']?>" class="card card-top card-hov shadow h-100" style="text-decoration:none">
                      <div class="card-body">
                        <div class="row no-gutters align-items-center">
                          <div class="col-10">
                            <div class="card-top-title"><?= $grupos[$i]['id_materia']?></div>
                            <div class="card-top-content"><?= $grupos[$i]['nombre']?></div>
                          </div>
                          <div class="col-2">
                            <ion-icon name="people-circle-outline" style="color: gray; font-size: 40px;"></ion-icon>
                          </div>
                        </div>
                      </div>
                  </a>
                </div>
              <?php };?>
              </div>
          <?php else: ?>
              <div class="row" style="margin-top:100px;">
                <div class="col-sm-12 mx-auto text-center text-secondary">
                  <i class='bx bx-ghost' style="font-size:115px;"></i>
                  <h5 class="mt-2">No tienes asignado ningún grupo</h5>
                </div>
              </div>
          <?php endif; ?>

        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->

      <!-- Footer 
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>UAEM 2020.</span>
          </div>
        </div>
      </footer>
      End of Footer -->

    </div>
    <!-- End of Content Wrapper -->
  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php include(SERVER_ROUTE.'/templates/logout-modal.php'); ?>

  <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
  <script src="https://unpkg.com/ionicons@5.2.3/dist/ionicons.js"></script>
  <script src="<?=WEB_ROUTE?>/js/standard-func.js"></script>
</body>
</html>
